from __future__ import annotations

import unittest

from collections.abc import Mapping
from dataclasses import dataclass
from typing import Any
from typing import Final

import pygenyamldoclexer
import pygenyamldoclexer.processor
import pygenyamldoclexer.processor.dictkey

from pygenos.abc import OSDir
from pygenpath.utils import PathUtils
from pygentestsample.utils import SampleTestUtils
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils
from pygentextreader.abc import TextReader
from pygenyamldoclexer.abc import YamlDocLexerProcessorMapper
from pygenyamldoclexer.processor.dictkey.impl import YamlDocLexerDictKeyProcessorImpl
from pygenyamldoclexer.result.impl import YamlDocLexerResultMapperImpl
from pygenyamldoclexer.value.impl import YamlDocLexerValueMapperImpl
from pygenyamldoclexerdev.abc import YamlDocLexerValueCollection
from pygenyamldoclexerdev.impl import YamlDocLexerValueListImpl
from pygenyamldoclexerdev.testcase.abc import YamlDocLexerTestCaseArgsFn
from pygenyamldoclexerdev.testcase.abc import YamlDocLexerTestCaseEntrypointFn
from pygenyamldoclexerdev.testcase.impl import YamlDocLexerTestCaseImpl
from pygenyamldoclexerdev.testcase.impl import YamlDocLexerTestCaseResultMapperImpl


@dataclass
class YamlDocLexerDictKeyProcessorTestCaseArgs(object):
    indent: Final[int]


class YamlDocLexerDictKeyProcessorTestCaseImpl(
    YamlDocLexerTestCaseImpl[YamlDocLexerDictKeyProcessorTestCaseArgs]
):
    class ArgsFnImpl(
        YamlDocLexerTestCaseArgsFn[
            YamlDocLexerDictKeyProcessorTestCaseArgs
        ]
    ):
        def __call__(
            self,
            args: Mapping[str, Any]
        ) -> YamlDocLexerDictKeyProcessorTestCaseArgs:
            return YamlDocLexerDictKeyProcessorTestCaseArgs(**args)

    class EntrypointFnImpl(
        YamlDocLexerTestCaseEntrypointFn[
            YamlDocLexerDictKeyProcessorTestCaseArgs
        ]
    ):
        def __call__(
            self,
            args: YamlDocLexerDictKeyProcessorTestCaseArgs,
            reader: TextReader
        ) -> YamlDocLexerValueCollection:
            values = YamlDocLexerValueListImpl()
            value_mapper = YamlDocLexerValueMapperImpl()
            result_mapper = YamlDocLexerTestCaseResultMapperImpl(
                values=values,
                origin=YamlDocLexerResultMapperImpl(value_mapper)
            )

            proccessor = YamlDocLexerDictKeyProcessorImpl(
                result_mapper=result_mapper,
                value_mapper=value_mapper,
                processor_mapper=YamlDocLexerProcessorMapper(),
                indent=args.indent
            )

            return YamlDocLexerValueListImpl.of(
                proccessor.process(reader).value()
            )

    def __init__(self, dirname: str, dir: OSDir) -> None:
        super().__init__(
            args_fn=self.ArgsFnImpl(),
            entrypoint_fn=self.EntrypointFnImpl(),
            dirname=dirname,
            dir=dir
        )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    dirpath = PathUtils.dirpath(
        path=TextUtils.format_args(
            tpl="{}/",
            args=[
                __file__
            ]
        )
    )
    workspace_dirpath = dirpath.resolve_dirpath("../../../../../../")

    test_suite = UnitTestSuiteImpl()

    SampleTestUtils.load(
        workspace_dirpath=workspace_dirpath,
        module=pygenyamldoclexer.processor.dictkey,
        create_test_case_fn=YamlDocLexerDictKeyProcessorTestCaseImpl,
        test_suite=test_suite
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
