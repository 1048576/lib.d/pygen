from __future__ import annotations

import unittest

from collections.abc import Iterable
from dataclasses import dataclass
from typing import Final
from typing import Protocol

from pygencollection.impl import ArrayImpl
from pygendataclass.abc import DataclassInstance
from pygenerr.err import SystemException
from pygenerr.err import UserException
from pygenpath.utils import PathUtils
from pygenrandom.randtext.impl import UniqueRandomTextGeneratorImpl
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


@dataclass
class PathTestCaseSampleArgs(DataclassInstance):
    foo: Final[str]
    bar: Final[str]
    baz: Final[str]


class InvalidPathTestCaseImpl(TestCase):
    class Entrypoint(Protocol):
        def __call__(self, path: str) -> object:
            raise SystemException()

    @dataclass
    class Sample(object):
        path_tpl: Final[str]
        expected_exception_msg_tpl: Final[str]

    @classmethod
    def dir(
        cls,
        name: str,
        entrypoint_fn: Entrypoint
    ) -> TestCase:
        return cls(
            name=name,
            entrypoint_fn=entrypoint_fn,
            samples=ArrayImpl.of(
                cls.Sample(
                    path_tpl="./{foo}",
                    expected_exception_msg_tpl=(
                        "A file path instead of a directory path"
                    )
                ),
                cls.Sample(
                    path_tpl="./{foo}/../{bar}/{baz}/",
                    expected_exception_msg_tpl=(
                        "Non-normalized path. "
                        "Replace [./{foo}/../{bar}/{baz}/] => [./{bar}/{baz}/]"
                    )
                )
            )
        )

    @classmethod
    def file(
        cls,
        name: str,
        entrypoint_fn: Entrypoint
    ) -> TestCase:
        return cls(
            name=name,
            entrypoint_fn=entrypoint_fn,
            samples=ArrayImpl.of(
                cls.Sample(
                    path_tpl="./{foo}/",
                    expected_exception_msg_tpl=(
                        "A directory path instead of a file path"
                    )
                ),
                cls.Sample(
                    path_tpl="./{foo}/../{bar}/{baz}",
                    expected_exception_msg_tpl=(
                        "Non-normalized path. "
                        "Replace [./{foo}/../{bar}/{baz}] => [./{bar}/{baz}]"
                    )
                )
            )
        )

    _name: Final[str]
    _entrypoint_fn: Final[Entrypoint]
    _samples: FinalCollection[Sample]
    _args: Final[PathTestCaseSampleArgs]

    def __init__(
        self,
        name: str,
        entrypoint_fn: Entrypoint,
        samples: Iterable[Sample]
    ) -> None:
        text_generator = UniqueRandomTextGeneratorImpl()

        self._name = name
        self._entrypoint_fn = entrypoint_fn
        self._samples = ArrayImpl.copy(samples)
        self._args = PathTestCaseSampleArgs(
            foo=text_generator.randtext(),
            bar=text_generator.randtext(),
            baz=text_generator.randtext()
        )

    def run(self, test: Test) -> None:
        for sample in self._samples:
            path = TextUtils.format(
                tpl=sample.path_tpl,
                args=self._args
            )

            expected_exception = UserException(
                msg=TextUtils.format(
                    tpl=sample.expected_exception_msg_tpl,
                    args=self._args
                )
            )

            sub_test = test.sub_test(
                msg=TextUtils.format_args(
                    tpl="{}: {}",
                    args=[
                        self._name,
                        str(sample)
                    ]
                )
            )

            with (sub_test):
                exception_catcher = test.exception_catcher()

                with (exception_catcher):
                    self._entrypoint_fn(path)

                test.assert_exception_equal(
                    expected=expected_exception,
                    exception_catcher=exception_catcher
                )


class PathNormalizeTestCaseImpl(TestCase):
    class EntrypointFn(Protocol):
        def __call__(self, path: str) -> str:
            raise SystemException()

    @dataclass
    class Sample(object):
        path_tpl: Final[str]
        expected_path_tpl: Final[str]

    _SAMPLES: FinalCollection[Sample] = ArrayImpl.of(
        Sample("/", "/"),
        Sample("/.", "/"),
        Sample("/./", "/"),
        Sample("/{foo}", "/{foo}"),
        Sample("/{foo}/.", "/{foo}/"),
        Sample("/{foo}/./", "/{foo}/"),
        Sample("/{foo}/..", "/"),
        Sample("/{foo}/../", "/"),
        Sample("/{foo}/./{bar}", "/{foo}/{bar}"),
        Sample("/{foo}/./{bar}/", "/{foo}/{bar}/"),
        Sample("/{foo}/../{bar}", "/{bar}"),
        Sample("/{foo}/../{bar}/", "/{bar}/"),
        Sample("/{foo}/{bar}", "/{foo}/{bar}"),
        Sample("/{foo}/{bar}/", "/{foo}/{bar}/")
    )

    _name: Final[str]
    _entrypoint_fn: Final[EntrypointFn]
    _args: Final[PathTestCaseSampleArgs]

    def __init__(
        self,
        name: str,
        entrypoint_fn: EntrypointFn
    ) -> None:
        text_generator = UniqueRandomTextGeneratorImpl()

        self._name = name
        self._entrypoint_fn = entrypoint_fn
        self._args = PathTestCaseSampleArgs(
            foo=text_generator.randtext(),
            bar=text_generator.randtext(),
            baz=text_generator.randtext()
        )

    def run(self, test: Test) -> None:
        for sample in self._SAMPLES:
            path = TextUtils.format(
                tpl=sample.path_tpl,
                args=self._args
            )

            expected_path = TextUtils.format(
                tpl=sample.expected_path_tpl,
                args=self._args
            )

            sub_test = test.sub_test(
                msg=TextUtils.format_args(
                    tpl="{}: {}",
                    args=[
                        self._name,
                        str(sample)
                    ]
                )
            )

            with (sub_test):
                test.assert_text_equal(
                    expected=expected_path,
                    actual=self._entrypoint_fn(path)
                )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    test_suite = UnitTestSuiteImpl()

    dirpath = PathUtils.dirpath("./")

    test_suite.add_test_case(
        test_case=InvalidPathTestCaseImpl.file(
            name=TextUtils.format_args(
                tpl="{}.{}",
                args=[
                    PathUtils.__name__,
                    PathUtils.filepath.__name__
                ]
            ),
            entrypoint_fn=PathUtils.filepath
        )
    )
    test_suite.add_test_case(
        test_case=InvalidPathTestCaseImpl.dir(
            name=TextUtils.format_args(
                tpl="{}.{}",
                args=[
                    PathUtils.__name__,
                    PathUtils.dirpath.__name__
                ]
            ),
            entrypoint_fn=PathUtils.dirpath
        )
    )
    test_suite.add_test_case(
        test_case=InvalidPathTestCaseImpl.file(
            name=TextUtils.format_args(
                tpl="{}.{}",
                args=[
                    dirpath.__class__.__name__,
                    dirpath.resolve_filepath.__name__
                ]
            ),
            entrypoint_fn=dirpath.resolve_filepath
        )
    )
    test_suite.add_test_case(
        test_case=InvalidPathTestCaseImpl.dir(
            name=TextUtils.format_args(
                tpl="{}.{}",
                args=[
                    dirpath.__class__.__name__,
                    dirpath.resolve_dirpath.__name__
                ]
            ),
            entrypoint_fn=dirpath.resolve_dirpath
        )
    )
    test_suite.add_test_case(
        test_case=PathNormalizeTestCaseImpl(
            name=TextUtils.format_args(
                tpl="{}.{}",
                args=[
                    PathUtils.__name__,
                    PathUtils.normalize.__name__
                ]
            ),
            entrypoint_fn=PathUtils.normalize
        )
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
