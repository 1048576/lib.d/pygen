from __future__ import annotations

import unittest

from dataclasses import dataclass
from typing import Final

from pygencli.abc import CliArg
from pygencli.abc import CliComplexCmd
from pygencli.abc import CliOption
from pygencli.abc import CliParser
from pygencli.abc import CliRootComplexCmd
from pygencli.abc import CliRootSimpleCmd
from pygencli.abc import CliSimpleCmd
from pygencli.err import CliException
from pygencli.parser.impl import CliParserBuilderImpl
from pygencollection.impl import ArrayImpl
from pygenrandom.randtext.abc import UniqueRandomTextGenerator
from pygenrandom.randtext.impl import UniqueRandomTextGeneratorImpl
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils


class CliParserTestCaseUtils(object):
    @classmethod
    def option(cls, name: str) -> str:
        return TextUtils.format_args(
            tpl="--{}",
            args=[
                name
            ]
        )


class CliParserArgAndEnvConflictTestCaseImpl(TestCase):
    _option_name: Final[str]
    _option_metavar: Final[str]
    _parser: Final[CliParser]

    def __init__(self) -> None:
        text_generator = UniqueRandomTextGeneratorImpl()
        builder = CliParserBuilderImpl()

        self._option_name = text_generator.randtext()
        self._option_metavar = text_generator.randtext()
        self._parser = builder.build(
            command=CliRootSimpleCmd(
                options=ArrayImpl.of(
                    CliOption(
                        name=self._option_name,
                        arg=CliArg(
                            metavar=self._option_metavar
                        )
                    )
                )
            )
        )

    def run(self, test: Test) -> None:
        exception_catcher = test.exception_catcher()

        with (exception_catcher):
            self._parser.parse(
                args=[
                    CliParserTestCaseUtils.option(self._option_name),
                    ""
                ],
                envs={
                    self._option_metavar: ""
                }
            )

        test.assert_exception_equal(
            expected=CliException(
                msg=TextUtils.format_args(
                    tpl=(
                        "Environment variable [{}] "
                        "conflicts with corresponding command-line argument"
                    ),
                    args=[
                        self._option_metavar
                    ]
                )
            ),
            exception_catcher=exception_catcher
        )


class CliParserTestCaseImpl(TestCase):
    @dataclass
    class SimpleCommand(object):
        name: Final[str]

    @dataclass
    class ComplexCommand(object):
        name: Final[str]
        metavar: Final[str]

    @dataclass
    class RootCommand(object):
        metavar: Final[str]

    @dataclass
    class Option(object):
        name: Final[str]
        metavar: Final[str]
        value: Final[str]

    @classmethod
    def _create_cli_cmd_arg(
        cls,
        command: ComplexCommand | RootCommand
    ) -> CliArg:
        return CliArg(
            metavar=command.metavar
        )

    @classmethod
    def _create_cli_option(
        cls,
        option: Option
    ) -> CliOption:
        return CliOption(
            name=option.name,
            arg=CliArg(
                metavar=option.metavar
            )
        )

    @classmethod
    def _create_complex_command(
        cls,
        text_generator: UniqueRandomTextGenerator
    ) -> ComplexCommand:
        return cls.ComplexCommand(
            name=text_generator.randtext(),
            metavar=text_generator.randtext()
        )

    @classmethod
    def _create_option(
        cls,
        text_generator: UniqueRandomTextGenerator
    ) -> Option:
        return cls.Option(
            name=text_generator.randtext(),
            metavar=text_generator.randtext(),
            value=text_generator.randtext()
        )

    @classmethod
    def _create_root_command(
        cls,
        text_generator: UniqueRandomTextGenerator
    ) -> RootCommand:
        return cls.RootCommand(
            metavar=text_generator.randtext()
        )

    @classmethod
    def _create_simple_command(
        cls,
        text_generator: UniqueRandomTextGenerator
    ) -> SimpleCommand:
        return cls.SimpleCommand(
            name=text_generator.randtext()
        )

    _baz_command: Final[SimpleCommand]
    _baz_foo_option: Final[Option]
    _baz_bar_option: Final[Option]
    _bar_command: Final[ComplexCommand]
    _bar_foo_option: Final[Option]
    _bar_bar_option: Final[Option]
    _foo_command: Final[RootCommand]
    _foo_foo_option: Final[Option]
    _foo_bar_option: Final[Option]

    _parser: Final[CliParser]

    def __init__(self) -> None:
        text_generator = UniqueRandomTextGeneratorImpl()
        builder = CliParserBuilderImpl()

        # ./foo bar baz --help
        # options:
        #   baz-foo - ...
        #   baz-bar - ...

        baz_command = self._create_simple_command(text_generator)
        baz_foo_option = self._create_option(text_generator)
        baz_bar_option = self._create_option(text_generator)

        baz_cmd = CliSimpleCmd(
            name=baz_command.name,
            options=ArrayImpl.of(
                self._create_cli_option(baz_foo_option),
                self._create_cli_option(baz_bar_option)
            )
        )

        # ./foo bar --help
        # options:
        #   bar-foo - ...
        #   bar-bar - ...
        # commands:
        #   baz - ...

        bar_command = self._create_complex_command(text_generator)
        bar_foo_option = self._create_option(text_generator)
        bar_bar_option = self._create_option(text_generator)

        bar_cmd = CliComplexCmd(
            name=bar_command.name,
            arg=self._create_cli_cmd_arg(bar_command),
            options=ArrayImpl.of(
                self._create_cli_option(bar_foo_option),
                self._create_cli_option(bar_bar_option)
            ),
            commands=ArrayImpl.of(
                baz_cmd
            )
        )

        # ./foo --help
        # options:
        #   foo-foo - ...
        #   foo-bar - ...
        # commands:
        #   bar - ...

        foo_command = self._create_root_command(text_generator)
        foo_foo_option = self._create_option(text_generator)
        foo_bar_option = self._create_option(text_generator)

        foo_cmd = CliRootComplexCmd(
            arg=self._create_cli_cmd_arg(foo_command),
            options=ArrayImpl.of(
                self._create_cli_option(foo_foo_option),
                self._create_cli_option(foo_bar_option)
            ),
            commands=ArrayImpl.of(
                bar_cmd
            )
        )

        self._baz_command = baz_command
        self._baz_foo_option = baz_foo_option
        self._baz_bar_option = baz_bar_option
        self._bar_command = bar_command
        self._bar_foo_option = bar_foo_option
        self._bar_bar_option = bar_bar_option
        self._foo_command = foo_command
        self._foo_foo_option = foo_foo_option
        self._foo_bar_option = foo_bar_option
        self._parser = builder.build(foo_cmd)

    def run(self, test: Test) -> None:
        actual_args = self._parser.parse(
            args=[
                CliParserTestCaseUtils.option(self._foo_foo_option.name),
                self._foo_foo_option.value,
                self._bar_command.name,
                CliParserTestCaseUtils.option(self._bar_foo_option.name),
                self._bar_foo_option.value,
                self._baz_command.name,
                CliParserTestCaseUtils.option(self._baz_foo_option.name),
                self._baz_foo_option.value
            ],
            envs={
                self._foo_bar_option.metavar: self._foo_bar_option.value,
                self._bar_bar_option.metavar: self._bar_bar_option.value,
                self._baz_bar_option.metavar: self._baz_bar_option.value
            }
        )

        expected_args = {
            self._foo_command.metavar: self._bar_command.name,
            self._bar_command.metavar: self._baz_command.name,

            self._foo_foo_option.metavar: self._foo_foo_option.value,
            self._bar_foo_option.metavar: self._bar_foo_option.value,
            self._baz_foo_option.metavar: self._baz_foo_option.value,

            self._foo_bar_option.metavar: self._foo_bar_option.value,
            self._bar_bar_option.metavar: self._bar_bar_option.value,
            self._baz_bar_option.metavar: self._baz_bar_option.value
        }

        test.assert_object_equal(
            expected=expected_args,
            actual=actual_args
        )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    test_suite = UnitTestSuiteImpl()

    test_suite.add_test_case(
        test_case=CliParserArgAndEnvConflictTestCaseImpl()
    )
    test_suite.add_test_case(
        test_case=CliParserTestCaseImpl()
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
