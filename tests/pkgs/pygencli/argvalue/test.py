from __future__ import annotations

import unittest

from typing import Final

from pygencli.abc import CliArgValueContextManager
from pygencli.argvalue.impl import CliArgValueContextManagerImpl
from pygencli.err import CliException
from pygenerr.err import UserException
from pygenrandom.randtext.impl import UniqueRandomTextGeneratorImpl
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentest.utils import TestUtils
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils


class CliArgValueContextManagerTestCaseImpl(TestCase):
    _value: Final[str]
    _manager: Final[CliArgValueContextManager]

    def __init__(self) -> None:
        text_generator = UniqueRandomTextGeneratorImpl()

        metavar = text_generator.randtext()

        self._value = text_generator.randtext()
        self._manager = CliArgValueContextManagerImpl(
            metavar=metavar,
            args={
                metavar: self._value
            }
        )

    def run(self, test: Test) -> None:
        with (self._manager as value):
            ...

        test.assert_text_equal(
            expected=self._value,
            actual=value
        )


class CliArgValueContextManagerUndefinedValueTestCaseImpl(TestCase):
    _metavar: Final[str]
    _manager: Final[CliArgValueContextManager]

    def __init__(self) -> None:
        text_generator = UniqueRandomTextGeneratorImpl()

        self._metavar = text_generator.randtext()
        self._manager = CliArgValueContextManagerImpl(self._metavar, {})

    def run(self, test: Test) -> None:
        exception_catcher = test.exception_catcher()

        with (exception_catcher):
            with (self._manager):
                ...

        test.assert_exception_equal(
            expected=CliException(
                msg=TextUtils.format_args(
                    tpl="Undefined arg [{}] value",
                    args=[
                        self._metavar
                    ]
                )
            ),
            exception_catcher=exception_catcher
        )


class CliArgValueContextManagerWrapUserExceptionTestCaseImpl(TestCase):
    class SomeUserException(UserException):
        ...

    _metavar: Final[str]
    _exception: Final[UserException]
    _manager: Final[CliArgValueContextManager]

    def __init__(self) -> None:
        text_generator = UniqueRandomTextGeneratorImpl()

        self._metavar = text_generator.randtext()
        self._exception = self.SomeUserException(text_generator.randtext())
        self._manager = CliArgValueContextManagerImpl(
            metavar=self._metavar,
            args={
                self._metavar: ""
            }
        )

    def run(self, test: Test) -> None:
        exception_catcher = test.exception_catcher()

        with (exception_catcher):
            with (self._manager):
                TestUtils.raise_exception(self._exception)

        test.assert_exception_equal(
            expected=self._exception.wrap(
                title=TextUtils.format_args(
                    tpl="Arg [{}]",
                    args=[
                        self._metavar
                    ]
                )
            ),
            exception_catcher=exception_catcher
        )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    test_suite = UnitTestSuiteImpl()

    test_suite.add_test_case(
        test_case=CliArgValueContextManagerTestCaseImpl()
    )
    test_suite.add_test_case(
        test_case=CliArgValueContextManagerUndefinedValueTestCaseImpl()
    )
    test_suite.add_test_case(
        test_case=CliArgValueContextManagerWrapUserExceptionTestCaseImpl()
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
