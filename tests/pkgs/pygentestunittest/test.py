from __future__ import annotations

import unittest

from typing import Final
from typing import Protocol
from unittest import TestSuite

from pygenerr.err import SystemException
from pygenrandom.randint.impl import UniqueRandomIntGeneratorImpl
from pygenrandom.randtext.abc import UniqueRandomTextGenerator
from pygenrandom.randtext.impl import UniqueRandomTextGeneratorImpl
from pygentest.abc import Test
from pygentest.abc import TestAssertExceptionEqualMsg
from pygentest.abc import TestAssertObjectEqualMsg
from pygentest.abc import TestAssertScalarEqualMsg
from pygentest.abc import TestAssertTextEqualMsg
from pygentest.utils import TestUtils
from pygentestunittest.abc import UnitTestCase
from pygentestunittest.impl import UnitTestImpl
from pygentext.utils import TextUtils


class UnitTestCaseAssertTestCaseImpl(UnitTestCase):
    class EntrypointFn(Protocol):
        def __call__(self, test: Test) -> None:
            raise SystemException()

    _entrypoint_fn: Final[EntrypointFn]
    _assert_message: Final[str]

    def __init__(
        self,
        entrypoint_fn: EntrypointFn,
        assert_message: str
    ) -> None:
        super().__init__(self.run_test.__name__)

        self._entrypoint_fn = entrypoint_fn
        self._assert_message = assert_message

    def run_test(self) -> None:
        test = UnitTestImpl(
            unit_test_case=self
        )

        with (self.assertRaises(AssertionError) as error_context):
            self._entrypoint_fn(test)

        self.assertEqual(
            first=AssertionError,
            second=error_context.exception.__class__
        )

        self.assertEqual(
            first=self._assert_message,
            second=TextUtils.join("", error_context.exception.args)
        )


class UnitTestCaseAssertExceptionEqualTestCaseImpl(
    UnitTestCaseAssertTestCaseImpl
):
    class EntrypointImpl(object):
        _expected: Final[Exception]
        _actual: Final[Exception]
        _msg: Final[TestAssertExceptionEqualMsg]

        def __init__(
            self,
            expected: Exception,
            actual: Exception,
            msg: TestAssertExceptionEqualMsg
        ) -> None:
            self._expected = expected
            self._actual = actual
            self._msg = msg

        def __call__(self, test: Test) -> None:
            exception_catcher = test.exception_catcher()

            with (exception_catcher):
                TestUtils.raise_exception(
                    exception=self._actual
                )

            test.assert_exception_equal(
                expected=self._expected,
                exception_catcher=exception_catcher,
                msg=self._msg
            )

    def __init__(self, text_generator: UniqueRandomTextGenerator) -> None:
        tpl = TextUtils.format_args(
            tpl=(
                "salt: {}\n"
                "expected: {{expected}}\n"
                "actual: {{actual}}"
            ),
            args=[
                text_generator.randtext()
            ]
        )
        expected = Exception()
        actual = Exception()

        entrypoint = self.EntrypointImpl(
            expected=expected,
            actual=actual,
            msg=lambda args: TextUtils.format_kwargs(
                tpl=tpl,
                expected=hash(args.expected),
                actual=hash(args.actual)
            )
        )

        super().__init__(
            entrypoint_fn=entrypoint,
            assert_message=TextUtils.format_kwargs(
                tpl=tpl,
                expected=hash(expected),
                actual=hash(actual)
            )
        )


class UnitTestCaseAssertObjectEqualTestCaseImpl(
    UnitTestCaseAssertTestCaseImpl
):
    class EntrypointImpl(object):
        _expected: Final[object]
        _actual: Final[object]
        _msg: Final[TestAssertObjectEqualMsg]

        def __init__(
            self,
            expected: object,
            actual: object,
            msg: TestAssertObjectEqualMsg
        ) -> None:
            self._expected = expected
            self._actual = actual
            self._msg = msg

        def __call__(self, test: Test) -> None:
            test.assert_object_equal(
                expected=self._expected,
                actual=self._actual,
                msg=self._msg
            )

    def __init__(self, text_generator: UniqueRandomTextGenerator) -> None:
        tpl = TextUtils.format_args(
            tpl=(
                "salt: {}\n"
                "expected: {{expected}}\n"
                "actual: {{actual}}"
            ),
            args=[
                text_generator.randtext()
            ]
        )
        expected = object()
        actual = object()

        entrypoint = self.EntrypointImpl(
            expected=expected,
            actual=actual,
            msg=lambda args: TextUtils.format_kwargs(
                tpl=tpl,
                expected=hash(args.expected),
                actual=hash(args.actual)
            )
        )

        super().__init__(
            entrypoint_fn=entrypoint,
            assert_message=TextUtils.format_kwargs(
                tpl=tpl,
                expected=hash(expected),
                actual=hash(actual)
            )
        )


class UnitTestCaseAssertScalarEqualTestCaseImpl(
    UnitTestCaseAssertTestCaseImpl
):
    class EntrypointImpl(object):
        _expected: Final[int]
        _actual: Final[int]
        _msg: Final[TestAssertScalarEqualMsg]

        def __init__(
            self,
            expected: int,
            actual: int,
            msg: TestAssertScalarEqualMsg
        ) -> None:
            self._expected = expected
            self._actual = actual
            self._msg = msg

        def __call__(self, test: Test) -> None:
            test.assert_scalar_equal(
                expected=self._expected,
                actual=self._actual,
                msg=self._msg
            )

    def __init__(self, text_generator: UniqueRandomTextGenerator) -> None:
        int_generator = UniqueRandomIntGeneratorImpl()

        tpl = TextUtils.format_args(
            tpl=(
                "salt: {}\n"
                "expected: {{expected}}\n"
                "actual: {{actual}}"
            ),
            args=[
                text_generator.randtext()
            ]
        )
        expected = int_generator.randint()
        actual = int_generator.randint()

        entrypoint = self.EntrypointImpl(
            expected=expected,
            actual=actual,
            msg=lambda args: TextUtils.format(
                tpl=tpl,
                args=args
            )
        )

        super().__init__(
            entrypoint_fn=entrypoint,
            assert_message=TextUtils.format_kwargs(
                tpl=tpl,
                expected=expected,
                actual=actual
            )
        )


class UnitTestCaseAssertTextEqualTestCaseImpl(
    UnitTestCaseAssertTestCaseImpl
):
    class EntrypointImpl(object):
        _expected: Final[str]
        _actual: Final[str]
        _msg: Final[TestAssertTextEqualMsg]

        def __init__(
            self,
            expected: str,
            actual: str,
            msg: TestAssertTextEqualMsg
        ) -> None:
            self._expected = expected
            self._actual = actual
            self._msg = msg

        def __call__(self, test: Test) -> None:
            test.assert_text_equal(
                expected=self._expected,
                actual=self._actual,
                msg=self._msg
            )

    def __init__(self, text_generator: UniqueRandomTextGenerator) -> None:
        tpl = TextUtils.format_args(
            tpl=(
                "salt: {}\n"
                "expected: {{expected}}\n"
                "actual: {{actual}}\n"
                "diff: {{diff}}"
            ),
            args=[
                text_generator.randtext()
            ]
        )
        expected = text_generator.randtext()
        actual = text_generator.randtext()
        diff = TextUtils.format_args(
            tpl=(
                "- {}\n"
                "+ {}\n"
            ),
            args=[
                expected,
                actual
            ]
        )

        entrypoint = self.EntrypointImpl(
            expected=expected,
            actual=actual,
            msg=lambda args: TextUtils.format_kwargs(
                tpl=tpl,
                expected=hash(args.expected),
                actual=hash(args.actual),
                diff=hash(args.diff)
            )
        )

        super().__init__(
            entrypoint_fn=entrypoint,
            assert_message=TextUtils.format_kwargs(
                tpl=tpl,
                expected=hash(expected),
                actual=hash(actual),
                diff=hash(diff)
            )
        )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    text_generator = UniqueRandomTextGeneratorImpl()

    test_suite = TestSuite()

    test_suite.addTest(
        test=UnitTestCaseAssertExceptionEqualTestCaseImpl(text_generator)
    )
    test_suite.addTest(
        test=UnitTestCaseAssertObjectEqualTestCaseImpl(text_generator)
    )
    test_suite.addTest(
        test=UnitTestCaseAssertScalarEqualTestCaseImpl(text_generator)
    )
    test_suite.addTest(
        test=UnitTestCaseAssertTextEqualTestCaseImpl(text_generator)
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
