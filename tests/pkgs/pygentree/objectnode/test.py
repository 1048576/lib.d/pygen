from __future__ import annotations

import unittest

from collections.abc import Callable
from collections.abc import Iterable
from collections.abc import Mapping
from dataclasses import dataclass
from typing import Final
from typing import Protocol

from pygencollection.abc import Dict
from pygencollection.abc import FinalDict
from pygencollection.impl import ArrayImpl
from pygencollection.impl import DictImpl
from pygencollection.utils import CollectionUtils
from pygenerr.err import SystemException
from pygenrandom.randtext.impl import UniqueRandomTextGeneratorImpl
from pygenrandom.utils import RandomUtils
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils
from pygentree.abc import TreeListNode
from pygentree.abc import TreeMapper
from pygentree.abc import TreeObjectNode
from pygentree.err import TreeInvalidNodeTypeException
from pygentree.err import TreeNodeNotFoundException
from pygentree.err import TreeUnexpectedNodesException
from pygentree.impl import TreeMapperImpl
from pygentree.objectnode.impl import TreeObjectNodeImpl
from pygentype.abc import FinalCollection


class TreeTestCaseMapperImpl(TreeMapper):
    _list_nodes: FinalDict[str, TreeListNode]
    _object_nodes: FinalDict[str, TreeObjectNode]

    def __init__(
        self,
        list_nodes: Dict[str, TreeListNode] = DictImpl(),
        object_nodes: Dict[str, TreeObjectNode] = DictImpl()
    ) -> None:
        self._list_nodes = list_nodes
        self._object_nodes = object_nodes

    def create_list_node(self, path: str, container: object) -> TreeListNode:
        node = self._list_nodes.detach(path)

        if (node is not None):
            return node
        else:
            raise SystemException()

    def create_object_node(
        self,
        path: str,
        container: object
    ) -> TreeObjectNode:
        node = self._object_nodes.detach(path)

        if (node is not None):
            return node
        else:
            raise SystemException()


@dataclass
class TreeTestCaseMethod(object):
    entrypoint_fn: Final[Callable[..., object]]
    instead_of: Final[str]


@dataclass
class TreeTestCaseNode(object):
    entrypoint_fn: Final[Callable[..., object]]
    create_mapper_fn: Final[Callable[[], TreeMapper]]
    name: Final[str]
    value: Final[object]


class TreeObjectNodeDetachNodeTestCaseImpl(TestCase):
    class CreateNodeFn(Protocol):
        def __call__(
            self,
            mapper: TreeMapper,
            path: str,
            container: Mapping[str, object]
        ) -> TreeObjectNode:
            raise SystemException()

    _create_node_fn: Final[CreateNodeFn]
    _path: Final[str]
    _methods: FinalCollection[TreeTestCaseMethod]
    _nodes: FinalCollection[TreeTestCaseNode]

    def __init__(
        self,
        create_node_fn: CreateNodeFn,
        path: str,
        methods: Iterable[TreeTestCaseMethod],
        nodes: Iterable[TreeTestCaseNode]
    ) -> None:
        self._create_node_fn = create_node_fn
        self._path = path
        self._methods = ArrayImpl.copy(methods)
        self._nodes = ArrayImpl.copy(nodes)

    def run(self, test: Test) -> None:
        for method in self._methods:
            for node in self._nodes:
                sub_test = test.sub_test(
                    msg=TextUtils.format_args(
                        tpl="{}: {}",
                        args=[
                            method.entrypoint_fn.__name__,
                            node.value.__class__.__name__
                        ]
                    )
                )

                with (sub_test):
                    if (node.entrypoint_fn == method.entrypoint_fn):
                        self._test_positive_case(
                            test=test,
                            method=method,
                            node=node
                        )
                    else:
                        self._test_negative_case(
                            test=test,
                            method=method,
                            node=node
                        )

    def _test_negative_case(
        self,
        test: Test,
        method: TreeTestCaseMethod,
        node: TreeTestCaseNode
    ) -> None:
        object_node = self._create_node_fn(
            mapper=TreeMapperImpl(),
            path=self._path,
            container={
                node.name: node.value
            }
        )

        exception_catcher = test.exception_catcher()

        with (exception_catcher):
            method.entrypoint_fn(
                self=object_node,
                name=node.name
            )

        expected = TreeInvalidNodeTypeException(
            msg=TextUtils.format_args(
                tpl="Node [{}.{}]: Invalid node type. [{}] instead of [{}]",
                args=[
                    self._path,
                    node.name,
                    node.value.__class__.__name__,
                    method.instead_of
                ]
            )
        )

        test.assert_exception_equal(
            expected=expected,
            exception_catcher=exception_catcher
        )

    def _test_positive_case(
        self,
        test: Test,
        method: TreeTestCaseMethod,
        node: TreeTestCaseNode
    ) -> None:
        object_node = self._create_node_fn(
            mapper=node.create_mapper_fn(),
            path=self._path,
            container={
                node.name: node.value
            }
        )

        actual = method.entrypoint_fn(
            self=object_node,
            name=node.name
        )

        test.assert_object_equal(
            expected=node.value,
            actual=actual
        )


class TreeObjectNodeDetachNonexistentNodeTestCaseImpl(TestCase):
    _path: Final[str]
    _nonexistent_node_name: Final[str]
    _methods: FinalCollection[TreeTestCaseMethod]
    _object_node: Final[TreeObjectNode]

    def __init__(
        self,
        create_node_fn: Callable[..., TreeObjectNode],
        path: str,
        nonexistent_node_name: str,
        methods: Iterable[TreeTestCaseMethod]
    ) -> None:
        self._path = path
        self._nonexistent_node_name = nonexistent_node_name
        self._methods = ArrayImpl.copy(methods)
        self._object_node = create_node_fn(
            mapper=TreeMapper(),
            path=self._path,
            container={}
        )

    def run(self, test: Test) -> None:
        for method in self._methods:
            sub_test = test.sub_test(
                msg=method.entrypoint_fn.__name__
            )

            with (sub_test):
                exception_catcher = test.exception_catcher()

                with (exception_catcher):
                    method.entrypoint_fn(
                        self=self._object_node,
                        name=self._nonexistent_node_name
                    )

                test.assert_exception_equal(
                    expected=TreeNodeNotFoundException(
                        msg=TextUtils.format_args(
                            tpl="Node [{}.{}] not found",
                            args=[
                                self._path,
                                self._nonexistent_node_name
                            ]
                        )
                    ),
                    exception_catcher=exception_catcher
                )


class TreeObjectNodeUnexpectedNodesTestCaseImpl(TestCase):
    _object_node: Final[TreeObjectNode]
    _expected_exception: Final[Exception]

    def __init__(
        self,
        create_node_fn: Callable[..., TreeObjectNode],
        path: str,
        nodes: Iterable[TreeTestCaseNode]
    ) -> None:
        container = CollectionUtils.to_dict(
            items=CollectionUtils.map(
                items=nodes,
                fn=lambda node: (node.name, node.value)
            )
        )

        self._object_node = create_node_fn(
            mapper=TreeMapper(),
            path=path,
            container=container
        )
        self._expected_exception = TreeUnexpectedNodesException(
            msg=TextUtils.format_args(
                tpl="Unexpected node(s) [{}{}]",
                args=[
                    path,
                    sorted(container.keys())
                ]
            )
        )

    def run(self, test: Test) -> None:
        exception_catcher = test.exception_catcher()

        with (exception_catcher):
            with (self._object_node):
                ...

        test.assert_exception_equal(
            expected=self._expected_exception,
            exception_catcher=exception_catcher
        )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    text_generator = UniqueRandomTextGeneratorImpl()

    path = text_generator.randtext()

    list_node_value = TreeListNode()
    list_node_name = text_generator.randtext()
    list_node_path = TextUtils.format_args(
        tpl="{}.{}",
        args=[
            path,
            list_node_name
        ]
    )

    object_node_value = TreeObjectNode()
    object_node_name = text_generator.randtext()
    object_node_path = TextUtils.format_args(
        tpl="{}.{}",
        args=[
            path,
            object_node_name
        ]
    )

    nonexistent_node_name = text_generator.randtext()

    methods = ArrayImpl.of(
        TreeTestCaseMethod(
            entrypoint_fn=TreeObjectNodeImpl.detach_text_node,
            instead_of="str"
        ),
        TreeTestCaseMethod(
            entrypoint_fn=TreeObjectNodeImpl.detach_bool_node,
            instead_of="bool"
        ),
        TreeTestCaseMethod(
            entrypoint_fn=TreeObjectNodeImpl.detach_float_node,
            instead_of="float"
        ),
        TreeTestCaseMethod(
            entrypoint_fn=TreeObjectNodeImpl.detach_int_node,
            instead_of="int"
        ),
        TreeTestCaseMethod(
            entrypoint_fn=TreeObjectNodeImpl.detach_list_node,
            instead_of="list"
        ),
        TreeTestCaseMethod(
            entrypoint_fn=TreeObjectNodeImpl.detach_object_node,
            instead_of="dict"
        )
    )

    nodes = ArrayImpl.of(
        TreeTestCaseNode(
            entrypoint_fn=TreeObjectNodeImpl.detach_text_node,
            create_mapper_fn=TreeTestCaseMapperImpl,
            name=text_generator.randtext(),
            value=text_generator.randtext()
        ),
        TreeTestCaseNode(
            entrypoint_fn=TreeObjectNodeImpl.detach_bool_node,
            create_mapper_fn=TreeTestCaseMapperImpl,
            name=text_generator.randtext(),
            value=True
        ),
        TreeTestCaseNode(
            entrypoint_fn=TreeObjectNodeImpl.detach_bool_node,
            create_mapper_fn=TreeTestCaseMapperImpl,
            name=text_generator.randtext(),
            value=False
        ),
        TreeTestCaseNode(
            entrypoint_fn=TreeObjectNodeImpl.detach_float_node,
            create_mapper_fn=TreeTestCaseMapperImpl,
            name=text_generator.randtext(),
            value=RandomUtils.randfloat()
        ),
        TreeTestCaseNode(
            entrypoint_fn=TreeObjectNodeImpl.detach_int_node,
            create_mapper_fn=TreeTestCaseMapperImpl,
            name=text_generator.randtext(),
            value=RandomUtils.randint()
        ),
        TreeTestCaseNode(
            entrypoint_fn=TreeObjectNodeImpl.detach_list_node,
            create_mapper_fn=lambda: TreeTestCaseMapperImpl(
                list_nodes=DictImpl.as_mapping(
                    pairs={
                        list_node_path: list_node_value
                    }
                )
            ),
            name=list_node_name,
            value=list_node_value
        ),
        TreeTestCaseNode(
            entrypoint_fn=TreeObjectNodeImpl.detach_object_node,
            create_mapper_fn=lambda: TreeTestCaseMapperImpl(
                object_nodes=DictImpl.as_mapping(
                    pairs={
                        object_node_path: object_node_value
                    }
                )
            ),
            name=object_node_name,
            value=object_node_value
        )
    )

    test_suite = UnitTestSuiteImpl()

    test_suite.add_test_case(
        test_case=TreeObjectNodeDetachNodeTestCaseImpl(
            create_node_fn=TreeObjectNodeImpl,
            path=path,
            methods=methods,
            nodes=nodes
        )
    )
    test_suite.add_test_case(
        test_case=TreeObjectNodeDetachNonexistentNodeTestCaseImpl(
            create_node_fn=TreeObjectNodeImpl,
            path=path,
            nonexistent_node_name=nonexistent_node_name,
            methods=methods
        )
    )
    test_suite.add_test_case(
        test_case=TreeObjectNodeUnexpectedNodesTestCaseImpl(
            create_node_fn=TreeObjectNodeImpl,
            path=path,
            nodes=nodes
        )
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
