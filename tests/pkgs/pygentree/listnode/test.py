from __future__ import annotations

import unittest

from typing import Final

from pygencollection.impl import ArrayImpl
from pygencollection.impl import DictImpl
from pygencollection.impl import ListImpl
from pygencollection.utils import CollectionUtils
from pygenrandom.randtext.impl import UniqueRandomTextGeneratorImpl
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils
from pygentree.abc import TreeMapper
from pygentree.abc import TreeObjectNode
from pygentree.err import TreeUnexpectedNodesException
from pygentree.listnode.impl import TreeListNodeImpl
from pygentype.abc import FinalCollection
from pygentype.abc import FinalMapping


class TreeListNodeDetachNodeTestCaseImpl(TestCase, TreeMapper):
    _TOTAL_NODE_COUNT: Final[int] = 3

    _path: Final[str]
    _key_to_object_node_mapping: FinalMapping[str, TreeObjectNode]
    _list_container: FinalCollection[object]
    _expected_object_nodes: FinalCollection[TreeObjectNode]

    def __init__(self) -> None:
        text_generator = UniqueRandomTextGeneratorImpl()

        path = text_generator.randtext()
        key_to_object_node_mapping = DictImpl[str, TreeObjectNode]()
        list_container = ListImpl[object]()
        expected_object_nodes = ListImpl[TreeObjectNode]()

        for index in range(0, self._TOTAL_NODE_COUNT):
            container = object()
            object_node = TreeObjectNode()
            key = TextUtils.format_args(
                tpl="{}[{}]:{}",
                args=[
                    path,
                    index,
                    id(container)
                ]
            )

            key_to_object_node_mapping[key] = object_node
            list_container.add(container)
            expected_object_nodes.add(object_node)

        self._path = path
        self._key_to_object_node_mapping = key_to_object_node_mapping
        self._list_container = ArrayImpl.copy(
            instance=list_container
        )
        self._expected_object_nodes = expected_object_nodes

    def create_object_node(
        self,
        path: str,
        container: object
    ) -> TreeObjectNode:
        key = TextUtils.format_args(
            tpl="{}:{}",
            args=[
                path,
                id(container)
            ]
        )

        return self._key_to_object_node_mapping[key]

    def run(self, test: Test) -> None:
        list_node = TreeListNodeImpl(
            mapper=self,
            path=self._path,
            container=self._list_container
        )
        actual_object_nodes = ListImpl[object]()

        with (list_node as list_node_ctx):
            while (list_node.has_more_nodes()):
                actual_object_nodes.add(
                    value=list_node_ctx.detach_object_node()
                )

        test.assert_scalar_equal(
            expected=True,
            actual=CollectionUtils.collection_eq(
                a=self._expected_object_nodes,
                b=actual_object_nodes
            )
        )


class TreeListNodeUnexpectedNodesTestCaseImpl(TestCase, TreeMapper):
    _TOTAL_NODE_COUNT: Final[int] = 3

    _path: Final[str]
    _container: FinalCollection[object]

    def __init__(self) -> None:
        text_generator = UniqueRandomTextGeneratorImpl()

        self._path = text_generator.randtext()
        self._container = ArrayImpl.collect(
            iterator=CollectionUtils.map(
                items=range(0, self._TOTAL_NODE_COUNT),
                fn=lambda _: object()
            )
        )

    def create_object_node(
        self,
        path: str,
        container: object
    ) -> TreeObjectNode:
        return TreeObjectNode()

    def run(self, test: Test) -> None:
        for detached_node_count in range(0, self._TOTAL_NODE_COUNT):
            sub_test = test.sub_test(
                msg=TextUtils.format_args(
                    tpl="detached node count: {}/{}",
                    args=[
                        detached_node_count,
                        self._TOTAL_NODE_COUNT
                    ]
                )
            )

            with (sub_test):
                list_node = TreeListNodeImpl(
                    mapper=self,
                    path=self._path,
                    container=self._container
                )
                exception_catcher = test.exception_catcher()

                with (exception_catcher):
                    with (list_node as list_node_ctx):
                        for _ in range(0, detached_node_count):
                            list_node_ctx.detach_object_node()

                test.assert_exception_equal(
                    expected=TreeUnexpectedNodesException(
                        msg=TextUtils.format_args(
                            tpl="Unexpected node(s) [{}[{}:{}]]",
                            args=[
                                self._path,
                                detached_node_count,
                                self._TOTAL_NODE_COUNT - 1
                            ]
                        )
                    ),
                    exception_catcher=exception_catcher
                )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    test_suite = UnitTestSuiteImpl()

    test_suite.add_test_case(
        test_case=TreeListNodeDetachNodeTestCaseImpl()
    )
    test_suite.add_test_case(
        test_case=TreeListNodeUnexpectedNodesTestCaseImpl()
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
