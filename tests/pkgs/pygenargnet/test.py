from __future__ import annotations

import unittest

from typing import Final

from pygenargnet.abc import NetworkAddressArg
from pygenargnet.impl import NetworkAddressArgImpl
from pygennet.abc import NetworkAddress
from pygennet.impl import NetworkAddressImpl
from pygenrandom.randtext.impl import UniqueRandomTextGeneratorImpl
from pygenrandom.utils import RandomUtils
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils


class NetworkAddressArgTestCaseImpl(TestCase):
    _description: Final[str]
    _value: Final[NetworkAddress]
    _arg: Final[NetworkAddressArg]

    def __init__(self) -> None:
        text_generator = UniqueRandomTextGeneratorImpl()

        self._description = text_generator.randtext()
        self._value = NetworkAddressImpl(
            host=text_generator.randtext(),
            port=RandomUtils.randint(0, 65536)
        )
        self._arg = NetworkAddressArgImpl(
            description=self._description,
            value=self._value
        )

    def run(self, test: Test) -> None:
        self._test_value(test)
        self._test_describe(test)

    def _test_describe(self, test: Test) -> None:
        with (test.sub_test(NetworkAddressArg.describe.__name__)):
            test.assert_text_equal(
                expected=TextUtils.format_args(
                    tpl="{}: [{}]",
                    args=[
                        self._description,
                        self._value
                    ]
                ),
                actual=self._arg.describe()
            )

    def _test_value(self, test: Test) -> None:
        with (test.sub_test(NetworkAddressArg.value.__name__)):
            arg_ctx = self._arg.__enter__()

            test.assert_object_equal(
                expected=self._value,
                actual=arg_ctx.value()
            )


def load_tests(
    loader: object,
    tests: object,
    pattern: object
) -> object:
    test_suite = UnitTestSuiteImpl()

    test_suite.add_test_case(
        test_case=NetworkAddressArgTestCaseImpl()
    )

    return test_suite


if (__name__ == "__main__"):
    unittest.main()
