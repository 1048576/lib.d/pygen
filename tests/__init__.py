import json
import sys

import pygenworkspace.utils

from pygencollection.utils import CollectionUtils

if (__name__ == "tests"):
    workspace_dirpath = pygenworkspace.utils.workspace_dirpath(__file__)

    pyrightcfg_filepath = workspace_dirpath.absolute.resolve_filepath(
        path="./pyrightconfig.json"
    )

    with (open(pyrightcfg_filepath.path()) as f):
        cfg = json.loads(f.read())

    paths = cfg.get("extraPaths")

    for index, path in CollectionUtils.items(paths):
        dirpath = workspace_dirpath.absolute.resolve_dirpath(path)

        sys.path.insert(index, dirpath.path())
