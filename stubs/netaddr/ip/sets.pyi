from collections.abc import Iterable

class IPSet:
    def __init__(self, iterable: Iterable[str]) -> None:
        ...

    def intersection(self, other: IPSet) -> IPSet:
        ...

    def union(self, other: IPSet) -> IPSet:
        ...
