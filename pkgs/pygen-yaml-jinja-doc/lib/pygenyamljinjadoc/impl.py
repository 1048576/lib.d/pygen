from __future__ import annotations

from typing import Final

from pygenjinjadoc.abc import JinjaDocTokenEOF
from pygenjinjadoc.processor.impl import JinjaDocProcessorImpl
from pygentext.utils import TextUtils
from pygentextdoc.abc import TextDocLoader
from pygentextdoc.abc import TextDocStream
from pygentextdoc.impl import TextDocLoaderImpl
from pygentype.abc import FinalCollection
from pygenyamldoc.abc import YamlDocTokenEOF
from pygenyamldoc.processor.impl import YamlDocProcessorDocumentImpl
from pygenyamljinjadoc.abc import YamlJinjaDocLoader
from pygenyamljinjadoc.reader.impl import YamlJinjaDocReaderImpl


class YamlJinjaDocLoaderImpl(YamlJinjaDocLoader):
    _jinja_loader: Final[TextDocLoader]
    _yaml_loader: Final[TextDocLoader]

    def __init__(self) -> None:
        self._jinja_loader = TextDocLoaderImpl(
            processor=JinjaDocProcessorImpl(),
            eof_token_type=JinjaDocTokenEOF
        )
        self._yaml_loader = TextDocLoaderImpl(
            processor=YamlDocProcessorDocumentImpl(),
            eof_token_type=YamlDocTokenEOF
        )

    def load_stream_from_file(self, path: str) -> TextDocStream:
        return self._yaml_loader.load_stream_from_reader(
            reader=YamlJinjaDocReaderImpl(
                name=TextUtils.format_args(
                    tpl="{}:{{line}}",
                    args=[
                        path
                    ]
                ),
                stream=self._jinja_loader.load_stream_from_file(path)
            )
        )


__all__: FinalCollection[str] = [
    "YamlJinjaDocLoaderImpl"
]
