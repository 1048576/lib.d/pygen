from __future__ import annotations

from dataclasses import dataclass
from typing import Final

from pygencollection.abc import FinalList
from pygencollection.impl import ListImpl
from pygenerr.err import SystemException
from pygenjinjadoc.abc import JinjaDocTokenBeginFor
from pygenjinjadoc.abc import JinjaDocTokenEndFor
from pygenjinjadoc.abc import JinjaDocTokenEOF
from pygenjinjadoc.abc import JinjaDocTokenNonprintingText
from pygenjinjadoc.abc import JinjaDocTokenText
from pygentext.utils import TextUtils
from pygentextdoc.abc import TextDocStream
from pygentextdoc.abc import TextDocStreamNode
from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection
from pygentype.abc import FinalIterator
from pygentype.abc import Mutable


class YamlJinjaDocReaderImpl(TextReader):
    @dataclass
    class Line(object):
        no: Final[int]
        length: Final[int]

    _name: Final[str]
    _stream_iterator: FinalIterator[TextDocStreamNode]
    _data_window_lines: FinalList[Line]
    _data_window_chars: Mutable[str]
    _pointer: Mutable[int]

    def __init__(self, name: str, stream: TextDocStream) -> None:
        self._name = name
        self._stream_iterator = iter(stream)
        self._data_window_lines = ListImpl()
        self._data_window_chars = ""
        self._pointer = 0

    def __str__(self) -> str:
        return TextUtils.format_kwargs(
            tpl=self._name,
            line=self.line()
        )

    def forward(self, length: int) -> None:
        if (not self._ensure_enough_data(length - 1)):
            raise SystemException()

        new_pointer = (self._pointer + length)
        line_pointer = 0
        removed_char_count = 0

        while (line_pointer < new_pointer):
            line = self._data_window_lines[0]
            line_pointer += line.length

            if (line_pointer <= new_pointer):
                self._data_window_lines.detach(0)

                removed_char_count += line.length

        self._pointer = (new_pointer - removed_char_count)

        if (removed_char_count > 0):
            self._data_window_chars = \
                self._data_window_chars[removed_char_count:]

    def line(self) -> int:
        self._ensure_enough_data(1)

        return self._data_window_lines[0].no

    def peek(self, index: int) -> str:
        if (self._ensure_enough_data(index)):
            return self._data_window_chars[self._pointer + index]
        else:
            return ""

    def _ensure_enough_data(self, index: int) -> bool:
        update_size = \
            (self._pointer + index - len(self._data_window_chars) + 1)

        if (update_size > 0):
            self._update(update_size)

        return ((self._pointer + index) < len(self._data_window_chars))

    def _update(self, size: int) -> None:
        current_size = 0

        while (current_size < size):
            try:
                node = next(self._stream_iterator)

                if (isinstance(node.token, JinjaDocTokenText)):
                    suffix = TextUtils.join(
                        delimiter="",
                        items=node.token.parts
                    )
                    line = self.Line(
                        no=node.line,
                        length=len(suffix)
                    )

                    self._data_window_lines.add(line)
                elif (isinstance(node.token, JinjaDocTokenNonprintingText)):
                    line = self.Line(
                        no=node.line,
                        length=0
                    )

                    self._data_window_lines.add(line)

                    continue
                elif (isinstance(node.token, JinjaDocTokenBeginFor)):
                    line = self.Line(
                        no=node.line,
                        length=0
                    )

                    self._data_window_lines.add(line)

                    continue
                elif (isinstance(node.token, JinjaDocTokenEndFor)):
                    line = self.Line(
                        no=node.line,
                        length=0
                    )

                    self._data_window_lines.add(line)

                    continue
                elif (isinstance(node.token, JinjaDocTokenEOF)):
                    line = self.Line(
                        no=node.line,
                        length=0
                    )

                    self._data_window_lines.add(line)

                    return
                else:
                    raise SystemException()
            except StopIteration:
                return

            current_size += len(suffix)

            self._data_window_chars += suffix


__all__: FinalCollection[str] = [
    "YamlJinjaDocReaderImpl"
]
