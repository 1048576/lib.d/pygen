from __future__ import annotations

import os

from typing import Final

from pygenerr.err import SystemException
from pygenerr.err import UserException
from pygenpath.abc import Dirpath
from pygenpath.abc import Filepath
from pygenpath.abc import Path
from pygenpath.abc import PathMapper
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class FilepathImpl(Filepath):
    _path: Final[str]

    def __init__(self, path: str) -> None:
        self._path = path

    def __eq__(self, value: object) -> bool:
        if (not isinstance(value, Filepath)):
            return False

        return (value.path() == self._path)

    def __str__(self) -> str:
        return self._path

    def path(self) -> str:
        return self._path


class DirpathImpl(Dirpath):
    _mapper: Final[PathMapper]
    _path: Final[str]

    def __init__(self, mapper: PathMapper, path: str) -> None:
        self._mapper = mapper
        self._path = path

    def __eq__(self, value: object) -> bool:
        if (not isinstance(value, Dirpath)):
            return False

        return (value.path() == self._path)

    def __lt__(self, value: object) -> bool:
        if (not isinstance(value, Dirpath)):
            raise SystemException()

        return (self._path < value.path())

    def __str__(self) -> str:
        return self._path

    def contains(self, path: Path) -> bool:
        return path.path().startswith(self._path)

    def path(self) -> str:
        return self._path

    def resolve_dirpath(self, path: str) -> Dirpath:
        self._mapper.raise_exception_on_unnormalized_dir_path(path)

        return DirpathImpl(
            mapper=self._mapper,
            path=self._mapper.normalize(
                path=os.path.join(self._path, path)
            )
        )

    def resolve_filepath(self, path: str) -> Filepath:
        self._mapper.raise_exception_on_unnormalized_file_path(path)

        return FilepathImpl(
            path=self._mapper.normalize(
                path=os.path.join(self._path, path)
            )
        )


class PathMapperImpl(PathMapper):
    def dirpath(self, path: str) -> Dirpath:
        self.raise_exception_on_unnormalized_dir_path(path)

        return DirpathImpl(self, path)

    def filename(self, path: str) -> str:
        basename = os.path.basename(path)

        if (basename in ("", ".", "..")):
            return ""
        else:
            return basename

    def filepath(self, path: str) -> Filepath:
        return DirpathImpl(self, "./").resolve_filepath(path)

    def normalize(self, path: str) -> str:
        filename = self.filename(path)
        body = os.path.normpath(path)

        if (body == "/"):
            return body
        elif (body == "."):
            return "./"

        if (body.startswith("/")):
            prefix = ""
        elif (body.startswith("../")):
            prefix = ""
        elif (body == ".."):
            prefix = ""
        else:
            prefix = "./"

        if (any(filename)):
            suffix = ""
        else:
            suffix = "/"

        return TextUtils.join(
            delimiter="",
            items=[
                prefix,
                body,
                suffix
            ]
        )

    def raise_exception_on_unnormalized_dir_path(self, path: str) -> None:
        self.raise_exception_on_unnormalized_path(path)

        filename = self.filename(path)

        if (filename != ""):
            raise UserException(
                msg="A file path instead of a directory path"
            )

    def raise_exception_on_unnormalized_file_path(self, path: str) -> None:
        self.raise_exception_on_unnormalized_path(path)

        filename = self.filename(path)

        if (filename == ""):
            raise UserException(
                msg="A directory path instead of a file path"
            )

    def raise_exception_on_unnormalized_path(self, path: str) -> None:
        normalized_path = self.normalize(path)

        if (path != normalized_path):
            raise UserException(
                msg=TextUtils.format_args(
                    tpl="Non-normalized path. Replace [{}] => [{}]",
                    args=[
                        path,
                        normalized_path
                    ]
                )
            )


__all__: FinalCollection[str] = [
    "PathMapperImpl"
]
