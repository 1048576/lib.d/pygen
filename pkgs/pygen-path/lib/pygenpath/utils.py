from __future__ import annotations

from typing import Final

from pygenpath.abc import Dirpath
from pygenpath.abc import Filepath
from pygenpath.abc import PathMapper
from pygenpath.impl import PathMapperImpl
from pygentype.abc import FinalCollection


class PathUtils(object):
    MAPPER: Final[PathMapper] = PathMapperImpl()

    @classmethod
    def dirpath(cls, path: str) -> Dirpath:
        return cls.MAPPER.dirpath(path)

    @classmethod
    def filename(cls, path: str) -> str:
        return cls.MAPPER.filename(path)

    @classmethod
    def filepath(cls, path: str) -> Filepath:
        return cls.MAPPER.filepath(path)

    @classmethod
    def normalize(cls, path: str) -> str:
        return cls.MAPPER.normalize(path)

    @classmethod
    def raise_exception_on_unnormalized_dir_path(cls, path: str) -> None:
        cls.MAPPER.raise_exception_on_unnormalized_dir_path(path)

    @classmethod
    def raise_exception_on_unnormalized_file_path(cls, path: str) -> None:
        cls.MAPPER.raise_exception_on_unnormalized_file_path(path)

    @classmethod
    def raise_exception_on_unnormalized_path(cls, path: str) -> None:
        cls.MAPPER.raise_exception_on_unnormalized_path(path)


__all__: FinalCollection[str] = [
    "PathUtils"
]
