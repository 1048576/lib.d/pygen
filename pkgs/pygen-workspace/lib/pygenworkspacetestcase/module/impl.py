from __future__ import annotations

import ast

from collections.abc import Iterable
from typing import Final

from pygenast.abc import ASTAnnAssignStmt
from pygenast.abc import ASTNode
from pygenast.utils import ASTUtils
from pygenastanalyzer.abc import ASTAnalyzer
from pygenastanalyzer.impl import ASTAnalyzerImpl
from pygenastanalyzer.impl import ASTTextConstantProcessorImpl
from pygencollection.abc import Array
from pygenjinja.abc import JinjaTemplate
from pygenpy.abc import PyModule
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygentype.abc import Optional


class WorkspaceModuleExportableNamesTestCaseImpl(TestCase):
    CONSTANT_NAME: Final[str] = "__all__"

    _template: Final[JinjaTemplate]
    _module: Final[PyModule]
    _analyzer: Final[ASTAnalyzer]

    def __init__(
        self,
        template: JinjaTemplate,
        module: PyModule
    ) -> None:
        self._template = template
        self._module = module
        self._analyzer = ASTAnalyzerImpl(
            name="Module",
            filepath=self._module.filepath
        )

    def run(self, test: Test) -> None:
        with (open(self._module.filepath.path()) as f):
            source = f.read()

        module = ast.parse(
            source=source,
            filename=self._module.filepath.path()
        )

        stmt = self._find_stmt(module.body)

        if (stmt is None):
            return

        names = self._read_names(stmt)

        test.assert_text_equal(
            expected=self._template.render(
                names=sorted(set(names))
            ),
            actual=TextUtils.join(
                delimiter="\n",
                items=source.split("\n")[stmt.lineno - 1:stmt.end_lineno]
            ),
            msg=lambda args: TextUtils.format_args(
                tpl="{}:{}\n{}",
                args=[
                    self._module.filepath,
                    stmt.lineno,
                    args.diff
                ]
            )
        )

    def _find_stmt(
        self,
        body: Iterable[ASTNode]
    ) -> Optional[ASTAnnAssignStmt]:
        for stmt in ASTUtils.filter(body, ASTAnnAssignStmt):
            if (ASTUtils.name_equal(stmt.target, self.CONSTANT_NAME)):
                return stmt

        return None

    def _read_names(
        self,
        stmt: ASTAnnAssignStmt
    ) -> Array[str]:
        return self._analyzer.read_list(
            what=self.CONSTANT_NAME,
            owner=stmt,
            expr=stmt.value,
            processor=ASTTextConstantProcessorImpl()
        )


__all__: FinalCollection[str] = [
    "WorkspaceModuleExportableNamesTestCaseImpl"
]
