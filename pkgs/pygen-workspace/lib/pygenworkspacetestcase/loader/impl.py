from __future__ import annotations

from typing import Final
from typing import Generic

from pygentype.abc import FinalCollection
from pygentype.abc import T
from pygenworkspacetestcase.loader.abc import LoaderResult


class LoaderResultSuccessImpl(Generic[T], LoaderResult[T]):
    _value: Final[T]

    def __init__(self, value: T) -> None:
        self._value = value

    def success(self) -> bool:
        return True

    def value(self) -> T:
        return self._value


class LoaderResultFailureImpl(Generic[T], LoaderResult[T]):
    _error: Final[str]

    def __init__(self, error: str) -> None:
        self._error = error

    def error(self) -> str:
        return self._error

    def success(self) -> bool:
        return False


__all__: FinalCollection[str] = [
    "LoaderResultFailureImpl",
    "LoaderResultSuccessImpl"
]
