from __future__ import annotations

from pygenjinja.abc import JinjaTemplate
from pygenpy.abc import PyPackage
from pygentestfilecontent.impl import FileContentTestCaseImpl
from pygentype.abc import FinalCollection


class WorkspacePackageInitModuleTestCaseImpl(FileContentTestCaseImpl):
    def __init__(
        self,
        template: JinjaTemplate,
        package: PyPackage
    ) -> None:
        super().__init__(
            filepath=package.init_module_filepath,
            expected=template.render()
        )


__all__: FinalCollection[str] = [
    "WorkspacePackageInitModuleTestCaseImpl"
]
