from __future__ import annotations

import os.path

from collections.abc import Iterator
from typing import Final

from pygencollection.impl import ArrayImpl
from pygenerr.err import UserException
from pygenos.abc import OSDir
from pygenos.impl import OSDirImpl
from pygenos.utils import OSUtils
from pygenpy.utils import PyUtils
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspaceDir
from pygenworkspace.abc import WorkspaceDirpath
from pygenworkspace.abc import WorkspaceTestsDir
from pygenworkspace.abc import WorkspaceTestsDirDataCollector


class WorkspaceTestsDirDataCollectorImpl(WorkspaceTestsDirDataCollector):
    _workspace_dirpath: Final[WorkspaceDirpath]
    _dir: Final[WorkspaceDir]

    def __init__(self, workspace_dirpath: WorkspaceDirpath) -> None:
        dirname = "tests"
        dirpath = workspace_dirpath.resolve_dirpath(
            path=TextUtils.format_args(
                tpl="./{}/",
                args=[
                    dirname
                ]
            )
        )

        self._workspace_dirpath = workspace_dirpath
        self._dir = WorkspaceDir(
            dirname=lambda: dirname,
            dirpath=lambda: dirpath
        )

    def collect(self) -> WorkspaceTestsDir:
        if (os.path.exists(self._dir.dirpath().absolute.path())):
            dirs = ArrayImpl.copy(
                instance=self._find_dirs()
            )

            packages = ArrayImpl.collect(
                iterator=PyUtils.find_packages(
                    dirs=dirs,
                    dirpath=self._dir.dirpath().absolute.resolve_dirpath(
                        path="../"
                    )
                )
            )

            return WorkspaceTestsDir(
                dir=self._dir,
                dirs=dirs,
                packages=packages
            )
        else:
            raise UserException(
                msg=TextUtils.format_args(
                    tpl="Tests dir doesn't exist [{}]",
                    args=[
                        self._dir.dirpath().absolute.path()
                    ]
                )
            )

    def _find_dirs(self) -> Iterator[OSDir]:
        yield OSDirImpl(
            dirpath=self._workspace_dirpath.absolute,
            dirnames=ArrayImpl.of(self._dir.dirname()),
            filenames=ArrayImpl.of()
        )

        for dirpath in OSUtils.walk(self._dir.dirpath().absolute):
            yield dirpath


__all__: FinalCollection[str] = [
    "WorkspaceTestsDirDataCollectorImpl"
]
