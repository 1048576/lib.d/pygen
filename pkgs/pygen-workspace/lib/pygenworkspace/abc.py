from __future__ import annotations

from collections.abc import Callable
from collections.abc import Collection
from dataclasses import dataclass
from typing import Final
from typing import Generic

from pygenerr.err import SystemException
from pygenos.abc import OSDir
from pygenpath.abc import Dirpath
from pygenpath.abc import Filepath
from pygenpy.abc import PyPackage
from pygentest.abc import TestSuite
from pygentype.abc import FinalCollection
from pygentype.abc import FinalMapping
from pygentype.abc import T


class Workspace(object):
    def entrypoint(self) -> str:
        raise SystemException()

    def load_tests(self, test_suite: TestSuite) -> None:
        raise SystemException()


class WorkspaceConstants(object):
    IMAGE_DIR_PATH: Final[str] = "./images/tests/"

    COVERAGE_CFG_PATH: Final[str] = "./.coveragerc"
    FLAKE8_CFG_PATH: Final[str] = "./.flake8"
    ISORT_CFG_PATH: Final[str] = "./.isort.cfg"
    PYRE_CFG_PATH: Final[str] = "./.pyre_configuration"
    PYRIGHT_CFG_PATH: Final[str] = "./pyrightconfig.json"
    REQUIREMENTS_PATH: Final[str] = "./.requirements.txt"


@dataclass
class WorkspaceDirpath(object):
    absolute: Final[Dirpath]
    relative: Final[Dirpath]

    def resolve_dirpath(self, path: str) -> WorkspaceDirpath:
        raise SystemException()

    def resolve_filepath(self, path: str) -> WorkspaceFilepath:
        raise SystemException()


@dataclass
class WorkspaceFilepath(object):
    absolute: Final[Filepath]
    relative: Final[Filepath]


@dataclass
class WorkspaceDir(object):
    dirname: Final[Callable[[], str]]
    dirpath: Final[Callable[[], WorkspaceDirpath]]


class WorkspaceDataCollector(Generic[T]):
    def collect(self) -> T:
        raise SystemException()


@dataclass
class WorkspacePackageLinkOwner(object):
    name: Final[str]
    dirpath: Final[Dirpath]


@dataclass
class WorkspacePackageLink(object):
    owner: Final[WorkspacePackageLinkOwner]
    reference: Final[str]


@dataclass
class WorkspacePkgEgg(object):
    info_dirpath: Final[WorkspaceDirpath]


@dataclass
class WorkspacePkg(object):
    dirpath: Final[WorkspaceDirpath]
    build_dirpath: Final[WorkspaceDirpath]
    lib_dirpath: Final[WorkspaceDirpath]
    setupfile_filepath: Final[WorkspaceFilepath]
    requirements_filepath: Final[WorkspaceFilepath]
    name: Final[str]
    package_data: FinalMapping[str, Collection[str]]
    package_dir: FinalMapping[str, str]
    entry_points: FinalMapping[str, Collection[str]]
    packages: FinalCollection[PyPackage]
    scripts: FinalCollection[str]
    egg: Final[WorkspacePkgEgg]


@dataclass
class WorkspacePkgsDir(object):
    used: Final[bool]
    dir: Final[WorkspaceDir]
    dirs: FinalCollection[OSDir]
    pkgs: FinalCollection[WorkspacePkg]


class WorkspacePkgsDirDataCollector(
    WorkspaceDataCollector[WorkspacePkgsDir]
):
    ...


@dataclass
class WorkspaceStubsDir(object):
    used: Final[bool]
    dir: Final[WorkspaceDir]


class WorkspaceStubsDirDataCollector(
    WorkspaceDataCollector[WorkspaceStubsDir]
):
    ...


@dataclass
class WorkspaceTestsDir(object):
    dir: Final[WorkspaceDir]
    dirs: FinalCollection[OSDir]
    packages: FinalCollection[PyPackage]


class WorkspaceTestsDirDataCollector(
    WorkspaceDataCollector[WorkspaceTestsDir]
):
    ...


@dataclass
class WorkspaceTestCaseLoaderContext(object):
    pkgs_dir: Final[WorkspacePkgsDir]
    stubs_dir: Final[WorkspaceStubsDir]
    tests_dir: Final[WorkspaceTestsDir]


class WorkspaceTestCaseLoader(object):
    def load(
        self,
        test_suite: TestSuite,
        context: WorkspaceTestCaseLoaderContext
    ) -> None:
        raise SystemException()


__all__: FinalCollection[str] = [
    "Workspace",
    "WorkspaceConstants",
    "WorkspaceDataCollector",
    "WorkspaceDir",
    "WorkspaceDirpath",
    "WorkspaceFilepath",
    "WorkspacePackageLink",
    "WorkspacePackageLinkOwner",
    "WorkspacePkg",
    "WorkspacePkgEgg",
    "WorkspacePkgsDir",
    "WorkspacePkgsDirDataCollector",
    "WorkspaceStubsDir",
    "WorkspaceStubsDirDataCollector",
    "WorkspaceTestCaseLoader",
    "WorkspaceTestCaseLoaderContext",
    "WorkspaceTestsDir",
    "WorkspaceTestsDirDataCollector"
]
