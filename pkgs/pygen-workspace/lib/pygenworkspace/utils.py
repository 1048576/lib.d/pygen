from __future__ import annotations

from collections.abc import Iterable

import pygenworkspacetemplate

from pygencollection.impl import ArrayImpl
from pygencollection.utils import CollectionUtils
from pygenjinja.impl import JinjaTemplateLoaderImpl
from pygenpath.utils import PathUtils
from pygentest.abc import TestSuite
from pygentestunittest.impl import UnitTestSuiteImpl
from pygentext.utils import TextUtils
from pygenworkspace.abc import Workspace
from pygenworkspace.abc import WorkspaceDirpath
from pygenworkspace.impl import WorkspaceDirpathImpl
from pygenworkspacetestcase.testspackage.impl import WorkspaceTestsPackageTestModuleTestCaseImpl


def unittest_suite() -> TestSuite:
    return UnitTestSuiteImpl()


def workspace_dirpath(path: str) -> WorkspaceDirpath:
    dirpath = PathUtils.dirpath(
        path=TextUtils.format_args(
            tpl="{}/",
            args=[
                path
            ]
        )
    )

    return WorkspaceDirpathImpl(
        absolute=dirpath.resolve_dirpath("../../"),
        relative=PathUtils.dirpath("./")
    )


def load_tests(
    test_suite: TestSuite,
    workspace_dirpath: WorkspaceDirpath,
    repo_url: str,
    workspaces: Iterable[Workspace]
) -> None:
    template_loader = JinjaTemplateLoaderImpl()

    test_suite.add_test_case(
        test_case=WorkspaceTestsPackageTestModuleTestCaseImpl(
            template=template_loader.load(
                module=pygenworkspacetemplate,
                name="testspackage-test-module"
            ),
            workspace_dirpath=workspace_dirpath,
            repo_url=repo_url,
            imports=ArrayImpl.collect(
                iterator=CollectionUtils.map(
                    items=workspaces,
                    fn=lambda workspace: TextUtils.format_args(
                        tpl="from {} import {}",
                        args=[
                            workspace.__class__.__module__,
                            workspace.__class__.__name__
                        ]
                    )
                )
            ),
            workspaces=ArrayImpl.collect(
                iterator=CollectionUtils.map(
                    items=workspaces,
                    fn=lambda workspace: workspace.entrypoint()
                )
            )
        )
    )

    for workspace in workspaces:
        workspace.load_tests(test_suite)
