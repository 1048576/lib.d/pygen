from __future__ import annotations

from typing import Final

from pygenjinja.abc import JinjaTemplate
from pygentest.abc import TestSuite
from pygentestfilecontent.impl import FileContentTestCaseImpl
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspaceConstants
from pygenworkspace.abc import WorkspaceDirpath
from pygenworkspace.abc import WorkspaceTestCaseLoader
from pygenworkspace.abc import WorkspaceTestCaseLoaderContext


class WorkspaceCoverageCfgTestCaseImpl(FileContentTestCaseImpl):
    ...


class WorkspaceCoverageTestCaseLoaderImpl(WorkspaceTestCaseLoader):
    _template: Final[JinjaTemplate]
    _workspace_dirpath: Final[WorkspaceDirpath]

    def __init__(
        self,
        template: JinjaTemplate,
        workspace_dirpath: WorkspaceDirpath
    ) -> None:
        self._template = template
        self._workspace_dirpath = workspace_dirpath

    def load(
        self,
        test_suite: TestSuite,
        context: WorkspaceTestCaseLoaderContext
    ) -> None:
        test_suite.add_test_case(
            test_case=WorkspaceCoverageCfgTestCaseImpl(
                filepath=self._workspace_dirpath.absolute.resolve_filepath(
                    path=WorkspaceConstants.COVERAGE_CFG_PATH
                ),
                expected=self._template.render()
            )
        )


__all__: FinalCollection[str] = [
    "WorkspaceCoverageTestCaseLoaderImpl"
]
