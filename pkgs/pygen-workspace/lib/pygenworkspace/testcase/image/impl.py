from __future__ import annotations

from collections.abc import Iterable
from collections.abc import Iterator
from typing import Final

from pygencollection.impl import ArrayImpl
from pygencollection.utils import CollectionUtils
from pygenerr.err import SystemException
from pygenjinja.abc import JinjaTemplate
from pygenpath.abc import Dirpath
from pygenpath.abc import Filepath
from pygenpath.utils import PathUtils
from pygentest.abc import TestSuite
from pygentestfilecontent.impl import FileContentTestCaseImpl
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspaceConstants
from pygenworkspace.abc import WorkspaceDirpath
from pygenworkspace.abc import WorkspaceTestCaseLoader
from pygenworkspace.abc import WorkspaceTestCaseLoaderContext


class WorkspaceImageDockerfileTestCaseImpl(FileContentTestCaseImpl):
    ...


class WorkspaceImageDockerfileTestCaseLoaderImpl(WorkspaceTestCaseLoader):
    _template: Final[JinjaTemplate]
    _workspace_dirpath: Final[WorkspaceDirpath]
    _dirnames: FinalCollection[str]
    _build_stage_dirpath: Final[Dirpath]
    _dockerfile_path: Final[Filepath]

    def __init__(
        self,
        template: JinjaTemplate,
        workspace_dirpath: WorkspaceDirpath,
        dirnames: Iterable[str]
    ) -> None:
        image_dirpath = PathUtils.dirpath(
            path=WorkspaceConstants.IMAGE_DIR_PATH
        )

        self._template = template
        self._workspace_dirpath = workspace_dirpath
        self._dirnames = ArrayImpl.copy(dirnames)
        self._build_stage_dirpath = image_dirpath.resolve_dirpath(
            path="./stages/build/"
        )
        self._dockerfile_path = image_dirpath.resolve_filepath("./Dockerfile")

    def load(
        self,
        test_suite: TestSuite,
        context: WorkspaceTestCaseLoaderContext
    ) -> None:
        preinstall_paths = self._preinstall_paths(context)
        postinstall_paths = self._postinstall_paths()
        isort_args = self._isort_args(context)
        flake8_args = self._flake8_args(context)

        if (context.pkgs_dir.used):
            expected = self._template.render(
                build_stage_dir_path=self._build_stage_dirpath.path(),
                preinstall_paths=preinstall_paths,
                postinstall_paths=postinstall_paths,
                isort_args=TextUtils.join(
                    delimiter=" ",
                    items=isort_args
                ),
                flake8_args=TextUtils.join(
                    delimiter=" ",
                    items=flake8_args
                )
            )
        else:
            raise SystemException()

        test_suite.add_test_case(
            test_case=WorkspaceImageDockerfileTestCaseImpl(
                filepath=self._workspace_dirpath.absolute.resolve_filepath(
                    path=self._dockerfile_path.path()
                ),
                expected=expected
            )
        )

    def _flake8_args(
        self,
        context: WorkspaceTestCaseLoaderContext
    ) -> Iterator[str]:
        if (context.pkgs_dir.used):
            yield context.pkgs_dir.dir.dirpath().relative.path()

        if (context.stubs_dir.used):
            yield context.stubs_dir.dir.dirpath().relative.path()

        yield context.tests_dir.dir.dirpath().relative.path()

    def _isort_args(
        self,
        context: WorkspaceTestCaseLoaderContext
    ) -> Iterator[str]:
        if (context.pkgs_dir.used):
            yield context.pkgs_dir.dir.dirpath().relative.path()

        yield context.tests_dir.dir.dirpath().relative.path()

    def _postinstall_paths(self) -> Iterator[str]:
        for dirname in self._dirnames:
            dirpath = PathUtils.dirpath(
                path=TextUtils.format_args(
                    tpl="./{}/",
                    args=[
                        dirname
                    ]
                )
            )

            yield dirpath.path()

    def _preinstall_paths(
        self,
        context: WorkspaceTestCaseLoaderContext
    ) -> Iterator[str]:
        requirements_file_paths = CollectionUtils.map(
            items=context.pkgs_dir.pkgs,
            fn=lambda pkg: pkg.requirements_filepath.relative.path()
        )

        yield self._dockerfile_path.path()

        yield WorkspaceConstants.COVERAGE_CFG_PATH

        yield WorkspaceConstants.FLAKE8_CFG_PATH

        yield WorkspaceConstants.ISORT_CFG_PATH

        yield WorkspaceConstants.PYRE_CFG_PATH

        yield WorkspaceConstants.PYRIGHT_CFG_PATH

        yield WorkspaceConstants.REQUIREMENTS_PATH

        for path in sorted(requirements_file_paths):
            yield path


__all__: FinalCollection[str] = [
    "WorkspaceImageDockerfileTestCaseLoaderImpl"
]
