from __future__ import annotations

from collections.abc import Iterable

from pygencollection.impl import ArrayImpl
from pygencollection.utils import CollectionUtils
from pygenos.utils import OSUtils
from pygenpath.abc import Filepath
from pygenpy.abc import PyPackage
from pygentest.abc import TestSuite
from pygentestunknownfile.impl import UnknownFileTestCaseImpl
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspaceTestCaseLoader
from pygenworkspace.abc import WorkspaceTestCaseLoaderContext
from pygenworkspace.abc import WorkspaceTestsDir


class WorkspaceTestsDirUnknownFileTestCaseImpl(UnknownFileTestCaseImpl):
    @classmethod
    def _filter(
        cls,
        test_packages: Iterable[PyPackage],
        filepath: Filepath
    ) -> bool:
        for package in test_packages:
            if (not package.dirpath.contains(filepath)):
                continue

            if (package.cache_dirpath.contains(filepath)):
                return False

            if (filepath == package.init_module_filepath):
                return False

            for module in package.modules:
                if (filepath == module.filepath):
                    return False

        return True

    def __init__(
        self,
        tests_dir: WorkspaceTestsDir
    ) -> None:
        filepaths = CollectionUtils.filter(
            items=OSUtils.filepaths(tests_dir.dirs),
            fn=lambda filepath: self._filter(tests_dir.packages, filepath)
        )

        super().__init__(
            filepaths=ArrayImpl.collect(filepaths)
        )


class WorkspaceTestsDirTestCaseLoaderImpl(WorkspaceTestCaseLoader):
    def load(
        self,
        test_suite: TestSuite,
        context: WorkspaceTestCaseLoaderContext
    ) -> None:
        test_suite.add_test_case(
            test_case=WorkspaceTestsDirUnknownFileTestCaseImpl(
                tests_dir=context.tests_dir
            )
        )


__all__: FinalCollection[str] = [
    "WorkspaceTestsDirTestCaseLoaderImpl"
]
