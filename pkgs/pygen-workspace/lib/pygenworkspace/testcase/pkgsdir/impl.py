from __future__ import annotations

import fnmatch

from pygencollection.impl import ArrayImpl
from pygencollection.utils import CollectionUtils
from pygenos.utils import OSUtils
from pygenpath.abc import Filepath
from pygentest.abc import TestSuite
from pygentestunknownfile.impl import UnknownFileTestCaseImpl
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspacePkg
from pygenworkspace.abc import WorkspacePkgsDir
from pygenworkspace.abc import WorkspaceTestCaseLoader
from pygenworkspace.abc import WorkspaceTestCaseLoaderContext


class WorkspacePkgsDirUnknownFileTestCaseImpl(UnknownFileTestCaseImpl):
    @classmethod
    def _filter(
        cls,
        pkgs_dir: WorkspacePkgsDir,
        filepath: Filepath
    ) -> bool:
        for pkg in pkgs_dir.pkgs:
            if (not pkg.dirpath.absolute.contains(filepath)):
                continue

            if (pkg.setupfile_filepath.absolute == filepath):
                return False

            if (pkg.build_dirpath.absolute.contains(filepath)):
                return False

            if (pkg.egg.info_dirpath.absolute.contains(filepath)):
                return False

            if (pkg.requirements_filepath.absolute == filepath):
                return False

            for package in pkg.packages:
                if (not package.dirpath.contains(filepath)):
                    continue

                if (package.cache_dirpath.contains(filepath)):
                    return False

                if (filepath == package.init_module_filepath):
                    return False

                for module in package.modules:
                    if (filepath == module.filepath):
                        return False

            for script in pkg.scripts:
                if (pkg.dirpath.absolute.resolve_filepath(script) == filepath):
                    return False

            return cls._package_data_filter(pkg, filepath)

        return True

    @classmethod
    def _package_data_filter(
        cls,
        pkg: WorkspacePkg,
        filepath: Filepath
    ) -> bool:
        for name, patterns in pkg.package_data.items():
            for pattern in patterns:
                full_pattern = TextUtils.format_args(
                    tpl="{}{}/{}",
                    args=[
                        pkg.lib_dirpath.absolute.path(),
                        name,
                        pattern
                    ]
                )

                if (fnmatch.fnmatch(filepath.path(), full_pattern)):
                    return False

        return True

    def __init__(
        self,
        pkgs_dir: WorkspacePkgsDir
    ) -> None:
        filepaths = CollectionUtils.filter(
            items=OSUtils.filepaths(pkgs_dir.dirs),
            fn=lambda filepath: self._filter(pkgs_dir, filepath)
        )

        super().__init__(
            filepaths=ArrayImpl.collect(filepaths)
        )


class WorkspacePkgsDirTestCaseLoaderImpl(WorkspaceTestCaseLoader):
    def load(
        self,
        test_suite: TestSuite,
        context: WorkspaceTestCaseLoaderContext
    ) -> None:
        test_suite.add_test_case(
            test_case=WorkspacePkgsDirUnknownFileTestCaseImpl(
                pkgs_dir=context.pkgs_dir
            )
        )


__all__: FinalCollection[str] = [
    "WorkspacePkgsDirTestCaseLoaderImpl"
]
