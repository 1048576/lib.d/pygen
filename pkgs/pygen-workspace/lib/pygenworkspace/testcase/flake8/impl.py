from __future__ import annotations

from collections.abc import Iterator
from typing import Final

from pygenjinja.abc import JinjaTemplate
from pygentest.abc import TestSuite
from pygentestfilecontent.impl import FileContentTestCaseImpl
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygenworkspace.abc import WorkspaceConstants
from pygenworkspace.abc import WorkspaceDirpath
from pygenworkspace.abc import WorkspaceTestCaseLoader
from pygenworkspace.abc import WorkspaceTestCaseLoaderContext


class WorkspaceFlake8CfgTestCaseImpl(FileContentTestCaseImpl):
    ...


class WorkspaceFlake8TestCaseLoaderImpl(WorkspaceTestCaseLoader):
    _template: Final[JinjaTemplate]
    _workspace_dirpath: Final[WorkspaceDirpath]

    def __init__(
        self,
        template: JinjaTemplate,
        workspace_dirpath: WorkspaceDirpath
    ) -> None:
        self._template = template
        self._workspace_dirpath = workspace_dirpath

    def load(
        self,
        test_suite: TestSuite,
        context: WorkspaceTestCaseLoaderContext
    ) -> None:
        exclude = self._exclude(context)
        paths = self._paths(context)

        test_suite.add_test_case(
            test_case=WorkspaceFlake8CfgTestCaseImpl(
                filepath=self._workspace_dirpath.absolute.resolve_filepath(
                    path=WorkspaceConstants.FLAKE8_CFG_PATH
                ),
                expected=self._template.render(
                    exclude=sorted(exclude),
                    paths=sorted(paths)
                )
            )
        )

    def _exclude(
        self,
        context: WorkspaceTestCaseLoaderContext
    ) -> Iterator[str]:
        if (context.pkgs_dir.used):
            for pkg in context.pkgs_dir.pkgs:
                yield pkg.build_dirpath.relative.path()

    def _paths(
        self,
        context: WorkspaceTestCaseLoaderContext
    ) -> Iterator[str]:
        if (context.pkgs_dir.used):
            for pkg in context.pkgs_dir.pkgs:
                for dirname in pkg.package_dir.values():
                    dirpath = pkg.dirpath.relative.resolve_dirpath(
                        path=TextUtils.format_args(
                            tpl="./{}/",
                            args=[dirname]
                        )
                    )

                    yield dirpath.path()


__all__: FinalCollection[str] = [
    "WorkspaceFlake8TestCaseLoaderImpl"
]
