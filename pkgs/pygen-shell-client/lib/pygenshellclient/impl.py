from __future__ import annotations

from collections.abc import Collection
from typing import Final

from pygenerr.err import SystemException
from pygenerr.err import UserException
from pygennet.abc import NetworkAddress
from pygenshell.abc import ShellOutput
from pygenshell.abc import ShellOutputEntry
from pygenshell.abc import ShellOutputSource
from pygenshell.abc import ShellSocket
from pygenshell.impl import ShellSocketImpl
from pygenshellclient.abc import ShellClient
from pygenshellclient.abc import ShellClientSession
from pygenshellclient.abc import ShellClientSessionContextManager
from pygensocket.abc import Socket
from pygensocket.impl import ClientSocketImpl
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class ShellClientSessionImpl(ShellClientSession):
    _socket: Final[ShellSocket]

    def __init__(self, socket: Socket) -> None:
        self._socket = ShellSocketImpl(socket)

    def call(
        self,
        args: Collection[str],
        output: ShellOutput
    ) -> None:
        for arg in args:
            self._socket.send(self._socket.Marker.ARG, arg)

        self._socket.send(self._socket.Marker.RUN, "")

        while (True):
            marker, data = self._socket.receive()

            if (marker == self._socket.Marker.STDOUT):
                output.write(
                    entry=ShellOutputEntry(
                        source=ShellOutputSource.STDOUT,
                        data=data
                    )
                )

                continue
            elif (marker == self._socket.Marker.STDERR):
                output.write(
                    entry=ShellOutputEntry(
                        source=ShellOutputSource.STDERR,
                        data=data
                    )
                )

                continue
            elif (marker == self._socket.Marker.STATUS):
                ...
            else:
                raise SystemException()

            if (data != "0"):
                raise UserException(
                    msg=TextUtils.format_args(
                        tpl="Return code [{}]",
                        args=[
                            data
                        ]
                    )
                )
            else:
                return


class ShellClientSessionContextManagerImpl(ShellClientSessionContextManager):
    _socket: Final[Socket]

    def __init__(self, socket: Socket) -> None:
        self._socket = socket

    def __enter__(self) -> ShellClientSession:
        return ShellClientSessionImpl(self._socket)

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        self._socket.close()


class ShellClientImpl(ShellClient):
    def connect(
        self,
        address: NetworkAddress
    ) -> ShellClientSessionContextManager:
        socket = ClientSocketImpl()

        try:
            socket.connect(address)
        except ConnectionRefusedError as e:
            raise UserException(e.strerror)

        return ShellClientSessionContextManagerImpl(socket)


__all__: FinalCollection[str] = [
    "ShellClientImpl"
]
