from __future__ import annotations

from typing import Protocol

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection


class IPSet(Protocol):
    def intersection(self, other: IPSet) -> IPSet:
        raise SystemException()

    def union(self, other: IPSet) -> IPSet:
        raise SystemException()


__all__: FinalCollection[str] = [
    "IPSet"
]
