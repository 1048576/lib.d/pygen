from __future__ import annotations

from typing import Final
from typing import Protocol

from pygenerr.err import SystemException
from pygentype.abc import CovariantT
from pygentype.abc import FinalCollection
from pygentype.abc import T


class Procedure(Protocol[CovariantT]):
    def __call__(self) -> CovariantT:
        raise SystemException()


FinalProcedure = Final[Procedure[T]]


__all__: FinalCollection[str] = [
    "Procedure",
    "FinalProcedure"
]
