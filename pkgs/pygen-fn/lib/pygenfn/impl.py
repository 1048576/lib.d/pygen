from __future__ import annotations

from typing import Any

from pygenfn.abc import Procedure
from pygentype.abc import FinalCollection


class ProcedureInstance(Procedure[Any]):
    ...


__all__: FinalCollection[str] = [
    "ProcedureInstance"
]
