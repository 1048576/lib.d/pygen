from __future__ import annotations

from collections.abc import Iterable
from collections.abc import Iterator
from dataclasses import dataclass
from typing import Final
from typing import Protocol

from pygencollection.abc import FinalDict
from pygendataclass.abc import DataclassInstance
from pygenerr.err import SystemException
from pygentype.abc import FinalCollection


@dataclass
class Flake8CheckerModule(object):
    exported_symbols: FinalCollection[str]


@dataclass
class Flake8CheckerCache(object):
    modules: FinalDict[str, Flake8CheckerModule]


@dataclass
class Flake8ReportEntry(DataclassInstance, Iterable[object]):
    line: Final[int]
    column: Final[int]
    text: Final[str]


class Flake8Report(Protocol):
    def __iter__(self) -> Iterator[Flake8ReportEntry]:
        raise SystemException()


class Flake8Checker(Protocol):
    def run(self) -> Flake8Report:
        raise SystemException()


__all__: FinalCollection[str] = [
    "Flake8Checker",
    "Flake8CheckerCache",
    "Flake8CheckerModule",
    "Flake8Report",
    "Flake8ReportEntry"
]
