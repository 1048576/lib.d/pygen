from __future__ import annotations

from types import ModuleType
from typing import Protocol

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection


class JinjaTemplate(object):
    def render(self, **kwargs: object) -> str:
        raise SystemException()


class JinjaTemplateLoader(Protocol):
    def load(
        self,
        module: ModuleType,
        name: str,
        keep_trailing_newline: bool = True
    ) -> JinjaTemplate:
        raise SystemException()


__all__: FinalCollection[str] = [
    "JinjaTemplate",
    "JinjaTemplateLoader"
]
