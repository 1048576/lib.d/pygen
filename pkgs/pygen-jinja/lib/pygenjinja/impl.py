from __future__ import annotations

from types import ModuleType
from typing import Final

import jinja2

from pygenjinja.abc import JinjaTemplate
from pygenjinja.abc import JinjaTemplateLoader
from pygenpath.utils import PathUtils
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class JinjaTemplateImpl(JinjaTemplate):
    _template: Final[jinja2.Template]

    def __init__(self, template: jinja2.Template) -> None:
        self._template = template

    def render(self, **kwargs: object) -> str:
        return self._template.render(**kwargs)


class JinjaTemplateLoaderImpl(JinjaTemplateLoader):
    def load(
        self,
        module: ModuleType,
        name: str,
        keep_trailing_newline: bool = True
    ) -> JinjaTemplate:
        dirpath = PathUtils.dirpath(
            path=PathUtils.normalize(
                path=TextUtils.format_args(
                    tpl="{}/../",
                    args=[
                        module.__file__
                    ]
                )
            )
        )
        filepath = dirpath.resolve_filepath(
            path=TextUtils.format_args(
                tpl="./{}.j2",
                args=[
                    name
                ]
            )
        )

        with (open(filepath.path(), "r") as f):
            return JinjaTemplateImpl(
                template=jinja2.Template(
                    source=f.read(),
                    keep_trailing_newline=keep_trailing_newline
                )
            )


__all__: FinalCollection[str] = [
    "JinjaTemplateLoaderImpl"
]
