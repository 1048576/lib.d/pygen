from __future__ import annotations

from pygenerr.err import UserException
from pygennet.abc import NetworkAddress
from pygennet.impl import NetworkAddressImpl
from pygentype.abc import FinalCollection


class NetworkUtils(object):
    @classmethod
    def address(cls, value: str) -> NetworkAddress:
        try:
            host, port = tuple(value.split(":"))
        except ValueError:
            raise UserException("Invalid address")

        try:
            return NetworkAddressImpl(host, int(port))
        except ValueError:
            raise UserException("Invalid port")


__all__: FinalCollection[str] = [
    "NetworkUtils"
]
