from __future__ import annotations

import json

from collections.abc import Iterable

from pygendataclass.abc import DataclassInstance
from pygendataclass.utils import DataclassUtils
from pygentype.abc import FinalCollection


class TextUtils(object):
    @classmethod
    def encode(
        cls,
        text: str
    ) -> bytes:
        return text.encode()

    @classmethod
    def endswith(cls, text: str, suffix: str) -> bool:
        return text.endswith(suffix)

    @classmethod
    def format(
        cls,
        tpl: str,
        args: DataclassInstance
    ) -> str:
        return tpl.format(**DataclassUtils.pairs(args))

    @classmethod
    def format_args(
        cls,
        tpl: str,
        args: Iterable[object]
    ) -> str:
        return tpl.format(*args)

    @classmethod
    def format_kwargs(
        cls,
        tpl: str,
        **kwargs: object
    ) -> str:
        return tpl.format(**kwargs)

    @classmethod
    def join(cls, delimiter: str, items: Iterable[str]) -> str:
        return delimiter.join(items)

    @classmethod
    def replace(
        cls,
        text: str,
        old: str,
        new: str,
        count: int = -1
    ) -> str:
        return text.replace(old, new, count)

    @classmethod
    def startswith(cls, text: str, prefix: str) -> bool:
        return text.startswith(prefix)

    @classmethod
    def to_safe_text(cls, instance: object) -> str:
        return json.dumps(instance)


__all__: FinalCollection[str] = [
    "TextUtils"
]
