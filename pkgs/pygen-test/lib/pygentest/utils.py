from __future__ import annotations

from pygentype.abc import FinalCollection


class TestUtils(object):
    @classmethod
    def raise_exception(cls, exception: Exception) -> None:
        raise exception


__all__: FinalCollection[str] = [
    "TestUtils"
]
