from __future__ import annotations

from collections.abc import Iterator
from typing import Final

from pygencollection.abc import Array
from pygencollection.impl import ArrayImpl
from pygencollection.utils import CollectionUtils
from pygenos.abc import OSDir
from pygenpath.abc import Dirpath
from pygenpath.abc import Filepath
from pygenpy.abc import PyModule
from pygenpy.abc import PyPackage
from pygenregex.abc import RegexPattern
from pygenregex.utils import RegexUtils
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class PyUtils(object):
    CACHE_DIRNAME: Final[str] = "__pycache__"
    INIT_MODULE_FILENAME: Final[str] = "__init__.py"
    MODULE_NAME_RE: Final[RegexPattern] = RegexUtils.compile(
        pattern=r"^([a-z]+).py$"
    )
    PACKAGE_NAME_RE: Final[RegexPattern] = RegexUtils.compile(
        pattern=r"^(?:[a-z][a-z0-8]*)(?:\.[a-z][a-z0-8]*)*$"
    )

    @classmethod
    def find_packages(
        cls,
        dirs: Array[OSDir],
        dirpath: Dirpath
    ) -> Iterator[PyPackage]:
        for dir in dirs:
            if (dir.dirpath == dirpath):
                break
        else:
            return

        for dirname in dir.dirnames:
            packages = cls._find_packages(
                dirs=dirs,
                package_name=dirname,
                package_dirpath=dir.dirpath.resolve_dirpath(
                    path=TextUtils.format_args(
                        tpl="./{}/",
                        args=[
                            dirname
                        ]
                    )
                )
            )

            for package in packages:
                yield package

    @classmethod
    def _find_modules(
        cls,
        package_dir: OSDir
    ) -> Iterator[PyModule]:
        for filename in package_dir.filenames:
            result = cls.MODULE_NAME_RE.match(filename)

            if (result is None):
                continue
            else:
                module_name = result.group(1)
                module_filepath = package_dir.dirpath.resolve_filepath(
                    path=TextUtils.format_args(
                        tpl="./{}",
                        args=[
                            filename
                        ]
                    )
                )

            yield cls._load_module(
                module_name=module_name,
                module_filepath=module_filepath
            )

    @classmethod
    def _find_packages(
        cls,
        dirs: Array[OSDir],
        package_name: str,
        package_dirpath: Dirpath
    ) -> Iterator[PyPackage]:
        if (cls.PACKAGE_NAME_RE.match(package_name) is None):
            return

        package_dirs = ArrayImpl.collect(
            iterator=CollectionUtils.filter(
                items=dirs,
                fn=lambda dir: package_dirpath.contains(dir.dirpath)
            )
        )

        for package_dir in package_dirs:
            if (package_dir.dirpath != package_dirpath):
                continue

            if (cls.INIT_MODULE_FILENAME not in package_dir.filenames):
                continue

            yield cls._load_package(
                package_name=package_name,
                package_dir=package_dir
            )

            for dirname in package_dir.dirnames:
                packages = cls._find_packages(
                    dirs=package_dirs,
                    package_name=TextUtils.join(
                        delimiter=".",
                        items=[
                            package_name,
                            dirname
                        ]
                    ),
                    package_dirpath=package_dir.dirpath.resolve_dirpath(
                        path=TextUtils.format_args(
                            tpl="./{}/",
                            args=[
                                dirname
                            ]
                        )
                    )
                )

                for package in packages:
                    yield package

            return

    @classmethod
    def _load_module(
        cls,
        module_name: str,
        module_filepath: Filepath
    ) -> PyModule:
        return PyModule(
            name=module_name,
            filepath=module_filepath
        )

    @classmethod
    def _load_package(
        cls,
        package_name: str,
        package_dir: OSDir
    ) -> PyPackage:
        modules = ArrayImpl.collect(
            iterator=cls._find_modules(
                package_dir=package_dir
            )
        )

        init_module_filepath = package_dir.dirpath.resolve_filepath(
            path=TextUtils.format_args(
                tpl="./{}",
                args=[
                    cls.INIT_MODULE_FILENAME
                ]
            )
        )

        cache_dirpath = package_dir.dirpath.resolve_dirpath(
            path=TextUtils.format_args(
                tpl="./{}/",
                args=[
                    cls.CACHE_DIRNAME
                ]
            )
        )

        return PyPackage(
            name=package_name,
            dirpath=package_dir.dirpath,
            modules=modules,
            init_module_filepath=init_module_filepath,
            cache_dirpath=cache_dirpath
        )


__all__: FinalCollection[str] = [
    "PyUtils"
]
