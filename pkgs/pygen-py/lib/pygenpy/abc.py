from __future__ import annotations

from dataclasses import dataclass
from typing import Final

from pygenpath.abc import Dirpath
from pygenpath.abc import Filepath
from pygentype.abc import FinalCollection


@dataclass
class PyModule(object):
    name: Final[str]
    filepath: Final[Filepath]


@dataclass
class PyPackage(object):
    name: Final[str]
    dirpath: Final[Dirpath]
    init_module_filepath: Final[Filepath]
    modules: FinalCollection[PyModule]
    cache_dirpath: Final[Dirpath]


__all__: FinalCollection[str] = [
    "PyModule",
    "PyPackage"
]
