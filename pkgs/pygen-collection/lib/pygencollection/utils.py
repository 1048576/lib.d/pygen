from __future__ import annotations

import itertools

from collections.abc import Callable
from collections.abc import Collection
from collections.abc import Iterable
from collections.abc import Iterator
from collections.abc import Mapping

from pygencollection.abc import Dict
from pygencollection.abc import IterableInstance
from pygencollection.abc import List
from pygencollection.abc import NamedPair
from pygencollection.impl import DictImpl
from pygencollection.impl import ListImpl
from pygencollection.impl import NamedPairImpl
from pygencollection.impl import OnceIterableInstanceImpl
from pygentype.abc import KT
from pygentype.abc import T0
from pygentype.abc import T1
from pygentype.abc import VT
from pygentype.abc import FinalCollection
from pygentype.abc import Pair
from pygentype.abc import T
from pygentype.utils import TypeUtils


class CollectionUtils(object):
    @classmethod
    def any(
        cls,
        items: Iterable[T],
        fn: Callable[[T], bool]
    ) -> bool:
        return any(fn(item) for item in items)

    @classmethod
    def chain(
        cls,
        links: Iterable[Iterable[T]]
    ) -> Iterator[T]:
        return itertools.chain(*links)

    @classmethod
    def collection_eq(
        cls,
        a: Collection[T],
        b: Collection[T]
    ) -> bool:
        if (len(a) != len(b)):
            return False

        it_a = iter(a)
        it_b = iter(b)

        while (True):
            try:
                if (next(it_a) != next(it_b)):
                    return False
            except StopIteration:
                return True

    @classmethod
    def contains(
        cls,
        items: Iterable[T],
        item: T
    ) -> bool:
        return cls.any(
            items=items,
            fn=lambda testable: TypeUtils.eq(testable, item)
        )

    @classmethod
    def difference(
        cls,
        a: Iterable[T0],
        b: Iterable[T0],
        fn: Callable[[frozenset[T0]], T1]
    ) -> T1:
        return fn(frozenset(a).difference(b))

    @classmethod
    def filter(
        cls,
        items: Iterable[T],
        fn: Callable[[T], bool]
    ) -> Iterator[T]:
        return filter(fn, items)

    @classmethod
    def head(
        cls,
        items: Iterable[T],
        size: int
    ) -> Iterator[T]:
        index = 0

        for item in items:
            if (index < size):
                index += 1

                yield item
            else:
                return

    @classmethod
    def index(
        cls,
        items: Iterable[T],
        fn: Callable[[T], bool]
    ) -> int:
        index = -1

        for item in items:
            index += 1

            if (fn(item)):
                break

        return index

    @classmethod
    def items(
        cls,
        collection: Iterable[T]
    ) -> Iterator[Pair[int, T]]:
        index = 0

        for value in collection:
            yield (index, value)

            index += 1

    @classmethod
    def join_by_index(
        cls,
        first: Iterable[T0],
        second: Iterable[T1]
    ) -> Iterator[Pair[T0, T1]]:
        first_iterator = iter(first)
        second_iterator = iter(second)

        try:
            while (True):
                yield (next(first_iterator), next(second_iterator))
        except StopIteration:
            return

    @classmethod
    def map(
        cls,
        items: Iterable[T0],
        fn: Callable[[T0], T1]
    ) -> Iterator[T1]:
        return map(fn, items)

    @classmethod
    def pairs(
        cls,
        map: Mapping[KT, VT]
    ) -> Iterator[NamedPair[KT, VT]]:
        return cls.map(
            items=map.items(),
            fn=lambda pair: NamedPairImpl(*pair)
        )

    @classmethod
    def to_dict(
        cls,
        items: Iterable[Pair[KT, VT]]
    ) -> Dict[KT, VT]:
        return DictImpl(items)

    @classmethod
    def to_iterable_instance(
        cls,
        iterator: Iterator[T]
    ) -> IterableInstance[T]:
        return OnceIterableInstanceImpl(iterator)

    @classmethod
    def to_list(
        cls,
        items: Iterable[T]
    ) -> List[T]:
        return ListImpl(iter(items))

    @classmethod
    def union(
        cls,
        a: Iterable[T0],
        b: Iterable[T0],
        fn: Callable[[frozenset[T0]], T1]
    ) -> T1:
        return fn(frozenset(a).union(b))


__all__: FinalCollection[str] = [
    "CollectionUtils"
]
