from __future__ import annotations

from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class SystemException(Exception):
    def __init__(
        self,
        msg: str = "System exception"
    ) -> None:
        super().__init__(msg)


class UserException(Exception):
    def __init__(self, msg: str) -> None:
        super().__init__(msg)

    def __eq__(self, other: object) -> bool:
        if (self.__class__ != other.__class__):
            return False

        return (str(self) == str(other))

    def wrap(self, title: str) -> Exception:
        return self.__class__(
            msg=TextUtils.format_args(
                tpl="{}:\n  {}",
                args=[
                    title,
                    str(self).replace("\n", "\n  ")
                ]
            )
        )


__all__: FinalCollection[str] = [
    "SystemException",
    "UserException"
]
