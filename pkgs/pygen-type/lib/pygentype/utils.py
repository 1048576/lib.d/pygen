from __future__ import annotations

from pygentype.abc import FinalCollection


class TypeUtils(object):
    @classmethod
    def eq(cls, a: object, b: object) -> bool:
        if (a.__class__ != b.__class__):
            return False

        if (a != b):
            return False

        return True


__all__: FinalCollection[str] = [
    "TypeUtils"
]
