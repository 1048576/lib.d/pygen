from __future__ import annotations

from collections.abc import Collection
from collections.abc import Iterator
from collections.abc import Mapping
from collections.abc import Sequence
from typing import Annotated
from typing import Final
from typing import TypeVar

KT = TypeVar("KT")
VT = TypeVar("VT")


T = TypeVar("T")
T0 = TypeVar("T0")
T1 = TypeVar("T1")
T2 = TypeVar("T2")
T3 = TypeVar("T3")


CovariantT = TypeVar(
    name="CovariantT",
    covariant=True
)


Class = type[object]
Mutable = Annotated[T, "Mutable"]
Optional = T | None
Pair = tuple[KT, VT]
Range = range
Scalar = str | int
Set = set[T]
Type = type[T]
Union2 = T0 | T1
Union3 = T0 | T1 | T2
Union4 = T0 | T1 | T2 | T3


ScalarT = TypeVar(
    name="ScalarT",
    bound=Scalar
)


MutableOptional = Mutable[Optional[T]]


FinalCollection = Final[Collection[T]]
FinalIterator = Final[Iterator[T]]
FinalMapping = Final[Mapping[KT, VT]]
FinalOptional = Final[Optional[T]]
FinalSequence = Final[Sequence[T]]
FinalSet = Final[Set[T]]
FinalType = Final[Type[T]]


__all__: FinalCollection[str] = [
    "Class",
    "CovariantT",
    "FinalCollection",
    "FinalIterator",
    "FinalMapping",
    "FinalOptional",
    "FinalSequence",
    "FinalType",
    "KT",
    "Mutable",
    "MutableOptional",
    "Optional",
    "Pair",
    "Range",
    "Scalar",
    "ScalarT",
    "Set",
    "T",
    "T0",
    "T1",
    "T2",
    "Type",
    "Union2",
    "Union4",
    "VT"
]
