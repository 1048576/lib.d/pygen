from __future__ import annotations

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection


class UniqueRandomIntGenerator(object):
    def randint(self) -> int:
        raise SystemException()


__all__: FinalCollection[str] = [
    "UniqueRandomIntGenerator"
]
