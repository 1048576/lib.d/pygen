from __future__ import annotations

from typing import Final

from pygenrandom.abc import UniqueRandomGenerator
from pygenrandom.impl import UniqueRandomGeneratorImpl
from pygenrandom.randint.abc import UniqueRandomIntGenerator
from pygenrandom.utils import RandomUtils
from pygentype.abc import FinalCollection


class UniqueRandomIntGeneratorImpl(UniqueRandomIntGenerator):
    _generator: Final[UniqueRandomGenerator[int]]

    def __init__(self, retry_limit: int = 10) -> None:
        self._generator = UniqueRandomGeneratorImpl(
            retry_limit=retry_limit,
            equal_fn=lambda a, b: (a == b)
        )

    def randint(self) -> int:
        return self._generator.randval(
            randval_fn=lambda: RandomUtils.randint()
        )


__all__: FinalCollection[str] = [
    "UniqueRandomIntGeneratorImpl"
]
