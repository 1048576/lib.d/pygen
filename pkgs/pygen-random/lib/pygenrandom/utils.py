from __future__ import annotations

import random

from io import StringIO

from pygencollection.abc import Array
from pygenrandom.abc import RandomDefaults
from pygentype.abc import FinalCollection
from pygentype.abc import T


class RandomUtils(object):
    @classmethod
    def choice(
        cls,
        values: Array[T]
    ) -> T:
        return random.choice(values)

    @classmethod
    def randfloat(
        cls,
        a: int = 0,
        b: int = 1
    ) -> float:
        return random.uniform(a, b)

    @classmethod
    def randint(
        cls,
        a: int = RandomDefaults.MIN_INT,
        b: int = RandomDefaults.MAX_INT
    ) -> int:
        return random.randint(a, b)

    @classmethod
    def randtext(
        cls,
        alphabet: str = RandomDefaults.TEXT_ALPHABET,
        size: int = RandomDefaults.TEXT_SIZE
    ) -> str:
        buffer = StringIO()

        for _ in range(0, size):
            buffer.write(random.choice(alphabet))

        return buffer.getvalue()


__all__: FinalCollection[str] = [
    "RandomUtils"
]
