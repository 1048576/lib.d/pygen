from __future__ import annotations

import string

from collections.abc import Callable
from typing import Final
from typing import Generic

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection
from pygentype.abc import T


class RandomDefaults(object):
    TEXT_ALPHABET: Final[str] = string.ascii_lowercase
    TEXT_SIZE: Final[int] = 10

    MIN_INT: Final[int] = -0xffff
    MAX_INT: Final[int] = 0xffff


class UniqueRandomGenerator(Generic[T]):
    def randval(
        self,
        randval_fn: Callable[[], T]
    ) -> T:
        raise SystemException()


FinalUniqueRandomGenerator = Final[UniqueRandomGenerator[T]]


__all__: FinalCollection[str] = [
    "RandomDefaults",
    "UniqueRandomGenerator"
]
