from __future__ import annotations

from typing import Final

from pygenrandom.abc import RandomDefaults
from pygenrandom.abc import UniqueRandomGenerator
from pygenrandom.impl import UniqueRandomGeneratorImpl
from pygenrandom.randtext.abc import UniqueRandomTextGenerator
from pygenrandom.utils import RandomUtils
from pygentype.abc import FinalCollection


class UniqueRandomTextGeneratorImpl(UniqueRandomTextGenerator):
    _generator: Final[UniqueRandomGenerator[str]]

    def __init__(self, retry_limit: int = 10) -> None:
        self._generator = UniqueRandomGeneratorImpl(
            retry_limit=retry_limit,
            equal_fn=lambda a, b: a == b
        )

    def randtext(
        self,
        alphabet: str = RandomDefaults.TEXT_ALPHABET,
        size: int = RandomDefaults.TEXT_SIZE
    ) -> str:
        return self._generator.randval(
            randval_fn=lambda: RandomUtils.randtext(alphabet, size)
        )


__all__: FinalCollection[str] = [
    "UniqueRandomTextGeneratorImpl"
]
