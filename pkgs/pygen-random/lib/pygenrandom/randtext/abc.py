from __future__ import annotations

from typing import Protocol

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection


class UniqueRandomTextGenerator(Protocol):
    def randtext(
        self,
        alphabet: str = ...,
        size: int = ...
    ) -> str:
        raise SystemException()


__all__: FinalCollection[str] = [
    "UniqueRandomTextGenerator"
]
