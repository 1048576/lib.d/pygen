from __future__ import annotations

from collections.abc import Callable
from typing import Final
from typing import Generic
from typing import Protocol

from pygencollection.abc import FinalList
from pygencollection.impl import ListImpl
from pygencollection.utils import CollectionUtils
from pygenerr.err import SystemException
from pygenrandom.abc import UniqueRandomGenerator
from pygentype.abc import FinalCollection
from pygentype.abc import T


class UniqueRandomGeneratorImpl(Generic[T], UniqueRandomGenerator[T]):
    class EqualFn(Protocol):
        def __call__(self, a: T, b: T) -> bool:
            raise SystemException()

    _values: FinalList[T]
    _retry_limit: Final[int]
    _equal_fn: Final[EqualFn]

    def __init__(
        self,
        retry_limit: int,
        equal_fn: EqualFn
    ) -> None:
        self._values = ListImpl()
        self._retry_limit = retry_limit
        self._equal_fn = equal_fn

    def randval(
        self,
        randval_fn: Callable[[], T]
    ) -> T:
        for _ in range(0, self._retry_limit):
            new_value = randval_fn()
            is_new_value_exists = CollectionUtils.any(
                items=self._values,
                fn=lambda value: self._equal_fn(new_value, value)
            )

            if (not is_new_value_exists):
                self._values.add(new_value)

                return new_value

        raise SystemException()


__all__: FinalCollection[str] = [
    "UniqueRandomGeneratorImpl"
]
