from __future__ import annotations

from typing import Final

from pygencollection.impl import ArrayImpl
from pygencollection.utils import CollectionUtils
from pygenerr.err import SystemException
from pygenos.utils import OSUtils
from pygenpath.abc import Dirpath
from pygentest.abc import TestSuite
from pygentestsample.abc import SampleTestCaseLoader
from pygentestsample.abc import SampleTestCreateTestCaseFn
from pygentestunknowndir.impl import UnknownDirTestCaseImpl
from pygentestunknownfile.impl import UnknownFileTestCaseImpl
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class SampleTestCaseLoaderImpl(SampleTestCaseLoader):
    _dirpath: Final[Dirpath]
    _create_test_case_fn: Final[SampleTestCreateTestCaseFn]

    def __init__(
        self,
        dirpath: Dirpath,
        create_test_case_fn: SampleTestCreateTestCaseFn
    ) -> None:
        self._dirpath = dirpath
        self._create_test_case_fn = create_test_case_fn

    def load(self, test_suite: TestSuite) -> None:
        dirs = ArrayImpl.collect(
            iterator=OSUtils.walk(self._dirpath)
        )

        for dir in dirs:
            if (dir.dirpath == self._dirpath):
                root_dir = dir

                break
        else:
            raise SystemException()

        data_dirs = ArrayImpl.collect(
            iterator=CollectionUtils.filter(
                items=CollectionUtils.filter(
                    items=dirs,
                    fn=lambda dir: any(dir.filenames)
                ),
                fn=lambda dir: (dir.dirpath != self._dirpath)
            )
        )

        sample_dirs = ArrayImpl.collect(
            iterator=CollectionUtils.filter(
                items=data_dirs,
                fn=lambda dir:
                    (dir.dirpath.resolve_dirpath("../") == self._dirpath)
            )
        )

        test_suite.add_test_case(
            test_case=UnknownFileTestCaseImpl(
                filepaths=CollectionUtils.map(
                    items=root_dir.filenames,
                    fn=lambda filename: root_dir.dirpath.resolve_filepath(
                        path=TextUtils.format_args(
                            tpl="./{}",
                            args=[
                                filename
                            ]
                        )
                    )
                )
            )
        )

        test_suite.add_test_case(
            test_case=UnknownDirTestCaseImpl(
                dirpaths=CollectionUtils.map(
                    items=CollectionUtils.filter(
                        items=data_dirs,
                        fn=lambda dir: (dir not in sample_dirs)
                    ),
                    fn=lambda dir: dir.dirpath
                )
            )
        )

        for dirname in root_dir.dirnames:
            dirpath = root_dir.dirpath.resolve_dirpath(
                path=TextUtils.format_args(
                    tpl="./{}/",
                    args=[
                        dirname
                    ]
                )
            )

            for sample_dir in sample_dirs:
                if (sample_dir.dirpath == dirpath):
                    test_suite.add_test_case(
                        test_case=self._create_test_case_fn(
                            dirname=dirname,
                            dir=sample_dir
                        )
                    )

                    break


__all__: FinalCollection[str] = [
    "SampleTestCaseLoaderImpl"
]
