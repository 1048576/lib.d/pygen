from __future__ import annotations

from typing import Protocol

from pygenerr.err import SystemException
from pygenos.abc import OSDir
from pygentest.abc import TestCase
from pygentest.abc import TestSuite
from pygentype.abc import FinalCollection


class SampleTestCreateTestCaseFn(Protocol):
    def __call__(
        self,
        dirname: str,
        dir: OSDir
    ) -> TestCase:
        raise SystemException()


class SampleTestCaseLoader(object):
    def load(
        self,
        test_suite: TestSuite
    ) -> None:
        raise SystemException()


__all__: FinalCollection[str] = [
    "SampleTestCreateTestCaseFn",
    "SampleTestCaseLoader"
]
