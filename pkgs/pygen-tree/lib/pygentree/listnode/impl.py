from __future__ import annotations

from collections.abc import Iterable
from dataclasses import dataclass
from typing import Final

from pygencollection.abc import FinalArray
from pygencollection.impl import ArrayImpl
from pygencounter.abc import Counter
from pygencounter.impl import CounterImpl
from pygentext.utils import TextUtils
from pygentree.abc import TreeListNode
from pygentree.abc import TreeListNodeContext
from pygentree.abc import TreeMapper
from pygentree.abc import TreeObjectNode
from pygentree.err import TreeUnexpectedNodesException
from pygentype.abc import FinalCollection


class TreeListNodeImpl(TreeListNodeContext, TreeListNode):
    @dataclass
    class ContainerItem(object):
        path: Final[str]
        container: Final[object]

    _mapper: Final[TreeMapper]
    _path: Final[str]
    _container: FinalArray[object]
    _index: Final[Counter]

    def __init__(
        self,
        mapper: TreeMapper,
        path: str,
        container: Iterable[object]
    ) -> None:
        self._mapper = mapper
        self._path = path
        self._container = ArrayImpl.copy(container)
        self._index = CounterImpl()

    def __enter__(self) -> TreeListNodeContext:
        return self

    def __eq__(self, __o: object) -> bool:
        if (not isinstance(__o, TreeListNodeImpl)):
            return False

        if (self._path != __o._path):
            return False

        if (self._container != __o._container):
            return False

        return True

    def __exit__(
        self,
        type: object,
        value: object,
        traceback: object
    ) -> None:
        if (value is not None):
            return

        if (self.has_more_nodes()):
            raise TreeUnexpectedNodesException.create(
                nodes=TextUtils.format_args(
                    tpl="{}[{}:{}]",
                    args=[
                        self.path(),
                        self._index,
                        len(self._container) - 1
                    ]
                )
            )

    def detach_object_node(self) -> TreeObjectNode:
        container_item = self._detach_container_item()

        return self._mapper.create_object_node(
            path=container_item.path,
            container=container_item.container
        )

    def has_more_nodes(self) -> bool:
        return (self._index.value() < len(self._container))

    def path(self) -> str:
        return self._path

    def _detach_container_item(self) -> ContainerItem:
        path = TextUtils.format_args(
            tpl="{}[{}]",
            args=[
                self._path,
                self._index
            ]
        )
        container = self._container[self._index.inc()]

        return self.ContainerItem(path, container)


__all__: FinalCollection[str] = [
    "TreeListNodeImpl"
]
