from __future__ import annotations

from collections.abc import Sequence
from contextlib import AbstractContextManager

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection


class TreeListNodeContext(object):
    def detach_object_node(self) -> TreeObjectNode:
        raise SystemException()

    def has_more_nodes(self) -> bool:
        raise SystemException()


class TreeListNode(AbstractContextManager[TreeListNodeContext]):
    def __enter__(self) -> TreeListNodeContext:
        raise SystemException()

    def __exit__(
        self,
        type: object,
        value: object,
        traceback: object
    ) -> None:
        raise SystemException()

    def path(self) -> str:
        raise SystemException()


class TreeObjectNodeContext(object):
    def detach_bool_node(
        self,
        name: str
    ) -> bool:
        raise SystemException()

    def detach_float_node(
        self,
        name: str
    ) -> float:
        raise SystemException()

    def detach_int_node(
        self,
        name: str
    ) -> int:
        raise SystemException()

    def detach_list_node(
        self,
        name: str
    ) -> TreeListNode:
        raise SystemException()

    def detach_object_node(
        self,
        name: str
    ) -> TreeObjectNode:
        raise SystemException()

    def detach_text_node(
        self,
        name: str
    ) -> str:
        raise SystemException()


class TreeObjectNode(AbstractContextManager[TreeObjectNodeContext]):
    def __enter__(self) -> TreeObjectNodeContext:
        raise SystemException()

    def __exit__(
        self,
        type: object,
        value: object,
        traceback: object
    ) -> None:
        raise SystemException()

    def names(self) -> Sequence[str]:
        raise SystemException()

    def path(self) -> str:
        raise SystemException()


class TreeMapper(object):
    def create_list_node(self, path: str, container: object) -> TreeListNode:
        raise SystemException()

    def create_object_node(
        self,
        path: str,
        container: object
    ) -> TreeObjectNode:
        raise SystemException()


class Tree(object):
    def create_list_node(self, container: object) -> TreeListNode:
        raise SystemException()

    def create_object_node(self, container: object) -> TreeObjectNode:
        raise SystemException()


__all__: FinalCollection[str] = [
    "Tree",
    "TreeListNode",
    "TreeListNodeContext",
    "TreeMapper",
    "TreeObjectNode",
    "TreeObjectNodeContext"
]
