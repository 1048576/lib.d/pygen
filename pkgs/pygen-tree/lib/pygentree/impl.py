from __future__ import annotations

from collections.abc import Iterator
from typing import Final

from pygencollection.abc import MappingInstance
from pygencollection.abc import SequenceInstance
from pygencollection.impl import ArrayImpl
from pygencollection.utils import CollectionUtils
from pygentext.utils import TextUtils
from pygentree.abc import Tree
from pygentree.abc import TreeListNode
from pygentree.abc import TreeMapper
from pygentree.abc import TreeObjectNode
from pygentree.err import TreeInvalidNodeTypeException
from pygentree.listnode.impl import TreeListNodeImpl
from pygentree.objectnode.impl import TreeObjectNodeImpl
from pygentype.abc import FinalCollection
from pygentype.abc import Pair


class TreeMapperImpl(TreeMapper):
    def create_list_node(self, path: str, container: object) -> TreeListNode:
        if (isinstance(container, SequenceInstance)):
            return TreeListNodeImpl(
                mapper=self,
                path=path,
                container=ArrayImpl.copy(container)
            )
        else:
            raise TreeInvalidNodeTypeException.create(
                path=path,
                expected_type=list.__name__,
                actual_type=type(container).__name__
            )

    def create_object_node(
        self,
        path: str,
        container: object
    ) -> TreeObjectNode:
        return TreeObjectNodeImpl(
            mapper=self,
            path=path,
            container=CollectionUtils.to_dict(
                items=self._dict_container(path, container)
            )
        )

    def _dict_container(
        self,
        path: str,
        container: object
    ) -> Iterator[Pair[str, object]]:
        if (isinstance(container, MappingInstance)):
            for k, v in container.items():
                if (isinstance(k, str)):
                    yield (k, v)
                else:
                    raise TreeInvalidNodeTypeException(
                        msg=TextUtils.format_args(
                            tpl="Invalid node name type. [{}] instead of str",
                            args=[
                                type(k).__name__
                            ]
                        )
                    )
        else:
            raise TreeInvalidNodeTypeException.create(
                path=path,
                expected_type=dict.__name__,
                actual_type=type(container).__name__
            )


class TreeImpl(Tree):
    _MAPPER: Final[TreeMapper] = TreeMapperImpl()
    _ROOT_PATH: Final[str] = "$"

    def create_list_node(self, container: object) -> TreeListNode:
        return self._MAPPER.create_list_node(self._ROOT_PATH, container)

    def create_object_node(self, container: object) -> TreeObjectNode:
        return self._MAPPER.create_object_node(self._ROOT_PATH, container)


__all__: FinalCollection[str] = [
    "TreeImpl",
    "TreeMapperImpl"
]
