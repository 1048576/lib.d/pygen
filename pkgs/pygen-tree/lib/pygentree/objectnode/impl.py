from __future__ import annotations

from collections.abc import Mapping
from collections.abc import Sequence
from typing import Final
from typing import NoReturn

from pygencollection.abc import FinalDict
from pygencollection.impl import DictImpl
from pygentext.utils import TextUtils
from pygentree.abc import TreeListNode
from pygentree.abc import TreeMapper
from pygentree.abc import TreeObjectNode
from pygentree.abc import TreeObjectNodeContext
from pygentree.err import TreeInvalidNodeTypeException
from pygentree.err import TreeNodeNotFoundException
from pygentree.err import TreeUnexpectedNodesException
from pygentype.abc import FinalCollection
from pygentype.abc import Type


class TreeObjectNodeImpl(TreeObjectNodeContext, TreeObjectNode):
    _mapper: Final[TreeMapper]
    _path: Final[str]
    _container: FinalDict[str, object]

    def __init__(
        self,
        mapper: TreeMapper,
        path: str,
        container: Mapping[str, object]
    ) -> None:
        self._mapper = mapper
        self._path = path
        self._container = DictImpl.as_mapping(container)

    def __enter__(self) -> TreeObjectNodeContext:
        return self

    def __eq__(self, __o: object) -> bool:
        if (not isinstance(__o, TreeObjectNodeImpl)):
            return False

        if (self._path != __o._path):
            return False

        if (self._container != __o._container):
            return False

        return True

    def __exit__(
        self,
        type: object,
        value: object,
        traceback: object
    ) -> None:
        if (value is not None):
            return

        if (any(self._container)):
            raise TreeUnexpectedNodesException.create(
                nodes=TextUtils.format_args(
                    tpl="{}{}",
                    args=[
                        self.path(),
                        sorted(self.names())
                    ]
                )
            )

    def detach_bool_node(
        self,
        name: str
    ) -> bool:
        t = bool

        node = self._detach_node(name)

        if (isinstance(node, t) and (type(node) is t)):
            return node

        self._raise_exception_invalid_type(name, node, t)

    def detach_float_node(
        self,
        name: str
    ) -> float:
        t = float

        node = self._detach_node(name)

        if (isinstance(node, t) and (type(node) is t)):
            return node

        self._raise_exception_invalid_type(name, node, t)

    def detach_int_node(
        self,
        name: str
    ) -> int:
        t = int

        node = self._detach_node(name)

        if (isinstance(node, t) and (type(node) is t)):
            return node

        self._raise_exception_invalid_type(name, node, t)

    def detach_list_node(
        self,
        name: str
    ) -> TreeListNode:
        node = self._detach_node(name)

        try:
            return self._mapper.create_list_node(self._node_path(name), node)
        except TreeInvalidNodeTypeException:
            self._raise_exception_invalid_type(name, node, list)

    def detach_object_node(
        self,
        name: str
    ) -> TreeObjectNode:
        node = self._detach_node(name)

        try:
            return self._mapper.create_object_node(self._node_path(name), node)
        except TreeInvalidNodeTypeException:
            self._raise_exception_invalid_type(name, node, dict)

    def detach_text_node(
        self,
        name: str
    ) -> str:
        t = str

        node = self._detach_node(name)

        if (isinstance(node, t)):
            return node

        self._raise_exception_invalid_type(name, node, t)

    def names(self) -> Sequence[str]:
        return list(self._container.keys())

    def path(self) -> str:
        return self._path

    def _detach_node(
        self,
        name: str
    ) -> object:
        node = self._container.detach(name)

        if (node is not None):
            return node
        else:
            raise TreeNodeNotFoundException.create(self._node_path(name))

    def _node_path(self, name: str) -> str:
        return TextUtils.format_args(
            tpl="{}.{}",
            args=[
                self._path,
                name
            ]
        )

    def _raise_exception_invalid_type(
        self,
        name: str,
        node: object,
        t: Type[object]
    ) -> NoReturn:
        raise TreeInvalidNodeTypeException.create(
            path=self._node_path(name),
            expected_type=t.__name__,
            actual_type=type(node).__name__
        )


__all__: FinalCollection[str] = [
    "TreeObjectNodeImpl"
]
