from __future__ import annotations

import os

from collections.abc import Iterable
from collections.abc import Iterator

from pygencollection.impl import ArrayImpl
from pygenos.abc import OSDir
from pygenos.impl import OSDirImpl
from pygenpath.abc import Dirpath
from pygenpath.abc import Filepath
from pygenpath.utils import PathUtils
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class OSUtils(object):
    @classmethod
    def filepaths(cls, dirs: Iterable[OSDir]) -> Iterator[Filepath]:
        for dir in dirs:
            for filename in dir.filenames:
                yield dir.dirpath.resolve_filepath(
                    path=TextUtils.format_args(
                        tpl="./{}",
                        args=[
                            filename
                        ]
                    )
                )

    @classmethod
    def find(cls, dirpath: Dirpath) -> Iterator[Filepath]:
        for dir in cls.walk(dirpath):
            for filename in dir.filenames:
                yield dirpath.resolve_filepath(
                    path=TextUtils.format_args(
                        tpl="./{}",
                        args=[
                            filename
                        ]
                    )
                )

    @classmethod
    def walk(cls, dirpath: Dirpath) -> Iterator[OSDir]:
        iterator = os.walk(dirpath.path())

        _, dirnames, filenames = next(iterator)

        yield OSDirImpl(
            dirpath=dirpath,
            dirnames=ArrayImpl.copy(dirnames),
            filenames=ArrayImpl.copy(filenames)
        )

        for path, dirnames, filenames in iterator:
            yield OSDirImpl(
                dirpath=PathUtils.dirpath(
                    path=TextUtils.format_args(
                        tpl="{}/",
                        args=[
                            path
                        ]
                    )
                ),
                dirnames=ArrayImpl.copy(dirnames),
                filenames=ArrayImpl.copy(filenames)
            )


__all__: FinalCollection[str] = [
    "OSUtils"
]
