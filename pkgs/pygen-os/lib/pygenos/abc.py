from __future__ import annotations

from dataclasses import dataclass
from typing import Final

from pygenerr.err import SystemException
from pygenpath.abc import Dirpath
from pygentype.abc import FinalCollection


@dataclass
class OSDir(object):
    dirpath: Final[Dirpath]
    dirnames: FinalCollection[str]
    filenames: FinalCollection[str]

    def __lt__(self, other: OSDir) -> bool:
        raise SystemException()


__all__: FinalCollection[str] = [
    "OSDir"
]
