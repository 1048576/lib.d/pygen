from __future__ import annotations

from pygenos.abc import OSDir
from pygentype.abc import FinalCollection


class OSDirImpl(OSDir):
    def __lt__(self, other: OSDir) -> bool:
        return (self.dirpath.path() < other.dirpath.path())


__all__: FinalCollection[str] = [
    "OSDirImpl"
]
