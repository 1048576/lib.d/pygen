from __future__ import annotations

import json

from collections.abc import Mapping
from io import StringIO
from typing import Any

from pygentype.abc import FinalCollection


class JsonUtils(object):
    @classmethod
    def load_dict(cls, content: str) -> Mapping[str, Any]:
        return json.loads(content)

    @classmethod
    def to_pretty_json(cls, instance: object) -> str:
        buffer = StringIO()

        text = json.dumps(
            obj=instance,
            indent=2
        )

        buffer.write(text)
        buffer.write("\n")

        return buffer.getvalue()


__all__: FinalCollection[str] = [
    "JsonUtils"
]
