from __future__ import annotations

from typing import Protocol

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection


class JsonEncoder(Protocol):
    def encode(self, instance: object) -> object:
        raise SystemException()

    def support(self, instance: object) -> bool:
        raise SystemException()


class JsonMapper(object):
    def to_text(self, instance: object) -> str:
        raise SystemException()


__all__: FinalCollection[str] = [
    "JsonEncoder",
    "JsonMapper"
]
