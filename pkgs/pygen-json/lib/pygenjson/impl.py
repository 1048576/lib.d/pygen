from __future__ import annotations

import json

from io import StringIO

from pygencollection.abc import Array
from pygencollection.impl import ArrayImpl
from pygenerr.err import SystemException
from pygenjson.abc import JsonEncoder
from pygenjson.abc import JsonMapper
from pygentype.abc import FinalCollection


class PrettyJsonMapperImpl(JsonMapper):
    _encoders: FinalCollection[JsonEncoder]

    def __init__(
        self,
        encoders: Array[JsonEncoder] = ArrayImpl.of()
    ) -> None:
        self._encoders = encoders

    def encode(self, instance: object) -> object:
        for encoder in self._encoders:
            if (encoder.support(instance)):
                return encoder.encode(instance)

        raise SystemException()

    def to_text(self, instance: object) -> str:
        buffer = StringIO()

        text = json.dumps(
            default=self.encode,
            obj=instance,
            indent=2
        )

        buffer.write(text)
        buffer.write("\n")

        return buffer.getvalue()


__all__: FinalCollection[str] = [
    "PrettyJsonMapperImpl"
]
