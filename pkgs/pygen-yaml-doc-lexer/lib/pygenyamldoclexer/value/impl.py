from __future__ import annotations

from pygentextreader.abc import TextReaderPosition
from pygentype.abc import FinalCollection
from pygenyamldoclexer.abc import YamlDocLexerError
from pygenyamldoclexer.abc import YamlDocLexerTextLiteralToken
from pygenyamldoclexer.abc import YamlDocLexerValue
from pygenyamldoclexer.abc import YamlDocLexerValueMapper


class YamlDocLexerTextLiteralTokenImpl(
    YamlDocLexerTextLiteralToken,
    YamlDocLexerValue
):
    ...


class YamlDocLexerErrorImpl(YamlDocLexerError, YamlDocLexerValue):
    def as_error(self) -> YamlDocLexerError:
        return self


class YamlDocLexerValueMapperImpl(YamlDocLexerValueMapper):
    def error(
        self,
        code: str,
        position: TextReaderPosition
    ) -> YamlDocLexerValue:
        return YamlDocLexerErrorImpl(code, position.line, position.column)

    def text_literal(
        self,
        code: str,
        value: str,
        begin_position: TextReaderPosition,
        end_position: TextReaderPosition
    ) -> YamlDocLexerValue:
        return YamlDocLexerTextLiteralTokenImpl(
            code=code,
            value=value,
            begin_line=begin_position.line,
            begin_column=begin_position.column,
            end_line=end_position.line,
            end_column=end_position.column
        )


__all__: FinalCollection[str] = [
    "YamlDocLexerValueMapperImpl"
]
