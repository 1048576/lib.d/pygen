from __future__ import annotations

from pygentextdoclexer.impl import TextDocLexerImpl
from pygentype.abc import FinalCollection
from pygenyamldoclexer.abc import YamlDocLexerFnMapper
from pygenyamldoclexer.abc import YamlDocLexerProcessor
from pygenyamldoclexer.abc import YamlDocLexerProcessorMapper
from pygenyamldoclexer.abc import YamlDocLexerResult
from pygenyamldoclexer.abc import YamlDocLexerValue


class YamlDocLexerImpl(
    TextDocLexerImpl[
        YamlDocLexerProcessor,
        YamlDocLexerResult,
        YamlDocLexerValue
    ]
):
    def __init__(
        self,
        fn_mapper: YamlDocLexerFnMapper,
        processor_mapper: YamlDocLexerProcessorMapper
    ) -> None:
        super().__init__(
            processor=processor_mapper.document(),
            process_fn=fn_mapper.process_fn(),
            success_fn=fn_mapper.success_fn(),
            value_fn=fn_mapper.value_fn(),
            next_fn=fn_mapper.next_fn()
        )


__all__: FinalCollection[str] = [
    "YamlDocLexerImpl"
]
