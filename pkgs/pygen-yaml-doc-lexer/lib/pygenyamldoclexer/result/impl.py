from __future__ import annotations

from typing import Final

from pygentextreader.abc import TextReaderPosition
from pygentype.abc import FinalCollection
from pygenyamldoclexer.abc import YamlDocLexerProcessor
from pygenyamldoclexer.abc import YamlDocLexerResult
from pygenyamldoclexer.abc import YamlDocLexerResultMapper
from pygenyamldoclexer.abc import YamlDocLexerValue
from pygenyamldoclexer.abc import YamlDocLexerValueMapper


class YamlDocLexerSuccessResultImpl(YamlDocLexerResult):
    _value: Final[YamlDocLexerValue]
    _next: Final[YamlDocLexerProcessor]

    def __init__(
        self,
        value: YamlDocLexerValue,
        next: YamlDocLexerProcessor
    ) -> None:
        self._value = value
        self._next = next

    def next(self) -> YamlDocLexerProcessor:
        return self._next

    def success(self) -> bool:
        return True

    def value(self) -> YamlDocLexerValue:
        return self._value


class YamlDocLexerFailureResultImpl(YamlDocLexerResult):
    _value: Final[YamlDocLexerValue]

    def __init__(self, value: YamlDocLexerValue) -> None:
        self._value = value

    def success(self) -> bool:
        return False

    def value(self) -> YamlDocLexerValue:
        return self._value


class YamlDocLexerResultMapperImpl(YamlDocLexerResultMapper):
    _value_mapper: Final[YamlDocLexerValueMapper]

    def __init__(self, value_mapper: YamlDocLexerValueMapper) -> None:
        self._value_mapper = value_mapper

    def failure(
        self,
        code: str,
        position: TextReaderPosition
    ) -> YamlDocLexerResult:
        return YamlDocLexerFailureResultImpl(
            value=self._value_mapper.error(code, position)
        )

    def success(
        self,
        value: YamlDocLexerValue,
        next: YamlDocLexerProcessor
    ) -> YamlDocLexerResult:
        return YamlDocLexerSuccessResultImpl(value, next)


__all__: FinalCollection[str] = [
    "YamlDocLexerResultMapperImpl"
]
