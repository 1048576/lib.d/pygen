from __future__ import annotations

from collections.abc import Callable
from collections.abc import Iterable
from collections.abc import Iterator
from dataclasses import dataclass
from typing import Final

from pygendataclass.abc import DataclassInstance
from pygenerr.err import SystemException
from pygentextdoclexer.abc import TextDocLexerNextFn
from pygentextdoclexer.abc import TextDocLexerProcessFn
from pygentextdoclexer.abc import TextDocLexerSuccessFn
from pygentextdoclexer.abc import TextDocLexerValueFn
from pygentextreader.abc import TextReader
from pygentextreader.abc import TextReaderPosition
from pygentype.abc import FinalCollection
from pygentype.abc import FinalMapping


class YamlDocLexerConstants(object):
    INDENT_LENGTH: Final[int] = 2
    SPECIAL_CHARS: FinalMapping[str, str] = {
        "n": "\n",
        "\"": "\"",
        "\\": "\\"
    }


@dataclass
class YamlDocLexerTextLiteralToken(DataclassInstance):
    code: Final[str]
    value: Final[str]
    begin_line: Final[int]
    begin_column: Final[int]
    end_line: Final[int]
    end_column: Final[int]


@dataclass
class YamlDocLexerError(DataclassInstance):
    code: Final[str]
    line: Final[int]
    column: Final[int]


@dataclass
class YamlDocLexerValue(object):
    code: Final[str]

    def as_error(self) -> YamlDocLexerError:
        raise SystemException()


class YamlDocLexerValueMapper(object):
    def dict_key(self, value: str) -> YamlDocLexerValue:
        raise SystemException()

    def error(
        self,
        code: str,
        position: TextReaderPosition
    ) -> YamlDocLexerValue:
        raise SystemException()

    def null_literal(self) -> YamlDocLexerValue:
        raise SystemException()

    def text_literal(
        self,
        code: str,
        value: str,
        begin_position: TextReaderPosition,
        end_position: TextReaderPosition
    ) -> YamlDocLexerValue:
        raise SystemException()


class YamlDocLexerProcessor(object):
    def process(
        self,
        reader: TextReader
    ) -> YamlDocLexerResult:
        raise SystemException()


class YamlDocLexerProcessorMapper(object):
    def begin_dict(
        self,
        first_line_indent: int,
        other_line_indent: int,
        next: YamlDocLexerProcessor
    ) -> YamlDocLexerProcessor:
        raise SystemException()

    def begin_list(
        self,
        indent: int,
        next: YamlDocLexerProcessor
    ) -> YamlDocLexerProcessor:
        raise SystemException()

    def begin_list_item(
        self,
        indent: int,
        end_list_item_processor: YamlDocLexerProcessor
    ) -> YamlDocLexerProcessor:
        raise SystemException()

    def dict_key(
        self,
        indent: int
    ) -> YamlDocLexerProcessor:
        raise SystemException()

    def document(self) -> YamlDocLexerProcessor:
        raise SystemException()

    def fork(
        self,
        processors: Iterable[YamlDocLexerProcessor],
        on_failure_fn: Callable[[], YamlDocLexerResult]
    ) -> YamlDocLexerProcessor:
        raise SystemException()

    def ln(
        self,
        on_success_fn: Callable[[], YamlDocLexerResult],
        on_failure_fn: Callable[[TextReaderPosition], YamlDocLexerResult]
    ) -> YamlDocLexerProcessor:
        raise SystemException()

    def stub(self) -> YamlDocLexerProcessor:
        raise SystemException()

    def text_literal(
        self,
        next: YamlDocLexerProcessor
    ) -> YamlDocLexerProcessor:
        raise SystemException()


class YamlDocLexerResult(object):
    def next(self) -> YamlDocLexerProcessor:
        raise SystemException()

    def success(self) -> bool:
        raise SystemException()

    def value(self) -> YamlDocLexerValue:
        raise SystemException()


class YamlDocLexerResultMapper(object):
    def failure(
        self,
        code: str,
        position: TextReaderPosition
    ) -> YamlDocLexerResult:
        raise SystemException()

    def success(
        self,
        value: YamlDocLexerValue,
        next: YamlDocLexerProcessor
    ) -> YamlDocLexerResult:
        raise SystemException()


class YamlDocLexerNextFn(
    TextDocLexerNextFn[YamlDocLexerResult, YamlDocLexerProcessor]
):
    def __call__(
        self,
        result: YamlDocLexerResult
    ) -> YamlDocLexerProcessor:
        raise SystemException()


class YamlDocLexerProcessFn(
    TextDocLexerProcessFn[YamlDocLexerProcessor, YamlDocLexerResult]
):
    def __call__(
        self,
        processor: YamlDocLexerProcessor,
        reader: TextReader
    ) -> YamlDocLexerResult:
        raise SystemException()


class YamlDocLexerSuccessFn(TextDocLexerSuccessFn[YamlDocLexerResult]):
    def __call__(self, result: YamlDocLexerResult) -> bool:
        raise SystemException()


class YamlDocLexerValueFn(
    TextDocLexerValueFn[YamlDocLexerResult, YamlDocLexerValue]
):
    def __call__(
        self,
        result: YamlDocLexerResult
    ) -> YamlDocLexerValue:
        raise SystemException()


class YamlDocLexerFnMapper(object):
    def next_fn(self) -> YamlDocLexerNextFn:
        raise SystemException()

    def process_fn(self) -> YamlDocLexerProcessFn:
        raise SystemException()

    def success_fn(self) -> YamlDocLexerSuccessFn:
        raise SystemException()

    def value_fn(self) -> YamlDocLexerValueFn:
        raise SystemException()


class YamlDocLexer(object):
    def tokenize(self, reader: TextReader) -> Iterator[YamlDocLexerValue]:
        raise SystemException()


__all__: FinalCollection[str] = [
    "YamlDocLexerConstants",
    "YamlDocLexerError",
    "YamlDocLexerFnMapper",
    "YamlDocLexerNextFn",
    "YamlDocLexerProcessFn",
    "YamlDocLexerProcessor",
    "YamlDocLexerProcessorMapper",
    "YamlDocLexerResult",
    "YamlDocLexerResultMapper",
    "YamlDocLexerSuccessFn",
    "YamlDocLexerTextLiteralToken",
    "YamlDocLexerValue",
    "YamlDocLexerValueFn",
    "YamlDocLexerValueMapper"
]
