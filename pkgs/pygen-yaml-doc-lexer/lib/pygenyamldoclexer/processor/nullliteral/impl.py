from __future__ import annotations

from dataclasses import dataclass
from typing import Final

from pygencounter.impl import CounterImpl
from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection
from pygenyamldoclexer.abc import YamlDocLexerProcessor
from pygenyamldoclexer.abc import YamlDocLexerResult
from pygenyamldoclexer.abc import YamlDocLexerResultMapper
from pygenyamldoclexer.abc import YamlDocLexerValueMapper


@dataclass
class YamlDocLexerNullLiteralProcessorImpl(YamlDocLexerProcessor):
    result_mapper: Final[YamlDocLexerResultMapper]
    value_mapper: Final[YamlDocLexerValueMapper]
    next: Final[YamlDocLexerProcessor]

    def process(self, reader: TextReader) -> YamlDocLexerResult:
        index = CounterImpl()

        for char in "null":
            if (char != reader.peek(index.inc())):
                return self.result_mapper.failure(
                    code="NullLiteral000",
                    position=reader.peek_position(index.value() - 1)
                )

        reader.forward(index.value())

        return self.result_mapper.success(
            value=self.value_mapper.null_literal(),
            next=self.next
        )


__all__: FinalCollection[str] = [
    "YamlDocLexerNullLiteralProcessorImpl"
]
