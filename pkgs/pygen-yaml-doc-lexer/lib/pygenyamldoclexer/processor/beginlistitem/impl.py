from __future__ import annotations

from dataclasses import dataclass
from typing import Final

from pygencounter.impl import CounterImpl
from pygenerr.err import SystemException
from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection
from pygenyamldoclexer.abc import YamlDocLexerProcessor
from pygenyamldoclexer.abc import YamlDocLexerResult
from pygenyamldoclexer.abc import YamlDocLexerResultMapper


@dataclass
class YamlDocLexerBeginListItemProcessorImpl(YamlDocLexerProcessor):
    result_mapper: Final[YamlDocLexerResultMapper]
    indent: Final[int]
    end_list_item_processor: Final[YamlDocLexerProcessor]

    def process(self, reader: TextReader) -> YamlDocLexerResult:
        index = CounterImpl()

        for char in "- ":
            if (char != reader.peek(index.inc())):
                return self.result_mapper.failure(
                    code="Y005",
                    position=reader.peek_position(index.value() - 1)
                )

        raise SystemException()


__all__: FinalCollection[str] = [
    "YamlDocLexerBeginListItemProcessorImpl"
]
