from __future__ import annotations

from socket import AF_INET
from socket import SOCK_STREAM
from socket import socket
from socket import socketpair
from typing import Final

from pygennet.abc import NetworkAddress
from pygennet.impl import NetworkAddressImpl
from pygensocket.abc import ClientSocket
from pygensocket.abc import ServerSocket
from pygensocket.abc import Socket
from pygensocket.abc import SocketContextManager
from pygensocket.abc import SocketPair
from pygensocket.abc import SocketPairContextManager
from pygensocket.err import SocketClosedException
from pygentype.abc import FinalCollection


class ClientSocketImpl(ClientSocket):
    _socket: Final[socket]

    def __init__(self) -> None:
        self._socket = socket(AF_INET, SOCK_STREAM)

    def close(self) -> None:
        self._socket.close()

    def connect(self, address: NetworkAddress) -> None:
        self._socket.connect(address.args())

    def receive(self, buffer_size: int) -> bytes:
        try:
            return self._socket.recv(buffer_size)
        except KeyboardInterrupt:
            raise SocketClosedException()

    def send(self, data: bytes) -> None:
        self._socket.sendall(data)


class SocketContextManagerImpl(SocketContextManager):
    _socket: Final[socket]

    def __init__(self, socket: socket) -> None:
        self._socket = socket

    def __enter__(self) -> Socket:
        return self

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        self._socket.close()

    def close(self) -> None:
        self._socket.close()

    def receive(self, buffer_size: int) -> bytes:
        data = self._socket.recv(buffer_size)

        if (data == b''):
            raise SocketClosedException()
        else:
            return data

    def send(self, data: bytes) -> None:
        self._socket.sendall(data)


class ServerSocketImpl(ServerSocket):
    _socket: Final[socket]

    def __init__(self) -> None:
        self._socket = socket()

    def accept(self) -> SocketContextManager:
        socket, _ = self._socket.accept()

        return SocketContextManagerImpl(socket)

    def address(self) -> NetworkAddress:
        return NetworkAddressImpl(*self._socket.getsockname())

    def bind(self, address: NetworkAddress) -> None:
        self._socket.bind(address.args())

    def close(self) -> None:
        self._socket.close()

    def listen(self, backlog: int) -> None:
        self._socket.listen(backlog)


class SocketPairImpl(SocketPairContextManager):
    class SocketImpl(Socket):
        _socket: Final[socket]

        def __init__(self, socket: socket) -> None:
            self._socket = socket

        def close(self) -> None:
            self._socket.close()

        def fileno(self) -> int:
            return self._socket.fileno()

        def receive(self, buffer_size: int) -> bytes:
            return self._socket.recv(buffer_size)

        def send(self, data: bytes) -> None:
            self._socket.sendall(data)

    _write: Final[Socket]
    _read: Final[Socket]

    def __init__(self) -> None:
        write, read = socketpair()

        self._write = self.SocketImpl(write)
        self._read = self.SocketImpl(read)

    def __enter__(self) -> SocketPair:
        return self

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        self._write.close()
        self._read.close()

    def read(self) -> Socket:
        return self._read

    def write(self) -> Socket:
        return self._write


__all__: FinalCollection[str] = [
    "ClientSocketImpl",
    "ServerSocketImpl",
    "SocketPairImpl"
]
