from __future__ import annotations

from collections.abc import Iterator
from collections.abc import Mapping
from typing import Final
from typing import Generic

from pygenast.abc import ASTCallExpr
from pygenast.abc import ASTConstantExpr
from pygenast.abc import ASTDictExpr
from pygenast.abc import ASTListExpr
from pygenast.abc import ASTNode
from pygenastanalyzer.abc import ASTAnalyzer
from pygenastanalyzer.abc import ASTAnalyzerProcessor
from pygenastanalyzer.err import ASTSyntaxException
from pygencollection.abc import Array
from pygencollection.impl import ArrayImpl
from pygencollection.utils import CollectionUtils
from pygenpath.abc import Filepath
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygentype.abc import Optional
from pygentype.abc import Pair
from pygentype.abc import T


class ASTTextConstantProcessorImpl(object):
    def process(
        self,
        analyzer: ASTAnalyzer,
        what: str,
        owner: ASTNode,
        expr: Optional[ASTNode]
    ) -> str:
        return analyzer.read_text_constant(what, owner, expr)


class ASTDictProcessorImpl(Generic[T]):
    _fn: Final[ASTAnalyzerProcessor[T]]

    def __init__(self, fn: ASTAnalyzerProcessor[T]) -> None:
        self._fn = fn

    def process(
        self,
        analyzer: ASTAnalyzer,
        what: str,
        owner: ASTNode,
        expr: Optional[ASTNode]
    ) -> Mapping[str, T]:
        return analyzer.read_dict(what, owner, expr, self._fn)


class ASTListProcessorImpl(Generic[T]):
    _fn: Final[ASTAnalyzerProcessor[T]]

    def __init__(self, fn: ASTAnalyzerProcessor[T]) -> None:
        self._fn = fn

    def process(
        self,
        analyzer: ASTAnalyzer,
        what: str,
        owner: ASTNode,
        expr: Optional[ASTNode]
    ) -> Array[T]:
        return analyzer.read_list(what, owner, expr, self._fn)


class ASTAnalyzerImpl(ASTAnalyzer):
    _name: Final[str]
    _filepath: Final[Filepath]

    def __init__(self, name: str, filepath: Filepath) -> None:
        self._name = name
        self._filepath = filepath

    def read_call_argument(
        self,
        what: str,
        expr: ASTCallExpr,
        name: str,
        processor: ASTAnalyzerProcessor[T]
    ) -> T:
        for keyword in expr.keywords:
            if (keyword.arg == name):
                return processor.process(
                    analyzer=self,
                    what=TextUtils.format_args(
                        tpl="Agument [{}]",
                        args=[
                            name
                        ]
                    ),
                    owner=keyword,
                    expr=keyword.value
                )

        raise ASTSyntaxException.argument_no_found(
            what=what,
            filepath=self._filepath,
            lineno=expr.lineno,
            name=name
        )

    def read_dict(
        self,
        what: str,
        owner: ASTNode,
        expr: Optional[ASTNode],
        processor: ASTAnalyzerProcessor[T]
    ) -> Mapping[str, T]:
        return CollectionUtils.to_dict(
            items=self._read_dict(what, owner, expr, processor)
        )

    def read_list(
        self,
        what: str,
        owner: ASTNode,
        expr: Optional[ASTNode],
        processor: ASTAnalyzerProcessor[T]
    ) -> Array[T]:
        return ArrayImpl.collect(
            iterator=self._read_list(what, owner, expr, processor)
        )

    def read_text_constant(
        self,
        what: str,
        owner: ASTNode,
        expr: Optional[ASTNode]
    ) -> str:
        if (isinstance(expr, ASTConstantExpr)):
            value = expr.value
        else:
            raise ASTSyntaxException.invalid_type(
                what=self._name,
                filepath=self._filepath,
                lineno=owner.lineno,
                varname=what,
                expected_type=ASTConstantExpr.__name__,
                actual_type=expr.__class__.__name__
            )

        if (isinstance(value, str)):
            return value
        else:
            raise ASTSyntaxException.invalid_type(
                what=self._name,
                filepath=self._filepath,
                lineno=expr.lineno,
                varname=what,
                expected_type=str.__name__,
                actual_type=value.__class__.__name__
            )

    def _read_dict(
        self,
        what: str,
        owner: ASTNode,
        expr: Optional[ASTNode],
        processor: ASTAnalyzerProcessor[T]
    ) -> Iterator[Pair[str, T]]:
        if (isinstance(expr, ASTDictExpr)):
            pairs = CollectionUtils.join_by_index(expr.keys, expr.values)
        else:
            raise ASTSyntaxException.invalid_type(
                what=self._name,
                filepath=self._filepath,
                lineno=owner.lineno,
                varname=what,
                expected_type=ASTListExpr.__name__,
                actual_type=expr.__class__.__name__
            )

        for key_expr, value_expr in pairs:
            key = self.read_text_constant(
                what="Dict key",
                owner=value_expr,
                expr=key_expr
            )
            value = processor.process(self, what, owner, value_expr)

            yield (key, value)

    def _read_list(
        self,
        what: str,
        owner: ASTNode,
        expr: Optional[ASTNode],
        processor: ASTAnalyzerProcessor[T]
    ) -> Iterator[T]:
        if (isinstance(expr, ASTListExpr)):
            elts = expr.elts
        else:
            raise ASTSyntaxException.invalid_type(
                what=self._name,
                filepath=self._filepath,
                lineno=owner.lineno,
                varname=what,
                expected_type=ASTListExpr.__name__,
                actual_type=expr.__class__.__name__
            )

        for elt in elts:
            yield processor.process(
                analyzer=self,
                what=TextUtils.format_args(
                    tpl="{} element",
                    args=[
                        what
                    ]
                ),
                owner=expr,
                expr=elt
            )


__all__: FinalCollection[str] = [
    "ASTAnalyzerImpl",
    "ASTDictProcessorImpl",
    "ASTListProcessorImpl",
    "ASTTextConstantProcessorImpl"
]
