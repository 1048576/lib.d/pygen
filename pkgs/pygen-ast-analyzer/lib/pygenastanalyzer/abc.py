from __future__ import annotations

from collections.abc import Mapping
from typing import Generic
from typing import Protocol

from pygenast.abc import ASTCallExpr
from pygenast.abc import ASTNode
from pygencollection.abc import Array
from pygenerr.err import SystemException
from pygentype.abc import CovariantT
from pygentype.abc import FinalCollection
from pygentype.abc import Optional
from pygentype.abc import T


class ASTAnalyzerProcessor(Protocol, Generic[CovariantT]):
    def process(
        self,
        analyzer: ASTAnalyzer,
        what: str,
        owner: ASTNode,
        expr: Optional[ASTNode]
    ) -> CovariantT:
        raise SystemException()


class ASTAnalyzer(object):
    def read_call_argument(
        self,
        what: str,
        expr: ASTCallExpr,
        name: str,
        processor: ASTAnalyzerProcessor[T]
    ) -> T:
        raise SystemException()

    def read_dict(
        self,
        what: str,
        owner: ASTNode,
        expr: Optional[ASTNode],
        processor: ASTAnalyzerProcessor[T]
    ) -> Mapping[str, T]:
        raise SystemException()

    def read_list(
        self,
        what: str,
        owner: ASTNode,
        expr: Optional[ASTNode],
        processor: ASTAnalyzerProcessor[T]
    ) -> Array[T]:
        raise SystemException()

    def read_text_constant(
        self,
        what: str,
        owner: ASTNode,
        expr: Optional[ASTNode]
    ) -> str:
        raise SystemException()


__all__: FinalCollection[str] = [
    "ASTAnalyzer",
    "ASTAnalyzerProcessor"
]
