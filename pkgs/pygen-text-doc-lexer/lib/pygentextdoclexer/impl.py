from __future__ import annotations

from collections.abc import Iterator
from typing import Final
from typing import Generic
from typing import TypeVar

from pygentextdoclexer.abc import FinalTextDocLexerNextFn
from pygentextdoclexer.abc import FinalTextDocLexerProcessFn
from pygentextdoclexer.abc import FinalTextDocLexerSuccessFn
from pygentextdoclexer.abc import FinalTextDocLexerValueFn
from pygentextdoclexer.abc import TextDocLexer
from pygentextdoclexer.abc import TextDocLexerNextFn
from pygentextdoclexer.abc import TextDocLexerProcessFn
from pygentextdoclexer.abc import TextDocLexerSuccessFn
from pygentextdoclexer.abc import TextDocLexerValueFn
from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection

ProcessorT = TypeVar("ProcessorT")
ResultT = TypeVar("ResultT")
ValueT = TypeVar("ValueT")


class TextDocLexerImpl(
    Generic[ProcessorT, ResultT, ValueT],
    TextDocLexer[ValueT]
):
    _processor: Final[ProcessorT]
    _process_fn: FinalTextDocLexerProcessFn[ProcessorT, ResultT]
    _success_fn: FinalTextDocLexerSuccessFn[ResultT]
    _value_fn: FinalTextDocLexerValueFn[ResultT, ValueT]
    _next_fn: FinalTextDocLexerNextFn[ResultT, ProcessorT]

    def __init__(
        self,
        processor: ProcessorT,
        process_fn: TextDocLexerProcessFn[ProcessorT, ResultT],
        success_fn: TextDocLexerSuccessFn[ResultT],
        value_fn: TextDocLexerValueFn[ResultT, ValueT],
        next_fn: TextDocLexerNextFn[ResultT, ProcessorT]
    ) -> None:
        self._processor = processor
        self._process_fn = process_fn
        self._success_fn = success_fn
        self._value_fn = value_fn
        self._next_fn = next_fn

    def tokenize(self, reader: TextReader) -> Iterator[ValueT]:
        processor = self._processor

        while (not reader.eof()):
            result = self._process_fn(processor, reader)

            yield self._value_fn(result)

            if (self._success_fn(result)):
                processor = self._next_fn(result)
            else:
                break


__all__: FinalCollection[str] = [
    "TextDocLexerImpl"
]
