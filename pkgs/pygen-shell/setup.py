#!/usr/bin/env python

from __future__ import annotations

import os

from collections.abc import Collection

import setuptools


def install_requires() -> Collection[str]:
    with (open("requirements.txt", "r") as f):
        return f.read().splitlines()


if (__name__ == "__main__"):
    setuptools.setup(
        name="pygen-shell",
        version=os.environ.get("BUILD_ARG_VERSION", "0.0.dev"),
        author="Vladyslav Kazakov",
        author_email="kazakov1048576@gmail.com",
        url="https://gitlab.com/1048576/lib.d/pygen",
        install_requires=install_requires(),
        package_data={
            "pygenshell": [
                "py.typed"
            ]
        },
        package_dir={
            "": "lib"
        },
        packages=setuptools.find_packages("./lib/"),
        entry_points={},
        scripts=[],
        license="MIT"
    )
