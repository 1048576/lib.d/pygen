from __future__ import annotations

from collections.abc import Iterable

from pygencollection.impl import ArrayImpl
from pygencollection.utils import CollectionUtils
from pygenpath.abc import Dirpath
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class UnknownDirTestCaseImpl(TestCase):
    _dirpaths: FinalCollection[Dirpath]

    def __init__(self, dirpaths: Iterable[Dirpath]) -> None:
        self._dirpaths = ArrayImpl.copy(dirpaths)

    def run(self, test: Test) -> None:
        dirpaths = CollectionUtils.head(
            items=self._dirpaths,
            size=10
        )

        test.assert_text_equal(
            expected="",
            actual=TextUtils.join(
                delimiter="\n",
                items=CollectionUtils.map(
                    items=dirpaths,
                    fn=lambda dirpath: dirpath.path()
                )
            ),
            msg=lambda args: TextUtils.format_args(
                tpl="Unknown dirs\n{}\n...",
                args=[
                    args.diff
                ]
            )
        )


__all__: FinalCollection[str] = [
    "UnknownDirTestCaseImpl"
]
