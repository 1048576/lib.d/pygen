from __future__ import annotations

from typing import Self

from pygenerr.err import SystemException
from pygenpath.abc import Filepath
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class PyPkgSetupfileException(SystemException):
    @classmethod
    def could_not_be_parsed(cls, filepath: Filepath, e: SyntaxError) -> Self:
        return cls(
            msg=TextUtils.format_args(
                tpl=(
                    "Setup file could not be parsed:\n"
                    "  error: {}\n"
                    "  line: {}:{}"
                ),
                args=[
                    e.msg,
                    filepath,
                    e.lineno
                ]
            )
        )

    @classmethod
    def could_not_be_read(cls, filepath: Filepath, e: OSError) -> Self:
        return cls(
            msg=TextUtils.format_args(
                tpl=(
                    "Setup file could not be read:\n"
                    "  error: {}\n"
                    "  filepath: {}"
                ),
                args=[
                    e.args[-1],
                    filepath
                ]
            )
        )

    @classmethod
    def setup_method_call_not_found(cls, filepath: Filepath) -> Self:
        return cls(
            msg=TextUtils.format_args(
                tpl=(
                    "Setup method is not being called in setup file:\n"
                    "  filepath: {}"
                ),
                args=[
                    filepath
                ]
            )
        )


__all__: FinalCollection[str] = [
    "PyPkgSetupfileException"
]
