from __future__ import annotations

import ast

from collections.abc import Iterable
from collections.abc import Iterator

from pygenast.abc import ASTAttributeExpr
from pygenast.abc import ASTModule
from pygenast.abc import ASTNameExpr
from pygenast.abc import ASTNode
from pygenast.abc import ASTNodeT
from pygenerr.err import SystemException
from pygentype.abc import FinalCollection
from pygentype.abc import Optional
from pygentype.abc import Type


class ASTUtils(object):
    @classmethod
    def as_name(
        cls,
        node: Optional[ASTNode]
    ) -> str:
        if (isinstance(node, ASTNameExpr)):
            return node.id
        else:
            raise SystemException()

    @classmethod
    def filter(
        cls,
        nodes: Iterable[ASTNode],
        t: Type[ASTNodeT]
    ) -> Iterator[ASTNodeT]:
        for stmt in nodes:
            if (isinstance(stmt, t)):
                yield stmt

    @classmethod
    def name_equal(
        cls,
        node: ASTNode,
        value: str
    ) -> bool:
        parts = value.split(".")
        current_node = node

        for part in reversed(parts[1:]):
            if (not isinstance(current_node, ASTAttributeExpr)):
                return False

            if (part != current_node.attr):
                return False

            current_node = current_node.value

        if (isinstance(current_node, ASTNameExpr)):
            name_expr = current_node
        else:
            return False

        return (name_expr.id == parts[0])

    @classmethod
    def parse(cls, source: str, path: str) -> ASTModule:
        return ast.parse(source, path)


__all__: FinalCollection[str] = [
    "ASTUtils"
]
