from __future__ import annotations

from typing import Final

from pygenjinjadoc.abc import JinjaDocLoader
from pygenjinjadoc.abc import JinjaDocTokenEOF
from pygenjinjadoc.processor.impl import JinjaDocProcessorImpl
from pygentextdoc.abc import TextDocLoader
from pygentextdoc.abc import TextDocStream
from pygentextdoc.impl import TextDocLoaderImpl
from pygentype.abc import FinalCollection


class JinjaDocLoaderImpl(JinjaDocLoader):
    _loader: Final[TextDocLoader]

    def __init__(self) -> None:
        self._loader = TextDocLoaderImpl(
            processor=JinjaDocProcessorImpl(),
            eof_token_type=JinjaDocTokenEOF
        )

    def load_stream_from_file(self, path: str) -> TextDocStream:
        return self._loader.load_stream_from_file(path)


__all__: FinalCollection[str] = [
    "JinjaDocLoaderImpl"
]
