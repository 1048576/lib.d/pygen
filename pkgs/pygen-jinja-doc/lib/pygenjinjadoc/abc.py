from __future__ import annotations

from dataclasses import dataclass

from pygenerr.err import SystemException
from pygentextdoc.abc import TextDocProcessor
from pygentextdoc.abc import TextDocStream
from pygentextdoc.abc import TextDocToken
from pygentype.abc import FinalCollection


@dataclass
class JinjaDocTokenText(TextDocToken):
    parts: FinalCollection[str]


@dataclass
class JinjaDocTokenNonprintingText(TextDocToken):
    ...


class JinjaDocTokenBeginFor(TextDocToken):
    ...


@dataclass
class JinjaDocTokenEndFor(TextDocToken):
    ...


@dataclass
class JinjaDocTokenEOF(TextDocToken):
    ...


@dataclass
class JinjaDocProcessor(TextDocProcessor):
    ...


class JinjaDocLoader(object):
    def load_stream_from_file(self, path: str) -> TextDocStream:
        raise SystemException()


__all__: FinalCollection[str] = [
    "JinjaDocLoader",
    "JinjaDocProcessor",
    "JinjaDocTokenBeginFor",
    "JinjaDocTokenEOF",
    "JinjaDocTokenEndFor",
    "JinjaDocTokenNonprintingText",
    "JinjaDocTokenText"
]
