from __future__ import annotations

from pygenarg.abc import Arg
from pygenerr.err import SystemException
from pygennet.abc import NetworkAddress
from pygentype.abc import FinalCollection


class NetworkAddressArgContext(object):
    def value(self) -> NetworkAddress:
        raise SystemException()


class NetworkAddressArg(
    Arg[NetworkAddressArgContext],
    NetworkAddressArgContext
):
    ...


__all__: FinalCollection[str] = [
    "NetworkAddressArg",
    "NetworkAddressArgContext"
]
