from __future__ import annotations

from dataclasses import dataclass

from pygentype.abc import FinalCollection


@dataclass
class DataclassInstance(object):
    ...


__all__: FinalCollection[str] = [
    "DataclassInstance"
]
