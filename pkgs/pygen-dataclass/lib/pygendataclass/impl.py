from __future__ import annotations

from pygendataclass.abc import DataclassInstance
from pygenerr.err import SystemException
from pygentype.abc import FinalCollection


class DataclassEncoderImpl(object):
    def encode(self, instance: object) -> object:
        if (isinstance(instance, DataclassInstance)):
            return repr(instance)
        else:
            raise SystemException()

    def support(self, instance: object) -> bool:
        return isinstance(instance, DataclassInstance)


__all__: FinalCollection[str] = [
    "DataclassEncoderImpl"
]
