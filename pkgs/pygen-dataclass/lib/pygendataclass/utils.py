from __future__ import annotations

import dataclasses

from collections.abc import Mapping

from pygendataclass.abc import DataclassInstance
from pygentype.abc import FinalCollection


class DataclassUtils(object):
    @classmethod
    def pairs(
        cls,
        instance: DataclassInstance
    ) -> Mapping[str, object]:
        return dataclasses.asdict(instance)


__all__: FinalCollection[str] = [
    "DataclassUtils"
]
