from __future__ import annotations

from collections.abc import Iterable
from io import StringIO
from typing import Final

from pygenalphabet.utils import AlphabetUtils
from pygencollection.impl import ArrayImpl
from pygencounter.abc import Counter
from pygencounter.impl import CounterImpl
from pygentextdoc.abc import TextDocProcessorResult
from pygentextdoc.processor.impl import TextDocProcessorCacheImpl
from pygentextdoc.processor.impl import TextDocProcessorForkImpl
from pygentextdoc.processor.impl import TextDocProcessorLnImpl
from pygentextdoc.processor.impl import TextDocProcessorResultFailureImpl
from pygentextdoc.processor.impl import TextDocProcessorResultSuccessImpl
from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection
from pygentype.abc import FinalMapping
from pygentype.abc import Optional
from pygenyamldoc.abc import YamlDocConstants
from pygenyamldoc.abc import YamlDocProcessor
from pygenyamldoc.abc import YamlDocTokenBeginDict
from pygenyamldoc.abc import YamlDocTokenBeginList
from pygenyamldoc.abc import YamlDocTokenBeginListItem
from pygenyamldoc.abc import YamlDocTokenDictKey
from pygenyamldoc.abc import YamlDocTokenEndDict
from pygenyamldoc.abc import YamlDocTokenEndList
from pygenyamldoc.abc import YamlDocTokenEndListItem
from pygenyamldoc.abc import YamlDocTokenEOF
from pygenyamldoc.abc import YamlDocTokenLiteralFalse
from pygenyamldoc.abc import YamlDocTokenLiteralNull
from pygenyamldoc.abc import YamlDocTokenLiteralNumber
from pygenyamldoc.abc import YamlDocTokenLiteralText
from pygenyamldoc.abc import YamlDocTokenLiteralTrue


class YamlDocProcessorResultSuccessImpl(TextDocProcessorResultSuccessImpl):
    ...


class YamlDocProcessorResultFailureImpl(TextDocProcessorResultFailureImpl):
    ...


class YamlDocProcessorForkImpl(
    TextDocProcessorForkImpl[YamlDocProcessor],
    YamlDocProcessor
):
    def __init__(
        self,
        processors: Iterable[YamlDocProcessor]
    ) -> None:
        super().__init__(
            processors=processors,
            on_failure=YamlDocProcessorResultFailureImpl()
        )


class YamlDocProcessorCacheImpl(TextDocProcessorCacheImpl, YamlDocProcessor):
    ...


class YamlDocProcessorFailureImpl(YamlDocProcessor):
    def process(self, reader: TextReader) -> TextDocProcessorResult:
        return YamlDocProcessorResultFailureImpl()


class YamlDocProcessorDocumentImpl(YamlDocProcessor):
    def process(self, reader: TextReader) -> TextDocProcessorResult:
        index = CounterImpl()

        for char in "---\n":
            if (char != reader.peek(index.inc())):
                return YamlDocProcessorResultFailureImpl()

        reader.forward(index.value())

        processor = YamlDocProcessorForkImpl(
            processors=ArrayImpl.of(
                YamlDocProcessorBeginDictImpl(
                    first_line_indent=0,
                    other_line_indent=0,
                    next=YamlDocProcessorEOFImpl()
                ),
                YamlDocProcessorBeginListImpl(
                    indent=0,
                    next=YamlDocProcessorEOFImpl()
                )
            )
        )

        return processor.process(reader)


class YamlDocProcessorBeginDictImpl(YamlDocProcessor):
    _first_line_indent: Final[int]
    _other_line_indent: Final[int]
    _next: Final[YamlDocProcessor]

    def __init__(
        self,
        first_line_indent: int,
        other_line_indent: int,
        next: YamlDocProcessor
    ) -> None:
        self._first_line_indent = first_line_indent
        self._other_line_indent = other_line_indent
        self._next = next

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        dict_key_processor = YamlDocProcessorDictKeyImpl(
            indent=self._first_line_indent
        )

        dict_key_processor_result = dict_key_processor.process(reader)

        if (dict_key_processor_result.success()):
            end_dict_processor = YamlDocProcessorEndDictImpl(
                indent=self._other_line_indent,
                next=self._next
            )
            dict_value_processor = YamlDocProcessorDictValueImpl(
                indent=self._other_line_indent,
                end_dict_processor=end_dict_processor
            )
            dict_key_processor = YamlDocProcessorCacheImpl(
                result=YamlDocProcessorResultSuccessImpl(
                    next=dict_value_processor,
                    token=dict_key_processor_result.token(),
                    line=dict_key_processor_result.line()
                )
            )

            return YamlDocProcessorResultSuccessImpl(
                next=dict_key_processor,
                token=YamlDocTokenBeginDict(),
                line=dict_key_processor_result.line()
            )
        else:
            return YamlDocProcessorResultFailureImpl()


class YamlDocProcessorEndDictImpl(YamlDocProcessor):
    _indent: Final[int]
    _next: Final[YamlDocProcessor]

    def __init__(
        self,
        indent: int,
        next: YamlDocProcessor
    ) -> None:
        self._indent = indent
        self._next = next

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        line = reader.line()
        dict_key_processor = YamlDocProcessorDictKeyImpl(
            indent=self._indent
        )

        dict_key_processor_result = dict_key_processor.process(reader)

        if (dict_key_processor_result.success()):
            return YamlDocProcessorResultSuccessImpl(
                next=YamlDocProcessorDictValueImpl(
                    indent=self._indent,
                    end_dict_processor=self
                ),
                token=dict_key_processor_result.token(),
                line=dict_key_processor_result.line()
            )
        else:
            return YamlDocProcessorResultSuccessImpl(
                next=self._next,
                token=YamlDocTokenEndDict(),
                line=(line - 1)
            )


class YamlDocProcessorDictKeyImpl(YamlDocProcessor):
    _indent: Final[int]

    def __init__(self, indent: int) -> None:
        self._indent = indent

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        line = reader.line()
        index = CounterImpl()
        value = StringIO()

        for _ in range(0, self._indent):
            for _ in range(0, YamlDocConstants.INDENT_LENGTH):
                if (reader.peek(index.inc()) != " "):
                    return YamlDocProcessorResultFailureImpl()

        char = reader.peek(index.inc())

        if (char == ""):
            return YamlDocProcessorResultFailureImpl()
        elif (char not in AlphabetUtils.LATIN):
            return YamlDocProcessorResultFailureImpl()
        else:
            value.write(char)

        while (True):
            char = reader.peek(index.inc())

            if (char == ":"):
                break
            elif (char == ""):
                return YamlDocProcessorResultFailureImpl()

            while (True):
                if (char in AlphabetUtils.LATIN):
                    break
                elif (char in AlphabetUtils.NUMBERS):
                    break
                elif (char in (".", "/", "_", "-")):
                    break

                return YamlDocProcessorResultFailureImpl()

            value.write(char)

        char = reader.peek(index.value())

        if (char in (" ", "\n")):
            reader.forward(index.value())

            return YamlDocProcessorResultSuccessImpl(
                next=YamlDocProcessor(),
                token=YamlDocTokenDictKey(
                    value=value.getvalue()
                ),
                line=line
            )
        else:
            return YamlDocProcessorResultFailureImpl()


class YamlDocProcessorDictValueImpl(YamlDocProcessor):
    _indent: Final[int]
    _end_dict_processor: Final[YamlDocProcessor]

    def __init__(
        self,
        indent: int,
        end_dict_processor: YamlDocProcessor
    ) -> None:
        self._indent = indent
        self._end_dict_processor = end_dict_processor

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        index = CounterImpl()

        char = reader.peek(index.inc())

        if (char == " "):
            reader.forward(index.value())

            processor = YamlDocProcessorForkImpl(
                processors=ArrayImpl.of(
                    YamlDocProcessorLiteralTextImpl(
                        next=self._end_dict_processor
                    ),
                    YamlDocProcessorLiteralNumberImpl(
                        next=self._end_dict_processor
                    ),
                    YamlDocProcessorLiteralBoolImpl(
                        next=self._end_dict_processor
                    ),
                    YamlDocProcessorLiteralNullImpl(
                        next=self._end_dict_processor
                    ),
                    YamlDocProcessorEmptyDictImpl(
                        next=self._end_dict_processor
                    ),
                    YamlDocProcessorEmptyListImpl(
                        next=self._end_dict_processor
                    ),
                    YamlDocProcessorLiteralMultilineTextImpl(
                        indent=self._indent + 1,
                        next=self._end_dict_processor
                    )
                )
            )

            literal_processor_result = processor.process(reader)

            if (not literal_processor_result.success()):
                return YamlDocProcessorResultFailureImpl()

            ln_processor = YamlDocProcessorLnImpl(
                on_success=literal_processor_result,
                on_failure=YamlDocProcessorResultFailureImpl()
            )

            return ln_processor.process(reader)
        elif (char == "\n"):
            reader.forward(index.value())

            container_processor = YamlDocProcessorForkImpl(
                processors=ArrayImpl.of(
                    YamlDocProcessorBeginDictImpl(
                        first_line_indent=(self._indent + 1),
                        other_line_indent=(self._indent + 1),
                        next=self._end_dict_processor
                    ),
                    YamlDocProcessorBeginListImpl(
                        indent=(self._indent + 1),
                        next=self._end_dict_processor
                    )
                )
            )

            return container_processor.process(reader)
        else:
            return YamlDocProcessorResultFailureImpl()


class YamlDocProcessorBeginListImpl(YamlDocProcessor):
    _indent: Final[int]
    _next: Final[YamlDocProcessor]

    def __init__(
        self,
        indent: int,
        next: YamlDocProcessor
    ) -> None:
        self._indent = indent
        self._next = next

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        end_list_processor = YamlDocProcessorEndLisImpl(
            indent=self._indent,
            next=self._next
        )
        end_list_item_processor = YamlDocProcessorEndListItemImpl(
            end_list_processor=end_list_processor
        )
        begin_list_item_processor = YamlDocProcessorBeginListItemImpl(
            indent=self._indent,
            end_list_item_processor=end_list_item_processor
        )

        begin_list_item_processor_result = begin_list_item_processor.process(
            reader=reader
        )

        if (begin_list_item_processor_result.success()):
            begin_list_item_cache_processor = YamlDocProcessorCacheImpl(
                result=begin_list_item_processor_result
            )

            return YamlDocProcessorResultSuccessImpl(
                next=begin_list_item_cache_processor,
                token=YamlDocTokenBeginList(),
                line=begin_list_item_processor_result.line()
            )
        else:
            return YamlDocProcessorResultFailureImpl()


class YamlDocProcessorEndLisImpl(YamlDocProcessor):
    _indent: Final[int]
    _next: Final[YamlDocProcessor]

    def __init__(
        self,
        indent: int,
        next: YamlDocProcessor
    ) -> None:
        self._indent = indent
        self._next = next

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        line = reader.line()
        end_list_item_processor = YamlDocProcessorEndListItemImpl(
            end_list_processor=self
        )
        begin_list_item_processor = YamlDocProcessorBeginListItemImpl(
            indent=self._indent,
            end_list_item_processor=end_list_item_processor
        )

        begin_list_item_processor_result = begin_list_item_processor.process(
            reader=reader
        )

        if (begin_list_item_processor_result.success()):
            return begin_list_item_processor_result
        else:
            return YamlDocProcessorResultSuccessImpl(
                next=self._next,
                token=YamlDocTokenEndList(),
                line=line - 1
            )


class YamlDocProcessorEmptyDictImpl(YamlDocProcessor):
    _next: Final[YamlDocProcessor]

    def __init__(self, next: YamlDocProcessor) -> None:
        self._next = next

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        line = reader.line()
        index = CounterImpl()

        for char in "{}":
            if (char != reader.peek(index.inc())):
                return YamlDocProcessorResultFailureImpl()

        reader.forward(index.value())

        return YamlDocProcessorResultSuccessImpl(
            next=YamlDocProcessorCacheImpl(
                result=YamlDocProcessorResultSuccessImpl(
                    token=YamlDocTokenEndDict(),
                    next=self._next,
                    line=line
                )
            ),
            token=YamlDocTokenBeginDict(),
            line=line
        )


class YamlDocProcessorEmptyListImpl(YamlDocProcessor):
    _next: Final[YamlDocProcessor]

    def __init__(self, next: YamlDocProcessor) -> None:
        self._next = next

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        line = reader.line()
        index = CounterImpl()

        for char in "[]":
            if (char != reader.peek(index.inc())):
                return YamlDocProcessorResultFailureImpl()

        reader.forward(index.value())

        return YamlDocProcessorResultSuccessImpl(
            next=YamlDocProcessorCacheImpl(
                result=YamlDocProcessorResultSuccessImpl(
                    token=YamlDocTokenEndList(),
                    next=self._next,
                    line=line
                )
            ),
            token=YamlDocTokenBeginList(),
            line=line
        )


class YamlDocProcessorBeginListItemImpl(YamlDocProcessor):
    _indent: Final[int]
    _end_list_item_processor: Final[YamlDocProcessor]

    def __init__(
        self,
        indent: int,
        end_list_item_processor: YamlDocProcessor
    ) -> None:
        self._indent = indent
        self._end_list_item_processor = end_list_item_processor

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        line = reader.line()
        index = CounterImpl()

        for _ in range(0, self._indent):
            for _ in range(0, YamlDocConstants.INDENT_LENGTH):
                if (reader.peek(index.inc()) != " "):
                    return YamlDocProcessorResultFailureImpl()

        for char in "- ":
            if (char != reader.peek(index.inc())):
                return YamlDocProcessorResultFailureImpl()

        reader.forward(index.value())

        literal_processor = YamlDocProcessorLiteralTextImpl(
            next=self._end_list_item_processor
        )

        literal_processor_result = literal_processor.process(reader)

        if (literal_processor_result.success()):
            next = YamlDocProcessorLnImpl(
                on_success=literal_processor_result,
                on_failure=YamlDocProcessorResultFailureImpl()
            )
        else:
            next = YamlDocProcessorBeginDictImpl(
                first_line_indent=0,
                other_line_indent=self._indent + 1,
                next=self._end_list_item_processor
            )

        return YamlDocProcessorResultSuccessImpl(
            next=next,
            token=YamlDocTokenBeginListItem(),
            line=line
        )


class YamlDocProcessorEndListItemImpl(YamlDocProcessor):
    _end_list_processor: Final[YamlDocProcessor]

    def __init__(self, end_list_processor: YamlDocProcessor) -> None:
        self._end_list_processor = end_list_processor

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        line = reader.line()

        return YamlDocProcessorResultSuccessImpl(
            next=self._end_list_processor,
            token=YamlDocTokenEndListItem(),
            line=line - 1
        )


class YamlDocProcessorLiteralNumberImpl(YamlDocProcessor):
    _next: Final[YamlDocProcessor]

    def __init__(self, next: YamlDocProcessor) -> None:
        self._next = next

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        line = reader.line()
        index = CounterImpl()
        value = StringIO()

        char = reader.peek(index.inc())

        if (char in AlphabetUtils.NUMBERS):
            value.write(char)
        else:
            return YamlDocProcessorResultFailureImpl()

        while (True):
            char = reader.peek(index.inc())

            if (char in AlphabetUtils.NUMBERS):
                value.write(char)
            else:
                reader.forward(index.value() - 1)

                return YamlDocProcessorResultSuccessImpl(
                    next=self._next,
                    token=YamlDocTokenLiteralNumber(),
                    line=line
                )


class YamlDocProcessorLiteralTextImpl(YamlDocProcessor):
    _SPECIAL_CHARS: FinalMapping[str, str] = {
        "n": "\n",
        "\"": "\"",
        "\\": "\\"
    }

    _next: Final[YamlDocProcessor]

    def __init__(self, next: YamlDocProcessor) -> None:
        self._next = next

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        line = reader.line()
        index = CounterImpl()
        value = StringIO()

        if (reader.peek(index.inc()) != "\""):
            return YamlDocProcessorResultFailureImpl()

        while (True):
            char = reader.peek(index.inc())

            if (char == ""):
                return YamlDocProcessorResultFailureImpl()
            elif (char == "\""):
                reader.forward(index.value())

                return YamlDocProcessorResultSuccessImpl(
                    next=self._next,
                    token=YamlDocTokenLiteralText(),
                    line=line
                )
            elif (char == "\\"):
                next_char = reader.peek(index.inc())

                if (self._SPECIAL_CHARS.get(next_char, None) is not None):
                    value.write(char)
                    value.write(next_char)
                else:
                    return YamlDocProcessorResultFailureImpl()
            else:
                value.write(char)


class YamlDocProcessorLiteralMultilineTextImpl(YamlDocProcessor):
    _indent: Final[int]
    _next: Final[YamlDocProcessor]

    def __init__(self, indent: int, next: YamlDocProcessor) -> None:
        self._indent = indent
        self._next = next

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        line = reader.line()
        index = CounterImpl()

        for char in "|\n":
            if (char != reader.peek(index.inc())):
                return YamlDocProcessorResultFailureImpl()

        value = self._process_line(reader, index)

        if (value is None):
            return YamlDocProcessorResultFailureImpl()

        while (True):
            value = self._process_line(reader, index)

            if (value is None):
                reader.forward(index.value() - 1)

                return YamlDocProcessorResultSuccessImpl(
                    next=self._next,
                    token=YamlDocTokenLiteralText(),
                    line=line
                )

    def _process_line(
        self,
        reader: TextReader,
        index: Counter
    ) -> Optional[str]:
        local_index = CounterImpl(index.value())

        char = reader.peek(local_index.value())

        if (char == "\n"):
            index.inc()

            return char
        else:
            for _ in range(0, self._indent):
                for _ in range(0, YamlDocConstants.INDENT_LENGTH):
                    if (reader.peek(local_index.inc()) != " "):
                        return None

        char = reader.peek(local_index.inc())

        if (char in ("", "\n")):
            return None

        value = StringIO(char)

        while (True):
            char = reader.peek(local_index.inc())

            if (char == ""):
                return None
            elif (char == "\n"):
                index.update(local_index.value())

                value.write(char)

                return value.getvalue()
            else:
                value.write(char)


class YamlDocProcessorLiteralTrueImpl(YamlDocProcessor):
    _next: Final[YamlDocProcessor]

    def __init__(self, next: YamlDocProcessor) -> None:
        self._next = next

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        line = reader.line()
        index = CounterImpl()

        for char in "true":
            if (char != reader.peek(index.inc())):
                return YamlDocProcessorResultFailureImpl()

        reader.forward(index.value())

        return YamlDocProcessorResultSuccessImpl(
            next=self._next,
            token=YamlDocTokenLiteralTrue(),
            line=line
        )


class YamlDocProcessorLiteralFalseImpl(YamlDocProcessor):
    _next: Final[YamlDocProcessor]

    def __init__(self, next: YamlDocProcessor) -> None:
        self._next = next

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        line = reader.line()
        index = CounterImpl()

        for char in "false":
            if (char != reader.peek(index.inc())):
                return YamlDocProcessorResultFailureImpl()

        reader.forward(index.value())

        return YamlDocProcessorResultSuccessImpl(
            next=self._next,
            token=YamlDocTokenLiteralFalse(),
            line=line
        )


class YamlDocProcessorLiteralBoolImpl(YamlDocProcessorForkImpl):
    def __init__(self, next: YamlDocProcessor) -> None:
        super().__init__(
            processors=ArrayImpl.of(
                YamlDocProcessorLiteralFalseImpl(next),
                YamlDocProcessorLiteralTrueImpl(next)
            )
        )


class YamlDocProcessorLiteralNullImpl(YamlDocProcessorForkImpl):
    _next: Final[YamlDocProcessor]

    def __init__(self, next: YamlDocProcessor) -> None:
        self._next = next

    def process(self, reader: TextReader) -> TextDocProcessorResult:
        line = reader.line()
        index = CounterImpl()

        for char in "null":
            if (char != reader.peek(index.inc())):
                return YamlDocProcessorResultFailureImpl()

        reader.forward(index.value())

        return YamlDocProcessorResultSuccessImpl(
            next=self._next,
            token=YamlDocTokenLiteralNull(),
            line=line
        )


class YamlDocProcessorLnImpl(TextDocProcessorLnImpl, YamlDocProcessor):
    ...


class YamlDocProcessorEOFImpl(YamlDocProcessor):
    def process(self, reader: TextReader) -> TextDocProcessorResult:
        char = reader.peek(0)

        if (char == ""):
            return YamlDocProcessorResultSuccessImpl(
                next=YamlDocProcessor(),
                token=YamlDocTokenEOF(),
                line=reader.line()
            )
        else:
            return YamlDocProcessorResultFailureImpl()


__all__: FinalCollection[str] = [
    "YamlDocProcessorDocumentImpl",
    "YamlDocProcessorResultFailureImpl"
]
