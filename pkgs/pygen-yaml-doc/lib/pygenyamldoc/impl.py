from __future__ import annotations

from typing import Final

from pygentextdoc.abc import TextDocLoader
from pygentextdoc.abc import TextDocStream
from pygentextdoc.impl import TextDocLoaderImpl
from pygentype.abc import FinalCollection
from pygenyamldoc.abc import YamlDocLoader
from pygenyamldoc.abc import YamlDocTokenEOF
from pygenyamldoc.processor.impl import YamlDocProcessorDocumentImpl


class YamlDocLoaderImpl(YamlDocLoader):
    _loader: Final[TextDocLoader]

    def __init__(self) -> None:
        self._loader = TextDocLoaderImpl(
            processor=YamlDocProcessorDocumentImpl(),
            eof_token_type=YamlDocTokenEOF
        )

    def load_stream_from_file(self, path: str) -> TextDocStream:
        return self._loader.load_stream_from_file(path)


__all__: FinalCollection[str] = [
    "YamlDocLoaderImpl"
]
