from __future__ import annotations

from dataclasses import dataclass
from typing import Final

from pygencollection.impl import ArrayImpl
from pygenerr.err import SystemException
from pygentextdoc.abc import TextDocProcessor
from pygentextdoc.abc import TextDocStream
from pygentextdoc.abc import TextDocToken
from pygentype.abc import Class
from pygentype.abc import FinalCollection


class YamlDocTokenBeginDict(TextDocToken):
    ...


class YamlDocTokenEndDict(TextDocToken):
    ...


@dataclass
class YamlDocTokenDictKey(TextDocToken):
    value: Final[str]


class YamlDocTokenBeginList(TextDocToken):
    ...


class YamlDocTokenEndList(TextDocToken):
    ...


class YamlDocTokenBeginListItem(TextDocToken):
    ...


class YamlDocTokenEndListItem(TextDocToken):
    ...


class YamlDocTokenLiteralNumber(TextDocToken):
    ...


class YamlDocTokenLiteralText(TextDocToken):
    ...


class YamlDocTokenLiteralTrue(TextDocToken):
    ...


class YamlDocTokenLiteralFalse(TextDocToken):
    ...


class YamlDocTokenLiteralNull(TextDocToken):
    ...


class YamlDocTokenEOF(TextDocToken):
    ...


class YamlDocProcessor(TextDocProcessor):
    ...


class YamlDocLoader(object):
    def load_stream_from_file(self, path: str) -> TextDocStream:
        raise SystemException()


class YamlDocConstants(object):
    INDENT_LENGTH: Final[int] = 2
    TOKEN_CLASSES: FinalCollection[Class] = ArrayImpl[Class].of(
        YamlDocTokenBeginDict,
        YamlDocTokenEndDict,
        YamlDocTokenDictKey,
        YamlDocTokenBeginList,
        YamlDocTokenEndList,
        YamlDocTokenBeginListItem,
        YamlDocTokenEndListItem,
        YamlDocTokenLiteralNumber,
        YamlDocTokenLiteralText,
        YamlDocTokenLiteralTrue,
        YamlDocTokenLiteralFalse,
        YamlDocTokenLiteralNull,
        YamlDocTokenEOF
    )


__all__: FinalCollection[str] = [
    "YamlDocConstants",
    "YamlDocLoader",
    "YamlDocProcessor",
    "YamlDocTokenBeginDict",
    "YamlDocTokenBeginList",
    "YamlDocTokenBeginListItem",
    "YamlDocTokenDictKey",
    "YamlDocTokenEOF",
    "YamlDocTokenEndDict",
    "YamlDocTokenEndList",
    "YamlDocTokenEndListItem",
    "YamlDocTokenLiteralFalse",
    "YamlDocTokenLiteralNull",
    "YamlDocTokenLiteralNumber",
    "YamlDocTokenLiteralText",
    "YamlDocTokenLiteralTrue"
]
