from __future__ import annotations

from pygencounter.abc import Counter
from pygentype.abc import FinalCollection
from pygentype.abc import Mutable


class CounterImpl(Counter):
    _value: Mutable[int]

    def __init__(self, value: int = 0) -> None:
        self._value = value

    def __str__(self) -> str:
        return str(self._value)

    def dec(self) -> int:
        try:
            return self._value
        finally:
            self._value -= 1

    def inc(self) -> int:
        try:
            return self._value
        finally:
            self._value += 1

    def update(self, value: int) -> None:
        self._value = value

    def value(self) -> int:
        return self._value


__all__: FinalCollection[str] = [
    "CounterImpl"
]
