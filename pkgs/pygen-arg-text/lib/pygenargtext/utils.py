from __future__ import annotations

from collections.abc import Mapping
from typing import Final

from pygenargtext.abc import TextArg
from pygenargtext.abc import TextArgMapper
from pygenargtext.impl import TextArgMapperImpl
from pygentype.abc import FinalCollection


class TextArgUtils(object):
    MAPPER: Final[TextArgMapper] = TextArgMapperImpl()

    @classmethod
    def optional(cls, description: str, value: str) -> TextArg:
        return cls.MAPPER.optional(description, value)

    @classmethod
    def optional_env(
        cls,
        envs: Mapping[str, str],
        env_name: str
    ) -> TextArg:
        return cls.MAPPER.optional_env(envs, env_name)

    @classmethod
    def required(cls, description: str, value: str) -> TextArg:
        return cls.MAPPER.required(description, value)

    @classmethod
    def required_env(
        cls,
        envs: Mapping[str, str],
        env_name: str
    ) -> TextArg:
        return cls.MAPPER.required_env(envs, env_name)


__all__: FinalCollection[str] = [
    "TextArgUtils"
]
