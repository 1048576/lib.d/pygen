from __future__ import annotations

from collections.abc import Mapping
from typing import Final

from pygenarg.err import ArgEmptyValueException
from pygenargtext.abc import TextArg
from pygenargtext.abc import TextArgContext
from pygenargtext.abc import TextArgMapper
from pygenerr.err import UserException
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class TextArgImpl(TextArg, TextArgContext):
    _description: Final[str]
    _value: Final[str]

    def __init__(
        self,
        description: str,
        value: str
    ) -> None:
        self._description = description
        self._value = value

    def __enter__(self) -> TextArgContext:
        return self

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        if (isinstance(value, UserException)):
            raise value.wrap(
                title=self.describe()
            )

    def describe(self) -> str:
        return TextUtils.format_args(
            tpl="{}: [{}]",
            args=[
                self._description,
                self._value
            ]
        )

    def value(self) -> str:
        return self._value


class TextArgMapperImpl(TextArgMapper):
    def optional(self, description: str, value: str) -> TextArg:
        return self.required(description, value)

    def optional_env(
        self,
        envs: Mapping[str, str],
        env_name: str
    ) -> TextArg:
        return self.required_env(envs, env_name)

    def required(self, description: str, value: str) -> TextArg:
        if (value == ""):
            raise ArgEmptyValueException.create(
                description=description
            )

        return TextArgImpl(description, value)

    def required_env(
        self,
        envs: Mapping[str, str],
        env_name: str
    ) -> TextArg:
        return self.required(
            description=TextUtils.format_args(
                tpl="Env [{}]",
                args=[
                    env_name
                ]
            ),
            value=envs.get(env_name, "")
        )


__all__: FinalCollection[str] = [
    "TextArgMapperImpl"
]
