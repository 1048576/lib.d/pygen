from __future__ import annotations

from collections.abc import Mapping
from typing import Final

from pygencli.abc import CliArgValueContextManager
from pygencli.err import CliException
from pygenerr.err import UserException
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection
from pygentype.abc import FinalOptional


class CliArgValueContextManagerImpl(CliArgValueContextManager):
    _metavar: Final[str]
    _value: FinalOptional[str]

    def __init__(self, metavar: str, args: Mapping[str, str]) -> None:
        self._metavar = metavar
        self._value = args.get(metavar)

    def __enter__(self) -> str:
        if (self._value is not None):
            return self._value
        else:
            raise CliException(
                msg=TextUtils.format_args(
                    tpl="Undefined arg [{}] value",
                    args=[
                        self._metavar
                    ]
                )
            )

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        if (isinstance(value, UserException)):
            raise value.wrap(
                title=TextUtils.format_args(
                    tpl="Arg [{}]",
                    args=[
                        self._metavar
                    ]
                )
            )


__all__: FinalCollection[str] = [
    "CliArgValueContextManagerImpl"
]
