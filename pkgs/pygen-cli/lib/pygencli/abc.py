from __future__ import annotations

from collections.abc import Mapping
from collections.abc import Sequence
from contextlib import AbstractContextManager
from dataclasses import dataclass
from typing import Final

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection
from pygentype.abc import FinalOptional
from pygentype.abc import Union2
from pygentype.abc import Union4


@dataclass
class CliArg(object):
    metavar: Final[str]


@dataclass
class CliOption(object):
    name: Final[str]
    arg: Final[CliArg]


@dataclass
class CliSimpleCmd(object):
    name: Final[str]
    options: FinalCollection[CliOption]
    help: FinalOptional[str] = None


@dataclass
class CliComplexCmd(object):
    name: Final[str]
    arg: Final[CliArg]
    options: FinalCollection[CliOption]
    commands: FinalCollection[CliSimpleCmd | CliComplexCmd]
    help: FinalOptional[str] = None


@dataclass
class CliRootSimpleCmd(object):
    options: FinalCollection[CliOption]


@dataclass
class CliRootComplexCmd(object):
    arg: Final[CliArg]
    options: FinalCollection[CliOption]
    commands: FinalCollection[CliSimpleCmd | CliComplexCmd]


CliRootCmd = Union2[
    CliRootSimpleCmd,
    CliRootComplexCmd
]


CliCmd = Union4[
    CliSimpleCmd,
    CliComplexCmd,
    CliRootSimpleCmd,
    CliRootComplexCmd
]


class CliParser(object):
    def parse(
        self,
        args: Sequence[str],
        envs: Mapping[str, str]
    ) -> Mapping[str, str]:
        raise SystemException()


class CliParserBuilder(object):
    def build(
        self,
        command: CliCmd
    ) -> CliParser:
        raise SystemException()


class CliArgValueContextManager(AbstractContextManager[str]):
    def __enter__(self) -> str:
        raise SystemException()

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        raise SystemException()


__all__: FinalCollection[str] = [
    "CliArg",
    "CliArgValueContextManager",
    "CliCmd",
    "CliComplexCmd",
    "CliOption",
    "CliParser",
    "CliParserBuilder",
    "CliRootCmd",
    "CliRootComplexCmd",
    "CliRootSimpleCmd",
    "CliSimpleCmd"
]
