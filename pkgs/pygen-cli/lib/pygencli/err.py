from __future__ import annotations

from pygenerr.err import UserException
from pygentype.abc import FinalCollection


class CliException(UserException):
    ...


__all__: FinalCollection[str] = [
    "CliException"
]
