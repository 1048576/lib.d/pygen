from __future__ import annotations

from typing import Generic

from pygenarg.abc import Arg
from pygenerr.err import SystemException
from pygenpath.abc import Dirpath
from pygenpath.abc import Filepath
from pygenpath.abc import PathT
from pygentype.abc import FinalCollection


class PathArgContext(Generic[PathT]):
    def value(self) -> PathT:
        raise SystemException()


class PathArg(
    Generic[PathT],
    Arg[PathArgContext[PathT]],
    PathArgContext[PathT]
):
    ...


class DirpathArg(PathArg[Dirpath]):
    ...


class FilepathArg(PathArg[Filepath]):
    ...


__all__: FinalCollection[str] = [
    "DirpathArg",
    "FilepathArg",
    "PathArg",
    "PathArgContext"
]
