from __future__ import annotations

from typing import Final
from typing import Generic

from pygenargpath.abc import PathArg
from pygenargpath.abc import PathArgContext
from pygenerr.err import UserException
from pygenpath.abc import PathT
from pygentext.utils import TextUtils
from pygentype.abc import FinalCollection


class PathArgImpl(Generic[PathT], PathArg[PathT], PathArgContext[PathT]):
    _description: Final[str]
    _value: Final[PathT]

    def __init__(
        self,
        description: str,
        value: PathT
    ) -> None:
        self._description = description
        self._value = value

    def __enter__(self) -> PathArgContext[PathT]:
        return self

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        if (isinstance(value, UserException)):
            raise value.wrap(
                title=self.describe()
            )

    def describe(self) -> str:
        return TextUtils.format_args(
            tpl="{}: [{}]",
            args=[
                self._description,
                self._value.path()
            ]
        )

    def value(self) -> PathT:
        return self._value


__all__: FinalCollection[str] = [
    "PathArgImpl"
]
