from __future__ import annotations

from collections.abc import Iterator
from typing import Final

from pygencollection.impl import ArrayImpl
from pygenerr.err import SystemException
from pygenshell.abc import ShellOutput
from pygenshell.abc import ShellOutputEntry
from pygenshell.abc import ShellOutputSource
from pygenshell.abc import ShellSocket
from pygenshell.impl import ShellSocketImpl
from pygenshellserver.abc import ShellServer
from pygenshellserver.abc import ShellServerProcessor
from pygensocket.abc import Socket
from pygensocket.err import SocketClosedException
from pygentype.abc import FinalCollection


class ShellServerOutputImpl(ShellOutput):
    _socket: Final[ShellSocket]

    def __init__(self, socket: ShellSocket) -> None:
        self._socket = socket

    def write(self, entry: ShellOutputEntry) -> None:
        if (entry.source == ShellOutputSource.STDOUT):
            self._socket.send(ShellSocket.Marker.STDOUT, entry.data)
        elif (entry.source == ShellOutputSource.STDERR):
            self._socket.send(ShellSocket.Marker.STDERR, entry.data)
        else:
            raise SystemException()


class ShellServerImpl(ShellServer):
    _socket: Final[ShellSocket]
    _output: Final[ShellOutput]
    _processor: Final[ShellServerProcessor]

    def __init__(
        self,
        socket: Socket,
        processor: ShellServerProcessor
    ) -> None:
        self._socket = ShellSocketImpl(socket)
        self._output = ShellServerOutputImpl(self._socket)
        self._processor = processor

    def serve_forever(self) -> None:
        while (True):
            try:
                self._serve()
            except SocketClosedException:
                break

    def _receive_args(self) -> Iterator[str]:
        while (True):
            marker, data = self._socket.receive()

            if (marker == ShellSocket.Marker.ARG):
                yield data
            elif (marker == ShellSocket.Marker.RUN):
                return
            else:
                raise SystemException()

    def _serve(self) -> None:
        args = ArrayImpl.collect(self._receive_args())

        status = self._processor.process(args, self._output)

        self._socket.send(ShellSocket.Marker.STATUS, str(status))


__all__: FinalCollection[str] = [
    "ShellServerImpl"
]
