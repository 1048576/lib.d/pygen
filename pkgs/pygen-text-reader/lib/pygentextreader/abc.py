from __future__ import annotations

from contextlib import AbstractContextManager
from dataclasses import dataclass
from typing import Final

from pygenerr.err import SystemException
from pygentype.abc import FinalCollection


@dataclass
class TextReaderPosition(object):
    line: Final[int]
    column: Final[int]


class TextReader(object):
    def __str__(self) -> str:
        raise SystemException()

    def eof(self) -> bool:
        raise SystemException()

    def forward(self, length: int) -> None:
        raise SystemException()

    def line(self) -> int:
        raise SystemException()

    def peek(self, index: int) -> str:
        raise SystemException()

    def peek_position(self, index: int) -> TextReaderPosition:
        raise SystemException()

    def position(self) -> TextReaderPosition:
        raise SystemException()


class TextFileReader(AbstractContextManager[TextReader]):
    def __enter__(self) -> TextReader:
        raise SystemException()

    def __exit__(self, type: object, value: object, traceback: object) -> None:
        raise SystemException()


__all__: FinalCollection[str] = [
    "TextFileReader",
    "TextReader",
    "TextReaderPosition"
]
