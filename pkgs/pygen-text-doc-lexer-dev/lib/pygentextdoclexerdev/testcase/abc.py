from __future__ import annotations

from collections.abc import Mapping
from dataclasses import dataclass
from typing import Any
from typing import Final
from typing import Generic
from typing import TypeVar

from pygenerr.err import SystemException
from pygentextreader.abc import TextReader
from pygentype.abc import FinalCollection
from pygentype.abc import FinalMapping

ArgsT = TypeVar("ArgsT")
ResultT = TypeVar("ResultT")


@dataclass
class TextDocLexerTestCaseInput(object):
    args: FinalMapping[str, Any]
    text: Final[str]


class TextDocLexerTestCaseArgsFn(Generic[ArgsT]):
    def __call__(self, args: Mapping[str, Any]) -> ArgsT:
        raise SystemException()


class TextDocLexerTestCaseEntrypointFn(Generic[ArgsT, ResultT]):
    def __call__(self, args: ArgsT, reader: TextReader) -> ResultT:
        raise SystemException()


class TextDocLexerTestCaseNameFn(Generic[ResultT]):
    def __call__(self, result: ResultT) -> str:
        raise SystemException()


FinalTextDocLexerTestCaseArgsFn = \
    Final[TextDocLexerTestCaseArgsFn[ResultT]]
FinalTextDocLexerTestCaseEntrypointFn = \
    Final[TextDocLexerTestCaseEntrypointFn[ArgsT, ResultT]]
FinalTextDocLexerTestCaseNameFn = \
    Final[TextDocLexerTestCaseNameFn[ResultT]]


__all__: FinalCollection[str] = [
    "FinalTextDocLexerTestCaseArgsFn",
    "FinalTextDocLexerTestCaseEntrypointFn",
    "FinalTextDocLexerTestCaseNameFn",
    "TextDocLexerTestCaseArgsFn",
    "TextDocLexerTestCaseEntrypointFn",
    "TextDocLexerTestCaseInput",
    "TextDocLexerTestCaseNameFn"
]
