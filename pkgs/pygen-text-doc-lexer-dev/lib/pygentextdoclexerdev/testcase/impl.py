from __future__ import annotations

from io import StringIO
from typing import Final
from typing import Generic
from typing import TypeVar

from pygenjson.abc import JsonMapper
from pygenjson.utils import JsonUtils
from pygenos.abc import OSDir
from pygentest.abc import Test
from pygentest.abc import TestCase
from pygentext.utils import TextUtils
from pygentextdoclexerdev.testcase.abc import FinalTextDocLexerTestCaseArgsFn
from pygentextdoclexerdev.testcase.abc import FinalTextDocLexerTestCaseEntrypointFn
from pygentextdoclexerdev.testcase.abc import FinalTextDocLexerTestCaseNameFn
from pygentextdoclexerdev.testcase.abc import TextDocLexerTestCaseArgsFn
from pygentextdoclexerdev.testcase.abc import TextDocLexerTestCaseEntrypointFn
from pygentextdoclexerdev.testcase.abc import TextDocLexerTestCaseInput
from pygentextdoclexerdev.testcase.abc import TextDocLexerTestCaseNameFn
from pygentextreader.impl import TextReaderImpl
from pygentype.abc import FinalCollection

ArgsT = TypeVar("ArgsT")
ResultT = TypeVar("ResultT")


class TextDocLexerTestCaseImpl(
    Generic[ArgsT, ResultT],
    TestCase
):
    _json_mapper: Final[JsonMapper]
    _args_fn: FinalTextDocLexerTestCaseArgsFn[ArgsT]
    _entrypoint_fn: FinalTextDocLexerTestCaseEntrypointFn[ArgsT, ResultT]
    _name_fn: FinalTextDocLexerTestCaseNameFn[ResultT]
    _expected_name: Final[str]
    _dir: Final[OSDir]

    def __init__(
        self,
        json_mapper: JsonMapper,
        args_fn: TextDocLexerTestCaseArgsFn[ArgsT],
        entrypoint_fn: TextDocLexerTestCaseEntrypointFn[ArgsT, ResultT],
        name_fn: TextDocLexerTestCaseNameFn[ResultT],
        dirname: str,
        dir: OSDir
    ) -> None:
        self._json_mapper = json_mapper
        self._args_fn = args_fn
        self._entrypoint_fn = entrypoint_fn
        self._name_fn = name_fn
        self._expected_name = dirname
        self._dir = dir

    def run(self, test: Test) -> None:
        input_filepath = self._dir.dirpath.resolve_filepath(
            path="./input.json"
        )
        output_filepath = self._dir.dirpath.resolve_filepath(
            path="./output.json"
        )

        with (open(output_filepath.path()) as f):
            expected = f.read()

        with (open(input_filepath.path()) as f):
            input_content = f.read()
            input_args = JsonUtils.load_dict(input_content)
            input = TextDocLexerTestCaseInput(**input_args)

        result = self._entrypoint_fn(
            args=self._args_fn(input.args),
            reader=TextReaderImpl(
                name="",
                io=StringIO(input.text)
            )
        )

        test.assert_text_equal(
            expected=self._json_mapper.to_text(input_args),
            actual=input_content,
            msg=lambda args: TextUtils.format_args(
                tpl="Invalid format [{}]\n{}",
                args=[
                    input_filepath,
                    args.diff
                ]
            )
        )

        test.assert_text_equal(
            expected=self._expected_name,
            actual=self._name_fn(result),
            msg=lambda args: TextUtils.format_args(
                tpl="Invalid name [{}]\n{}",
                args=[
                    self._dir.dirpath,
                    args.diff
                ]
            )
        )

        test.assert_text_equal(
            expected=expected,
            actual=self._json_mapper.to_text(result),
            msg=lambda args: TextUtils.format_args(
                tpl="expected != actual [{}]\n{}",
                args=[
                    self._dir.dirpath,
                    args.diff
                ]
            )
        )

        test.assert_text_equal(
            expected=self._json_mapper.to_text(
                instance=sorted(self._dir.filenames)
            ),
            actual=self._json_mapper.to_text(
                instance=[
                    "input.json",
                    "output.json"
                ]
            ),
            msg=lambda args: TextUtils.format_args(
                tpl="expected != actual [{}]:\n{}",
                args=[
                    self._dir.dirpath,
                    args.diff
                ]
            )
        )


__all__: FinalCollection[str] = [
    "TextDocLexerTestCaseImpl"
]
