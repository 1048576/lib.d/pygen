from __future__ import annotations

from typing import Final
from typing import Generic
from typing import TypeVar

from pygencollection.impl import ArrayImpl
from pygencollection.impl import CollectionEncoderImpl
from pygencollection.utils import CollectionUtils
from pygendataclass.impl import DataclassEncoderImpl
from pygenjson.impl import PrettyJsonMapperImpl
from pygenos.abc import OSDir
from pygentext.utils import TextUtils
from pygentextdoclexerdev.testcase.abc import TextDocLexerTestCaseNameFn
from pygentextdoclexerdev.testcase.impl import TextDocLexerTestCaseImpl
from pygentextreader.abc import TextReaderPosition
from pygentype.abc import FinalCollection
from pygenyamldoclexer.abc import YamlDocLexerProcessor
from pygenyamldoclexer.abc import YamlDocLexerResult
from pygenyamldoclexer.abc import YamlDocLexerResultMapper
from pygenyamldoclexer.abc import YamlDocLexerValue
from pygenyamldoclexerdev.abc import YamlDocLexerValueCollection
from pygenyamldoclexerdev.abc import YamlDocLexerValueList
from pygenyamldoclexerdev.testcase.abc import YamlDocLexerTestCaseArgsFn
from pygenyamldoclexerdev.testcase.abc import YamlDocLexerTestCaseEntrypointFn

ArgsT = TypeVar("ArgsT")


class YamlDocLexerTestCaseJsonMapperImpl(PrettyJsonMapperImpl):
    def __init__(self) -> None:
        super().__init__(
            encoders=ArrayImpl.of(
                CollectionEncoderImpl(),
                DataclassEncoderImpl()
            )
        )


class YamlDocLexerTestCaseResultMapperImpl(YamlDocLexerResultMapper):
    _values: Final[YamlDocLexerValueList]
    _origin: Final[YamlDocLexerResultMapper]

    def __init__(
        self,
        values: YamlDocLexerValueList,
        origin: YamlDocLexerResultMapper
    ) -> None:
        self._values = values
        self._origin = origin

    def failure(
        self,
        code: str,
        position: TextReaderPosition
    ) -> YamlDocLexerResult:
        result = self._origin.failure(code, position)

        self._values.add(result.value())

        return result

    def success(
        self,
        value: YamlDocLexerValue,
        next: YamlDocLexerProcessor
    ) -> YamlDocLexerResult:
        return self._origin.success(value, next)


class YamlDocLexerTestCaseNameFnImpl(
    TextDocLexerTestCaseNameFn[YamlDocLexerValueCollection]
):
    def __call__(self, result: YamlDocLexerValueCollection) -> str:
        return TextUtils.join(
            delimiter=",",
            items=CollectionUtils.map(
                items=result,
                fn=lambda value: value.code
            )
        )


class YamlDocLexerTestCaseImpl(
    Generic[ArgsT],
    TextDocLexerTestCaseImpl[
        ArgsT,
        YamlDocLexerValueCollection
    ]
):
    def __init__(
        self,
        args_fn: YamlDocLexerTestCaseArgsFn[ArgsT],
        entrypoint_fn: YamlDocLexerTestCaseEntrypointFn[ArgsT],
        dirname: str,
        dir: OSDir
    ) -> None:
        super().__init__(
            json_mapper=YamlDocLexerTestCaseJsonMapperImpl(),
            args_fn=args_fn,
            entrypoint_fn=entrypoint_fn,
            name_fn=YamlDocLexerTestCaseNameFnImpl(),
            dirname=dirname,
            dir=dir
        )


__all__: FinalCollection[str] = [
    "YamlDocLexerTestCaseImpl",
    "YamlDocLexerTestCaseJsonMapperImpl",
    "YamlDocLexerTestCaseNameFnImpl",
    "YamlDocLexerTestCaseResultMapperImpl"
]
