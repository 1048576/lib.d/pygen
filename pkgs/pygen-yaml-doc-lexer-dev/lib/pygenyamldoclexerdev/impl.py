from __future__ import annotations

from pygencollection.impl import ListImpl
from pygentype.abc import FinalCollection
from pygenyamldoclexer.abc import YamlDocLexerValue
from pygenyamldoclexerdev.abc import YamlDocLexerValueList


class YamlDocLexerValueListImpl(
    ListImpl[YamlDocLexerValue],
    YamlDocLexerValueList
):
    ...


__all__: FinalCollection[str] = [
    "YamlDocLexerValueListImpl"
]
