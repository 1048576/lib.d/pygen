from __future__ import annotations

from collections.abc import Iterable


def f() -> Iterable[str]:
    ...
