from __future__ import annotations

from collections.abc import Iterable
from typing import Final
from typing import Protocol

from pygentype.abc import Mutable


class C1(object):
    v0: str = ""
    v1: Final[str] = ""
    v2: Mutable[str] = ""
    v3: Iterable[Iterable[str]] = []


class C2(Protocol):
    v: str
