from __future__ import annotations


class C(object):
     def c(self) -> None:
          ...

     @classmethod
     def z(cls) -> None:
          ...

     def __str__(self) -> str:
          ...

     def a(self) -> None:
          ...

     def b(self) -> None:
          ...

     def _c(self) -> None:
          ...

     def _a(self) -> None:
          ...

     def _b(self) -> None:
          ...

     def __eq__(self, other: object) -> bool:
          ...

     def __init__(self) -> None:
          ...

     @classmethod
     def x(cls) -> None:
          ...

     @classmethod
     def y(cls) -> None:
          ...

     @classmethod
     def _z(cls) -> None:
          ...

     @classmethod
     def _x(cls) -> None:
          ...

     @classmethod
     def _y(cls) -> None:
          ...
