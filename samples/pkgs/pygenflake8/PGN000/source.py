from __future__ import annotations


def f(a0: str, a1: str) -> None:
    ...


if (__name__ == "__main__"):
    f("", a1="")

    f(
        "",
        ""
    )
