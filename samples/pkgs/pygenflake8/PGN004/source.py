from __future__ import annotations

from typing import Final
from typing import Generic

from pygentype.abc import FinalCollection
from pygentype.abc import T


class C(
    Generic[
        T,
    ]
):
    v: Final[T,]


def f0(a0: str,) -> None:
    ...


def f1(a0: str, a1: str,) -> None:
    ...


def f2(*args: str,) -> None:
    ...


def f3(**kwargs: str,) -> None:
    ...


def f4(a0: str = "",) -> None:
    ...


def f() -> object:
    f0("",)

    f0(
        a0="",
    )

    f1("", "",)

    f1(
        a0="",
        a1="",
    )

    v0 = [
        "",
    ]

    v1 = [
        "",
        "",
    ]

    v2 = {
        "k0": "",
    }

    v3 = {
        "k0": "",
        "k1": "",
    }

    return (v0, v1, v2, v3)


FinalC = Final[C[T,]]


__all__: FinalCollection[str] = [
    "f",
]
