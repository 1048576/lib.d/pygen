from __future__ import annotations

from typing import Final
from typing import Generic

from pygentype.abc import T


class C(
    Generic[
         T
    ]
):
    v: Final[
         T
    ]


def f(
     a: str
) -> object:
    v0 = [
         ""
    ]

    v1 = {
         "": ""
    }

    return (v0, v1)


if (__name__ == "__main__"):
    f(
         a=""
    )


FinalC = Final[
    C[
         T
    ]
]
